package mobapply.freightexchange.library.arrayadapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.model.MyProposal;
import mobapply.freightexchange.model.SortingStates;
import mobapply.freightexchange.model.statuses.ProposalStatuses;

/**
 * Created by Ivan Dunskiy on 4/6/15.
 *
 */
public class MyProposalsAdapterFixed extends ArrayAdapter<MyProposal> {
    private List<MyProposal> proposalsList;
    private Context mContext;

    ViewHolder viewHolder = null;

    private boolean isCityToHighlighted;
    private boolean isCityFromHighlighted;
    private boolean isCountryToHighlighted;
    private boolean isCountryFromHighlighted;
    private boolean isZipCodeFromHighlighted;
    private boolean isZipCodeToHighlighted;
    private boolean isGoodsHighlighted;
    private boolean isInfoHighlighted;
    private boolean isDateHighlighted;
    private boolean isWeightHighlighted;


    public MyProposalsAdapterFixed(Context context, int resource, List<MyProposal> objects) {
        super(context, resource, objects);
        this.proposalsList = objects;
        this.mContext = context;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // ViewHolder буферизирует оценку различных полей шаблона элемента

        View rowView = convertView;
        if (rowView == null) {

            LayoutInflater inflater = LayoutInflater.from(mContext);
            rowView = inflater.inflate(R.layout.my_proposal_item, null, true);
            viewHolder = new ViewHolder();
            rowView.setTag(viewHolder);

            viewHolder.proposalId = (TextView) rowView.findViewById(R.id.proposal_id);

            viewHolder.proposal_status = (TextView) rowView.findViewById(R.id.proposal_status);

            viewHolder.proposalFromCity = (TextView) rowView.findViewById(R.id.proposal_from_city);
            viewHolder.proposalFromCountry = (TextView) rowView.findViewById(R.id.proposal_from_country);
            viewHolder.proposalFromIndex = (TextView) rowView.findViewById(R.id.proposal_from_index);

            viewHolder.proposalToCity = (TextView) rowView.findViewById(R.id.proposal_to_city);
            viewHolder.proposalToCountry = (TextView) rowView.findViewById(R.id.proposal_to_country);
            viewHolder.proposalToIndex = (TextView) rowView.findViewById(R.id.proposal_to_index);

            viewHolder.proposalGoods = (TextView) rowView.findViewById(R.id.proposal_goods);
            viewHolder.proposalWeight = (TextView) rowView.findViewById(R.id.proposal_weight);
            viewHolder.proposalInfo = (TextView) rowView.findViewById(R.id.proposal_info);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        MyProposal proposal = proposalsList.get(position);
        viewHolder.proposalId.setText(Utils.getFormattedDate(mContext, proposal.getMyProposalOrder().getAppointmentFrom())+  System.getProperty("line.separator") +
                Utils.getFormattedTime(mContext, proposal.getMyProposalOrder().getAppointmentFrom()));

        viewHolder.proposalFromCity.setText(proposal.getMyProposalOrder().getDepartureAddress().getCity());
        viewHolder.proposalFromCountry.setText(proposal.getMyProposalOrder().getDepartureAddress().getCountry());
        /*
         * Displaying only 2 digits in index if security option turn on
         */
        if (Utils.getCurrentConfiguration(mContext) !=null &&
                Utils.getCurrentConfiguration(mContext).isAnonymousOrders())
        {
            viewHolder.proposalFromIndex.setText(proposal.getMyProposalOrder().getDepartureAddress().getZipCode().substring(0,2));
        }
        else
            viewHolder.proposalFromIndex.setText(proposal.getMyProposalOrder().getDepartureAddress().getZipCode());


        viewHolder.proposalToCity.setText(proposal.getMyProposalOrder().getDestinationAddress().getCity());
        viewHolder.proposalToCountry.setText(proposal.getMyProposalOrder().getDestinationAddress().getCountry());
        if (Utils.getCurrentConfiguration(mContext) !=null &&
                Utils.getCurrentConfiguration(mContext).isAnonymousOrders()) {
            viewHolder.proposalToIndex.setText(proposal.getMyProposalOrder().getDestinationAddress().getZipCode().substring(0,2));
        }
        else
            viewHolder.proposalToIndex.setText(proposal.getMyProposalOrder().getDestinationAddress().getZipCode());

        viewHolder.proposalGoods.setText(proposal.getMyProposalOrder().getGood());
        viewHolder.proposalInfo.setText(Float.toString(proposal.getCostOfFreight())+ " " + mContext.getString(R.string.euro_label));

        String status = proposal.getStatus();

        if (status != null && !status.isEmpty()) {
            String completeStatus = null;

            if (status.equalsIgnoreCase(ProposalStatuses.PENDING.toString())) {
                viewHolder.proposal_status.setTextColor(Color.parseColor("#1e8dce"));
                completeStatus = mContext.getString(R.string.status_proposal_pending);
            } else if (status.equalsIgnoreCase(ProposalStatuses.ACCEPTED.toString())) {
                viewHolder.proposal_status.setTextColor(Color.parseColor("#f74ba8"));
                completeStatus = mContext.getString(R.string.status_proposal_accepted);
            } else if (status.equalsIgnoreCase(ProposalStatuses.REJECTED.toString())) {
                viewHolder.proposal_status.setTextColor(Color.parseColor("#ff483e"));
                completeStatus = mContext.getString(R.string.status_proposal_rejected);
            }  else if (status.equalsIgnoreCase(ProposalStatuses.CANCELLED.toString())) {
                viewHolder.proposal_status.setTextColor(Color.parseColor("#be6839"));
                completeStatus = mContext.getString(R.string.status_proposal_cancelled);
            }
            else if (status.equalsIgnoreCase(ProposalStatuses.REPLACED.toString())) {
                viewHolder.proposal_status.setTextColor(Color.parseColor("#A50505"));
                completeStatus = mContext.getString(R.string.status_proposal_replaced);
            }
            else if (status.equalsIgnoreCase(ProposalStatuses.REFUSED.toString())) {
                viewHolder.proposal_status.setTextColor(Color.parseColor("#6D96F7"));
                completeStatus = mContext.getString(R.string.status_proposal_refused);
            }
            else if (status.equalsIgnoreCase(ProposalStatuses.NOT_ACTUAL.toString())) {
                viewHolder.proposal_status.setTextColor(Color.parseColor("#6B4631"));
                completeStatus = mContext.getString(R.string.status_proposal_not_actual);
            }
            else if (status.equalsIgnoreCase(ProposalStatuses.FAILED.toString())) {
                viewHolder.proposal_status.setTextColor(Color.parseColor("#9F3E05"));
                completeStatus = mContext.getString(R.string.status_proposal_failed);
            }

            viewHolder.proposal_status.setText(completeStatus);
        }

        if (isCityToHighlighted)
        {
            sortingStateView(SortingStates.CITY_TO);
        }
        if (isCityFromHighlighted)
        {
            sortingStateView(SortingStates.CITY_FROM);
        }
        if (isCountryFromHighlighted)
        {
            sortingStateView(SortingStates.COUNTRY_FROM);
        }
        if (isCountryToHighlighted)
        {
            sortingStateView(SortingStates.COUNTRY_TO);
        }
        if (isZipCodeToHighlighted)
        {
            sortingStateView(SortingStates.ZIPCODE_TO);
        }
        if (isZipCodeFromHighlighted)
        {
            sortingStateView(SortingStates.ZIPCODE_FROM);
        }
        if (isGoodsHighlighted) {
            sortingStateView(SortingStates.GOODS);
        }
        if (isInfoHighlighted){
            sortingStateView(SortingStates.INFO);
        }
        if (isDateHighlighted){
            sortingStateView(SortingStates.DATE);
        }
        if (isWeightHighlighted){
            sortingStateView(SortingStates.WEIGHT);
        }


        return rowView;
    }

    public void highlighLabel(SortingStates sortingStates) {
        switch (sortingStates){
            case CITY_TO:
                isCityToHighlighted = true;
                isCountryToHighlighted = false;
                isZipCodeToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case CITY_FROM:
                isCityFromHighlighted = true;
                isCountryFromHighlighted = false;
                isZipCodeFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case COUNTRY_TO:
                isCountryToHighlighted = true;
                isCityToHighlighted = false;
                isZipCodeToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case COUNTRY_FROM:
                isCountryFromHighlighted = true;
                isCityFromHighlighted = false;
                isZipCodeFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case ZIPCODE_FROM:
                isZipCodeFromHighlighted = true;
                isCountryFromHighlighted = false;
                isCityFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case ZIPCODE_TO:
                isZipCodeToHighlighted = true;
                isCityToHighlighted = false;
                isCountryToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case GOODS:
                isGoodsHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isInfoHighlighted = false;
                isDateHighlighted = false;
                isWeightHighlighted = false;
                break;
            case INFO:
                isInfoHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isWeightHighlighted = false;
                break;
            case DATE:
                isDateHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case WEIGHT:
                isWeightHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isInfoHighlighted = false;
                isDateHighlighted = false;
                break;

        }
        notifyDataSetChanged();

    }

    private void removeHighlightFrom()
    {
        viewHolder.proposalFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.proposalFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.proposalFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
    }
    private void removeHighlightTo(){
        viewHolder.proposalToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.proposalToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.proposalToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
    }
    private void highlightOffFrom(){
        isZipCodeFromHighlighted = false;
        isCountryFromHighlighted = false;
        isCityFromHighlighted = false;
    }
    private void highlightOffTo(){
        isZipCodeToHighlighted = false;
        isCityToHighlighted = false;
        isCountryToHighlighted = false;
    }

    private  void sortingStateView(SortingStates sortingStates)
    {
        switch (sortingStates){
            case CITY_TO:
                viewHolder.proposalToCity.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case CITY_FROM:
                viewHolder.proposalFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case COUNTRY_TO:
                viewHolder.proposalToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case COUNTRY_FROM:
                viewHolder.proposalFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case ZIPCODE_TO:
                viewHolder.proposalToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case ZIPCODE_FROM:
                viewHolder.proposalFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case DATE:
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case INFO:
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case WEIGHT:
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case GOODS:
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
        }
    }

    public void setContext(Context context){
        this.mContext = context;
    }

    public static class ViewHolder{

        private  TextView proposal_status;
        public  TextView proposalId;
        private  TextView proposalFromCountry;
        private  TextView proposalFromIndex;
        private  TextView proposalToCity;
        private  TextView proposalToCountry;
        private  TextView proposalToIndex;
        private  TextView proposalFromCity;
        public  TextView proposalGoods;
        public  TextView proposalInfo;
        public TextView proposalWeight;
    }


    public void setProposalsList(List<MyProposal> proposalsList){
        this.proposalsList = proposalsList;
        notifyDataSetChanged();
    }
}
