package mobapply.freightexchange.library.arrayadapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import mobapply.freightexchange.library.interfaces.NestedFragmentsSwitcher;
import mobapply.freightexchange.ui.fragments.FilterFragment;
import mobapply.freightexchange.ui.fragments.SavedFiltersDeleteFragment;
import mobapply.freightexchange.ui.fragments.SavedFiltersFragment;

/**
 * Created by Ivan Dunskiy on 7/8/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class FiltersPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "FiltersPagerAdapter";
    private final FragmentManager mFragmentManager;
    public Fragment mFragmentAtPos0;
    private final class FirstPageListener implements
            NestedFragmentsSwitcher {
        public void onSwitchToNextFragment() {
            mFragmentManager.beginTransaction().remove(mFragmentAtPos0)
                    .commit();
            if (mFragmentAtPos0 instanceof SavedFiltersFragment){
                mFragmentAtPos0 = new SavedFiltersDeleteFragment(mFirstPageListener);
                ((SavedFiltersDeleteFragment) mFragmentAtPos0).setNestedFragmentsSwitcher(mFirstPageListener);
            }else{ // Instance of NextFragment
                mFragmentAtPos0 = new SavedFiltersFragment(mFirstPageListener);
                ((SavedFiltersFragment) mFragmentAtPos0).setNestedFragmentsSwitcher(mFirstPageListener);
            }
            notifyDataSetChanged();
        }
    }

    FirstPageListener mFirstPageListener = new FirstPageListener();
    public FiltersPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
    }

    @Override
    public Fragment getItem(int i) {
        if (i==0){
            return new FilterFragment();
        }
        else {
            if (mFragmentAtPos0 == null)
            {
                mFragmentAtPos0 = new SavedFiltersFragment(mFirstPageListener);
            }
            return mFragmentAtPos0;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (position == 0)? "Create filter" : "Saved Filters" ;
    }
    @Override
    public int getItemPosition(Object object)
    {
        if (object instanceof SavedFiltersFragment &&
                mFragmentAtPos0 instanceof SavedFiltersDeleteFragment) {
            return POSITION_NONE;
        }
        if (object instanceof SavedFiltersDeleteFragment &&
                mFragmentAtPos0 instanceof SavedFiltersFragment) {
            return POSITION_NONE;
        }
        return POSITION_UNCHANGED;
    }




}
