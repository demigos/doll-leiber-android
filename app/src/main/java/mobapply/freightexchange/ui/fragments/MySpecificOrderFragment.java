package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.GPSTracker;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.model.statuses.OrderStatuses;
import mobapply.freightexchange.providers.MySpecificOrderProvider;
import mobapply.freightexchange.views.MySpecificOrderView;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link mobapply.freightexchange.ui.fragments.MySpecificOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MySpecificOrderFragment extends Fragment implements View.OnClickListener, MySpecificOrderProvider.MySpecificOrderCallback
         ,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{

    private MySpecificOrderProvider mSpecificOrderProvider;
    private String uuid;
    private SharedPreferences preferences;
    private MySpecificOrderView mMySpecificOrderView;
    private EditText edit_proposal;
    private Button btn_cancel_order;
    private Button btn_change_order_status;
    private Button btn_send_message;
    private EditText specific_order_message_box;
    private ImageView btn_attach_position;
    private int btn_attach_position_state;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    public static MySpecificOrderFragment newInstance(String uuid) {
        MySpecificOrderFragment fragment = new MySpecificOrderFragment();
        Bundle args = new Bundle();
        args.putString(Constants.KEY_CHOOSED_ORDER, uuid);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uuid = getArguments().getString(Constants.KEY_CHOOSED_ORDER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_my_specific_order, container, false);
        btn_cancel_order  =  (Button) view.findViewById(R.id.btn_cancel_order);
        btn_change_order_status  =  (Button) view.findViewById(R.id.btn_change_order_status);
        btn_send_message = (Button) view.findViewById(R.id.btn_send_message);
        btn_attach_position = (ImageView) view.findViewById(R.id.btn_attach_position);
        specific_order_message_box = (EditText) view.findViewById(R.id.specific_order_message_box);

        btn_cancel_order.setOnClickListener(this);
        btn_change_order_status.setOnClickListener(this);
        btn_send_message.setOnClickListener(this);
        btn_attach_position.setOnClickListener(this);

        preferences = getActivity().getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        mMySpecificOrderView = new MySpecificOrderView(getActivity(), view);
        mSpecificOrderProvider = new MySpecificOrderProvider(getActivity(),uuid);
        mSpecificOrderProvider.loadOrder(this);



        return view;
    }

    @Override
    public void onOrderLoaded(Order mOrder) {
        mMySpecificOrderView.displayOrder(mOrder);
    }

    @Override
    public void onStatusChanged() {
        mSpecificOrderProvider = new MySpecificOrderProvider(getActivity(),uuid);
        mSpecificOrderProvider.loadOrder(this);
    }

    @Override
    public void onMessageSend(int flag) {
        if (flag==1){
            specific_order_message_box.getText().clear();
        }
        if (flag ==2){
            mSpecificOrderProvider.changeOrderStatus(this, OrderStatuses.CANCELLED.name());
        }
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()) {

            case R.id.btn_attach_position:
                if (btn_attach_position_state == 0) {
                    btn_attach_position.setImageResource(R.drawable.ic_attach_position_blue);
                    btn_attach_position_state = 1;
                } else if (btn_attach_position_state == 1) {
                    btn_attach_position.setImageResource(R.drawable.ic_attach_position_black);
                    btn_attach_position_state = 0;
                }
                break;

            case R.id.btn_cancel_order:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                DialogFragment dialog = new OrderCancelFragment();
                dialog.setTargetFragment(this, Constants.REQUESTCODE_ORDER_CANCEL);// creating new object
                dialog.show(fm, "dialog");
                break;

            case R.id.btn_change_order_status:

                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.label_change_status)
                        .setMessage(R.string.label_change_status_description)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String nextOrderStatus = mMySpecificOrderView.getNewOrderStatus();
                                mSpecificOrderProvider.changeOrderStatus(MySpecificOrderFragment.this, nextOrderStatus);
                                specific_order_message_box.getText().clear();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(R.drawable.ic_launcher)
                        .show();
                break;
            case R.id.btn_send_message:
                if (specific_order_message_box != null && !specific_order_message_box.getText().toString().isEmpty() )
                {
                    String message = specific_order_message_box.getText().toString();

                    if (btn_attach_position_state==1) {
                        GPSTracker gpsTracker = new GPSTracker(getActivity());
                        String stringLatitude = "", stringLongitude = "", nameOfLocation="";
                        if (gpsTracker.canGetLocation()) {

                            stringLatitude = String.valueOf(gpsTracker.latitude);
                            stringLongitude = String.valueOf(gpsTracker.longitude);
                        }
                        if (stringLatitude != null && stringLongitude!=null) {
                            mSpecificOrderProvider.sendMessage(this, 1, message,
                                    stringLatitude, stringLongitude);
                        }
                    }
                    else
                        mSpecificOrderProvider.sendMessage(this, 1, message,"0","0");
                }
                else
                    Toast.makeText(getActivity(), getString(R.string.toast_empty_message), Toast.LENGTH_LONG).show();

                break;


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case Constants.REQUESTCODE_ORDER_CANCEL:
                if (resultCode == Activity.RESULT_OK) {

                    if (data.getExtras() != null && data.getExtras().getString(Constants.BUNDLE_CANCELLATION_MESSAGE) != null)
                    {
                        String message = data.getExtras().getString(Constants.BUNDLE_CANCELLATION_MESSAGE);
                        if (btn_attach_position_state==1) {
                            GPSTracker gpsTracker = new GPSTracker(getActivity());
                            String stringLatitude = "", stringLongitude = "", nameOfLocation="";
                            if (gpsTracker.canGetLocation()) {

                                stringLatitude = String.valueOf(gpsTracker.latitude);
                                stringLongitude = String.valueOf(gpsTracker.longitude);
                            }
                            if (stringLatitude != null && stringLongitude!=null) {
                                mSpecificOrderProvider.sendMessage(this, 2, message,
                                        stringLatitude, stringLongitude);
                            }
                        }
                        else
                            mSpecificOrderProvider.sendMessage(this, 2, message,"0","0");

                    }


                } else if (resultCode == Activity.RESULT_CANCELED){
                    // After Cancel code.
                }

                break;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
