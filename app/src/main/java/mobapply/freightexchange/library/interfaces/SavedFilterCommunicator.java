package mobapply.freightexchange.library.interfaces;

import mobapply.freightexchange.model.FilterItem;

/**
 * Created by Ivan Dunskiy on 7/25/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public interface SavedFilterCommunicator {
    void passSavedFilter(FilterItem filterItem);
}
