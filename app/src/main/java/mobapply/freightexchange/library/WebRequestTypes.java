package mobapply.freightexchange.library;

/**
 * Created by Ivan Dunskiy on 7/27/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public enum WebRequestTypes {

    LOGIN{
        @Override
        public String toString() {
            return "login";
        }
    },
    ALL_ORDERS{
        @Override
        public String toString() {
            return "all_orders";
        }
    },

    USERS{
        @Override
        public String toString() {
            return "users";
        }
    },
    FILTER{
        @Override
        public String toString() {
            return "users";
        }
    },
    PROPOSALS{
        @Override
        public String toString() {
            return "proposals";
        }
    },
    ADD_PROPOSAL{
        @Override
        public String toString() {
            return "add_proposal";
        }
    },
    DELETE_PROPOSAL{
        @Override
        public String toString() {
            return "delete_proposal";
        }
    },
    CHANGE_STATUS{
        @Override
        public String toString() {
            return "change_status";
        }
    },
    SEND_MESSAGE{
        @Override
        public String toString() {
            return "send_message";
        }
    },
    OVERVIEW_BY_COUNTRY{
        @Override
        public String toString() {
            return "overview_by_country";
        }
    },
    FILTER_COUNT{
        @Override
        public String toString() {
            return "filter_count";
        }
    },
    FILTER_RESULTS{
        @Override
        public String toString() {
            return "filter_results";
        }
    },
    ALL_CITIES {
        @Override
        public String toString() {
            return "cities";
        }
    },
     ALL_COUNTRIES {
        @Override
        public String toString() {
            return "countries";
        }
    },
    ALL_ZIPCODES {
        @Override
        public String toString() {
            return "zipcodes";
        }
    },




}
