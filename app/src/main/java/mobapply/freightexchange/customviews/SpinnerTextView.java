package mobapply.freightexchange.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ivan Dunskiy on 7/20/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class SpinnerTextView extends TextView {

    public SpinnerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }



    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
