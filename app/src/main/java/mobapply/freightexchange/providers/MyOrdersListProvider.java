package mobapply.freightexchange.providers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.MyProposal;
import mobapply.freightexchange.model.Order;

/**
 * Created by Ivan Dunskiy on 7/3/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class MyOrdersListProvider {

    private static final String TAG = "MyOrdersListProvider";
    private ProgressDialog mProgressDialog;
    private Context mContext;

    public MyOrdersListProvider(Context mContext) {
        this.mContext = mContext;
    }

    /*
     * Class for loading and processing orders
     */
    public void loadMyOrders(final MyOrdersCallback callback) {
        final SharedPreferences mPreferences = mContext.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        String url = Utils.getRequestUrl(mContext, WebRequestTypes.USERS)
                + mPreferences.getString(Constants.SHARED_CURRENT_USERNAME, "")+ "/orders";
        Utils.sendNewJsonVolleyRequest(mContext, Request.Method.GET , url, null, new VolleyCallback() {
            @Override
            public void onSuccess(String stringObject) {
                try {
                    ((Activity)(mContext)).runOnUiThread(new MyProposalsParsingHelper(new JSONArray(stringObject), callback));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public interface MyOrdersCallback {
        void onMyOrdersLoaded(List<Order> orderList);
    }

    class MyProposalsParsingHelper implements Runnable {
        // Defines the code to run for this task
        JSONArray receivedJsonArray;
        MyOrdersCallback ordersCallback;

        MyProposalsParsingHelper(JSONArray receivedJsonArray, MyOrdersCallback ordersCallback) {
            this.receivedJsonArray = receivedJsonArray;
            this.ordersCallback = ordersCallback;
        }

        public void run() {
            List<Order> ordersList = JSONParser.parseOrders(receivedJsonArray);
            ordersCallback.onMyOrdersLoaded(ordersList);
        }
    }
}
