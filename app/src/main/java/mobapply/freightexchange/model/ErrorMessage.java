package mobapply.freightexchange.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ivan Dunskiy on 7/11/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class ErrorMessage {

    @SerializedName("message")
    String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return errorMessage;
    }
}
