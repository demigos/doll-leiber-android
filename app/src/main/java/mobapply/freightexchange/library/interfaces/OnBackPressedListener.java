package mobapply.freightexchange.library.interfaces;

/**
 * Created by Ivan Dunskiy on 6/2/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public interface OnBackPressedListener {
    public void doBack();
}
