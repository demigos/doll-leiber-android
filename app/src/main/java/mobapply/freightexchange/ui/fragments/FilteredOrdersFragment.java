package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobapply.freightexchange.R;
import mobapply.freightexchange.customviews.CustomLinearLayoutManager;
import mobapply.freightexchange.customviews.DividerItemDecoration;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.RecyclerItemClickListener;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.arrayadapters.OrdersAdapter;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.ui.activities.FilterTabbedActivity;
import mobapply.freightexchange.ui.activities.MainActivity;
import mobapply.freightexchange.ui.activities.SpecificOrderActivity;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;

/**
 * Created by Ivan Dunskiy on 4/6/15.
   Copyright (c) 2015 Rarus. All rights reserved.
 */
public class FilteredOrdersFragment extends Fragment{
    private static final String TAG = "FilteredOrdersFragment";
    private ArrayList<Order> ordersList = new ArrayList<>();
    private RecyclerView recyclerView;
    private JSONObject mFilterJson;
    private Toolbar mToolBar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =   LayoutInflater.from(container.getContext())
                .inflate(R.layout.fragment_filtered_orders_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.orders_recycler_view);
        recyclerView.setLayoutManager(new CustomLinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        ((MainActivity) getActivity()).getSupportActionBar().hide();
        // 3. create an adapter
        mToolBar = (Toolbar) view.findViewById(R.id.filtered_toolbar);
        mToolBar.setTitle("");
        ActionBarActivity activity = (ActionBarActivity) getActivity();
        activity.setSupportActionBar(mToolBar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolBar.setNavigationIcon(R.drawable.ic_filter_white_24dp);
        this.setHasOptionsMenu(true);
        filterOrders(mFilterJson);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {

                        Intent a = new Intent(getActivity(), SpecificOrderActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.KEY_CHOOSED_ORDER, ordersList.get(position).getUuid());
                        a.putExtras(bundle);
                        startActivity(a);

                    }
                })
        );
        mToolBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent a = new Intent(getActivity(), FilterTabbedActivity.class);
                     startActivityForResult(a, Constants.REQUESTCODE_FILTER);

            }
        });

        return view;
    }

    public static FilteredOrdersFragment newInstance(String param1) {
        FilteredOrdersFragment fragment = new FilteredOrdersFragment();
        Bundle args = new Bundle();
        args.putString(Constants.BUNDLE_FILTER_STRING, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public FilteredOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mFilterString = getArguments().getString(Constants.BUNDLE_FILTER_STRING);
            if ( mFilterString !=null){
                try {
                    mFilterJson = new JSONObject(mFilterString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_filtered_orders, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.filter_action_close) {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.flContent, new OrdersFragment(), "NewFragmentTag");
            ft.commit();
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((MainActivity) getActivity()).setToolBar();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode==Constants.REQUESTCODE_FILTER)
            {
                if (data!=null && data.getStringExtra(Constants.BUNDLE_FILTER_STRING)!=null)
                {
                    String receivedFilterString = data.getStringExtra(Constants.BUNDLE_FILTER_STRING);
                    try {
                        JSONObject filterString = new JSONObject(receivedFilterString);
                        filterOrders(filterString);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        }
    }
    void filterOrders(JSONObject jsonObject) {
        Log.i(TAG + " received json", jsonObject.toString());
        Utils.sendNewJsonVolleyRequest(getActivity(), Request.Method.GET, Utils.getUrlWithParameters(Utils.getRequestUrl(getActivity(), WebRequestTypes.FILTER), jsonObject), null, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                JSONArray filteredResults = null;
                try {
                    filteredResults = new JSONArray(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ordersParsingDisplaying(filteredResults);

            }

            @Override
            public void onErrorResponse(String error) {

            }
        });
    }

    private void ordersParsingDisplaying(JSONArray jsonArray)
    {
        ordersList = (ArrayList<Order>) JSONParser.parseOrders(jsonArray);
        OrdersAdapter mAdapter = new OrdersAdapter(getActivity(), ordersList);
        mToolBar.setTitle(Integer.toString(mAdapter.getItemCount()) + " " +getString(R.string.text_filtered_results_found));
        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

}
