package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;

import java.util.Calendar;

import mobapply.freightexchange.library.Constants;

/**
 * Created by Ivan Dunskiy on 4/28/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * Date Picker for filtering date choosing
 */
public class DatePickerFragment  extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = " DatePickerFragment";

    public static DatePickerFragment newInstance(FilterFragment.FilterType type) {
        DatePickerFragment f = new DatePickerFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(Constants.DATETYPE_PICKER, type);
        Log.i(TAG + "DatePickerFragment type", type.toString());
        f.setArguments(args);
        return f;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        iDateSet = (IDateSet) activity;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (getArguments()!= null)
        {
            StringBuilder sb = new StringBuilder();
            int mMonth = month + 1 ;
            sb.append(Integer.toString(day)).append("-");
            if (mMonth > 9)
                sb.append(Integer.toString(mMonth)).append("-");
            else
                sb.append("0").append(Integer.toString(mMonth)).append("-");
            sb.append(Integer.toString(year));

            FilterFragment.FilterType dateTypes = (FilterFragment.FilterType)getArguments().getSerializable(Constants.DATETYPE_PICKER);
            Intent intent = new Intent();
            switch (dateTypes){
                case DATE_FROM:
                    intent.putExtra(Constants.BUNDLE_DATE_FROM, sb.toString());
                    break;
                case DATE_TO:
                    intent.putExtra(Constants.BUNDLE_DATE_TO, sb.toString());
                    break;
            }
            getTargetFragment().onActivityResult(getTargetRequestCode(),
                    Activity.RESULT_OK,
                    intent);
        }
    }


}
