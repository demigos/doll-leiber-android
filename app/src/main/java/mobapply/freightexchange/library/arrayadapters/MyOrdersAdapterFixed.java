package mobapply.freightexchange.library.arrayadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.model.SortingStates;

/**
 * Created by Ivan Dunskiy on 4/6/15.
 *
 *
 */
public class MyOrdersAdapterFixed extends ArrayAdapter<Order> {
    private List<Order> proposalsList;
    private Context mContext;

    private boolean isCityToHighlighted;
    private boolean isCityFromHighlighted;
    private boolean isCountryToHighlighted;
    private boolean isCountryFromHighlighted;
    private boolean isZipCodeFromHighlighted;
    private boolean isZipCodeToHighlighted;
    private boolean isGoodsHighlighted;
    private boolean isInfoHighlighted;
    private boolean isDateHighlighted;
    private boolean isWeightHighlighted;


    ViewHolder viewHolder = null;

    public MyOrdersAdapterFixed(Context context, int resource, List<Order> objects) {
        super(context, resource, objects);
        this.proposalsList = objects;
        this.mContext = context;
    }

    // Create new views (invoked by the layout manager)

    public void setContext(Context context){
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // ViewHolder буферизирует оценку различных полей шаблона элемента


        // Очищает сущетсвующий шаблон, если параметр задан
        // Работает только если базовый шаблон для всех классов один и тот же
        View rowView = convertView;
        if (rowView == null) {

            LayoutInflater inflater = LayoutInflater.from(mContext);
            rowView = inflater.inflate(R.layout.my_proposal_item, null, true);
            viewHolder = new ViewHolder();
            rowView.setTag(viewHolder);

            viewHolder.proposalId = (TextView) rowView.findViewById(R.id.proposal_id);

            viewHolder.proposalStatus = (TextView) rowView.findViewById(R.id.proposal_status);

            viewHolder.proposalFromCity = (TextView) rowView.findViewById(R.id.proposal_from_city);
            viewHolder.proposalFromCountry = (TextView) rowView.findViewById(R.id.proposal_from_country);
            viewHolder.proposalFromIndex = (TextView) rowView.findViewById(R.id.proposal_from_index);

            viewHolder.proposalToCity = (TextView) rowView.findViewById(R.id.proposal_to_city);
            viewHolder.proposalToCountry = (TextView) rowView.findViewById(R.id.proposal_to_country);
            viewHolder.proposalToIndex = (TextView) rowView.findViewById(R.id.proposal_to_index);

            viewHolder.proposalGoods = (TextView) rowView.findViewById(R.id.proposal_goods);
            viewHolder.proposalWeight = (TextView) rowView.findViewById(R.id.proposal_weight);
            viewHolder.proposalInfo = (TextView) rowView.findViewById(R.id.proposal_info);


        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        Order order = proposalsList.get(position);
        viewHolder.proposalId.setText(Utils.getFormattedDate(mContext, order.getAppointmentFrom())+  System.getProperty("line.separator") +
                Utils.getFormattedTime(mContext, order.getAppointmentFrom()));

        viewHolder.proposalFromCity.setText(order.getDepartureAddress().getCity());
        viewHolder.proposalFromCountry.setText(order.getDepartureAddress().getCountry());
         /*
         * Displaying only 2 digits in index if security option turn on
         */
        if (Utils.getCurrentConfiguration(mContext) !=null &&
                Utils.getCurrentConfiguration(mContext).isAnonymousOrders())
        {
            viewHolder.proposalFromIndex.setText(order.getDepartureAddress().getZipCode().substring(0,2));
        }
        else
            viewHolder.proposalFromIndex.setText(order.getDepartureAddress().getZipCode());


        viewHolder.proposalToCity.setText(order.getDestinationAddress().getCity());
        viewHolder.proposalToCountry.setText(order.getDestinationAddress().getCountry());

        if (Utils.getCurrentConfiguration(mContext) !=null &&
                Utils.getCurrentConfiguration(mContext).isAnonymousOrders()) {
            viewHolder.proposalToIndex.setText(order.getDestinationAddress().getZipCode().substring(0,2));
        }
        else
            viewHolder.proposalToIndex.setText(order.getDestinationAddress().getZipCode());

        viewHolder.proposalGoods.setText(order.getGood());
        viewHolder.proposalWeight.setText(Double.toString(order.getActualWeight()));
        viewHolder.proposalInfo.setText(order.getAdditionalInfo());

        String status = order.getOrderStatus();

        if (status != null && !status.isEmpty()) {

            String output = status.substring(0, 1).toUpperCase() + status.substring(1).toLowerCase();
            viewHolder.proposalStatus.setText(output);
        }

        if (isCityToHighlighted)
        {
            sortingStateView(SortingStates.CITY_TO);
        }
        if (isCityFromHighlighted)
        {
            sortingStateView(SortingStates.CITY_FROM);
        }
        if (isCountryFromHighlighted)
        {
            sortingStateView(SortingStates.COUNTRY_FROM);
        }
        if (isCountryToHighlighted)
        {
            sortingStateView(SortingStates.COUNTRY_TO);
        }
        if (isZipCodeToHighlighted)
        {
            sortingStateView(SortingStates.ZIPCODE_TO);
        }
        if (isZipCodeFromHighlighted)
        {
            sortingStateView(SortingStates.ZIPCODE_FROM);
        }
        if (isGoodsHighlighted) {
            sortingStateView(SortingStates.GOODS);
        }
        if (isInfoHighlighted){
            sortingStateView(SortingStates.INFO);
        }
        if (isDateHighlighted){
            sortingStateView(SortingStates.DATE);
        }
        if (isWeightHighlighted){
            sortingStateView(SortingStates.WEIGHT);
        }


        return rowView;
    }

    public void highlighLabel(SortingStates sortingStates) {
        switch (sortingStates){
            case CITY_TO:
                isCityToHighlighted = true;
                isCountryToHighlighted = false;
                isZipCodeToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case CITY_FROM:
                isCityFromHighlighted = true;
                isCountryFromHighlighted = false;
                isZipCodeFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case COUNTRY_TO:
                isCountryToHighlighted = true;
                isCityToHighlighted = false;
                isZipCodeToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case COUNTRY_FROM:
                isCountryFromHighlighted = true;
                isCityFromHighlighted = false;
                isZipCodeFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case ZIPCODE_FROM:
                isZipCodeFromHighlighted = true;
                isCountryFromHighlighted = false;
                isCityFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case ZIPCODE_TO:
                isZipCodeToHighlighted = true;
                isCityToHighlighted = false;
                isCountryToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case GOODS:
                isGoodsHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isInfoHighlighted = false;
                isDateHighlighted = false;
                isWeightHighlighted = false;
                break;
            case INFO:
                isInfoHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isWeightHighlighted = false;
                break;
            case DATE:
                isDateHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case WEIGHT:
                isWeightHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isInfoHighlighted = false;
                isDateHighlighted = false;
                break;

        }
        notifyDataSetChanged();

    }

    private void removeHighlightFrom()
    {
        viewHolder.proposalFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.proposalFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.proposalFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
    }
    private void removeHighlightTo(){
        viewHolder.proposalToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.proposalToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.proposalToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
    }
    private void highlightOffFrom(){
        isZipCodeFromHighlighted = false;
        isCountryFromHighlighted = false;
        isCityFromHighlighted = false;
    }
    private void highlightOffTo(){
        isZipCodeToHighlighted = false;
        isCityToHighlighted = false;
        isCountryToHighlighted = false;
    }

    private  void sortingStateView(SortingStates sortingStates)
    {
        switch (sortingStates){
            case CITY_TO:
                viewHolder.proposalToCity.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case CITY_FROM:
                viewHolder.proposalFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case COUNTRY_TO:
                viewHolder.proposalToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case COUNTRY_FROM:
                viewHolder.proposalFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case ZIPCODE_TO:
                viewHolder.proposalToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case ZIPCODE_FROM:
                viewHolder.proposalFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.proposalFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case DATE:
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case INFO:
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case WEIGHT:
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case GOODS:
                viewHolder.proposalGoods.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.proposalWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
        }
    }

    public static class ViewHolder  {

        private  TextView proposalStatus;
        public  TextView proposalId;
        private  TextView proposalFromCountry;
        private  TextView proposalFromIndex;
        private  TextView proposalToCity;
        private  TextView proposalToCountry;
        private  TextView proposalToIndex;
        private  TextView proposalFromCity;
        public  TextView proposalGoods;
        public  TextView proposalInfo;
        public TextView proposalWeight;
    }
    public void setProposalsList(List<Order> proposalsList){
        this.proposalsList = proposalsList;
        notifyDataSetChanged();
    }
}
