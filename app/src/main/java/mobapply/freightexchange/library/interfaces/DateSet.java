package mobapply.freightexchange.library.interfaces;

import mobapply.freightexchange.ui.fragments.FilterFragment;

/**
 * Created by Ivan Dunskiy on 4/28/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * Interface for Date object maintaining in FilterActivity corresponding to FilterType
 */
public interface DateSet {
     void onDateSet(FilterFragment.FilterType type , String date);
}
