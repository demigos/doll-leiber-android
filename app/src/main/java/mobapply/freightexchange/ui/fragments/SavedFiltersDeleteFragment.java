package mobapply.freightexchange.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.arrayadapters.FilterToDeleteAdapter;
import mobapply.freightexchange.library.interfaces.NestedFragmentsSwitcher;
import mobapply.freightexchange.model.FilterItem;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 */
public class SavedFiltersDeleteFragment extends Fragment implements AbsListView.OnItemClickListener
{

    private ArrayList<FilterItem> filterItems;
    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private FilterToDeleteAdapter mAdapter;
    private TextView mEmptyTextView;
    NestedFragmentsSwitcher nestedFragmentsSwitcher;

    public SavedFiltersDeleteFragment(NestedFragmentsSwitcher nestedFragmentsSwitcher) {
        this.nestedFragmentsSwitcher = nestedFragmentsSwitcher;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
         filterItems = Utils.loadFiltersList(getActivity());
         mAdapter = new FilterToDeleteAdapter(getActivity());
        if (filterItems!=null && !filterItems.isEmpty())
             mAdapter.addItems(filterItems);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
//        ((MainActivity)getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()));
        View view = inflater.inflate(R.layout.fragment_filteritem, container, false);

        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mEmptyTextView = (TextView) view.findViewById(android.R.id.empty);
        getActivity().invalidateOptionsMenu();
        updateUI();
        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_saved_filter_delete, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete_filter) {
            mAdapter.removeFiltersItems();
            if (mAdapter !=null && mAdapter.getCount()==0) {
                nestedFragmentsSwitcher.onSwitchToNextFragment();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            FilterItem currentFilterItem = (FilterItem)mAdapter.getItem(position);
            currentFilterItem.setHasToBeDeleted(!currentFilterItem.isHasToBeDeleted());
            mAdapter.notifyDataSetChanged();
    }

    public void updateUI() {
        getActivity().invalidateOptionsMenu();
        if (mAdapter != null ) {
            if (mAdapter.getCount() != 0)
            {
                mListView.setVisibility(View.VISIBLE);
                mEmptyTextView.setVisibility(View.GONE);
                mAdapter = new FilterToDeleteAdapter(getActivity());
                if (filterItems!=null && !filterItems.isEmpty()) {
                    mAdapter.addItems(filterItems);
                }
                mListView.setAdapter(mAdapter);
            }
            else
            {
                mEmptyTextView.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
            }
        }
    }

    public void setNestedFragmentsSwitcher(NestedFragmentsSwitcher nestedFragmentsSwitcher) {
        this.nestedFragmentsSwitcher = nestedFragmentsSwitcher;
    }



    public void backPressed() {
        nestedFragmentsSwitcher.onSwitchToNextFragment();
    }
}
