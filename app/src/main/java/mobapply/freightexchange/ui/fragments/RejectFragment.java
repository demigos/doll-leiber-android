package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class RejectFragment extends Fragment {
    String rejectAmount;

    public static RejectFragment newInstance(String uuid) {
        RejectFragment fragment = new RejectFragment();
        Bundle args = new Bundle();
        args.putString(Constants.BUNDLE_REJECT_AMOUNT, uuid);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            rejectAmount = getArguments().getString(Constants.BUNDLE_REJECT_AMOUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reject, container, false);
        TextView reject_price = (TextView) view.findViewById(R.id.reject_price);
        ImageView reject_image = (ImageView) view.findViewById(R.id.reject_image);
        Button btn_reject_ok = (Button) view.findViewById(R.id.btn_reject_ok);
        if (rejectAmount!=null)
            reject_price.setText(rejectAmount + " " + getString(R.string.euro_label));

        SVG svg = SVGParser.getSVGFromResource(getResources(), R.raw.ic_done_48px, 0xFF000000, 0xFFFFFFFF);
        Drawable drawable = svg.createPictureDrawable();
        reject_image.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        reject_image.setImageDrawable(drawable);

        btn_reject_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        });
        return view;
    }

}
