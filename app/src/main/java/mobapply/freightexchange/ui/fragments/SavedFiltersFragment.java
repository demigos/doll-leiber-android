package mobapply.freightexchange.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

import mobapply.freightexchange.R;

import mobapply.freightexchange.library.arrayadapters.FilterAdapter;
import mobapply.freightexchange.library.interfaces.NestedFragmentsSwitcher;
import mobapply.freightexchange.model.FilterItem;
import mobapply.freightexchange.ui.activities.FilterTabbedActivity;

import static mobapply.freightexchange.library.Utils.*;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 */
public class SavedFiltersFragment extends Fragment implements AbsListView.OnItemClickListener, NestedFragmentsSwitcher {

    private List<FilterItem> filterItems;
     NestedFragmentsSwitcher nestedFragmentsSwitcher;

    public SavedFiltersFragment(NestedFragmentsSwitcher nestedFragmentsSwitcher) {
        this.nestedFragmentsSwitcher = nestedFragmentsSwitcher;
    }

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private FilterAdapter mAdapter;
    private TextView mEmptyTextView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */


    public SavedFiltersFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
         filterItems = loadFiltersList(getActivity());
         mAdapter = new FilterAdapter(getActivity());
        if (filterItems!=null && !filterItems.isEmpty())
             mAdapter.addItems(filterItems);

    }

    @Override
    public void onStart() {
        super.onStart();
        updateUI();
    }

    private void updateUI() {
        filterItems = loadFiltersList(getActivity());
        if (filterItems != null && filterItems.size() != 0)
        {
            mListView.setVisibility(View.VISIBLE);
            mEmptyTextView.setVisibility(View.GONE);
            mAdapter = new FilterAdapter(getActivity());
            if (filterItems!=null && !filterItems.isEmpty())
                mAdapter.addItems(filterItems);
            mListView.setAdapter(mAdapter);
        }
        else
        {
            mEmptyTextView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (savedItemsExist())
            inflater.inflate(R.menu.menu_saved_filters, menu);
        else
        {
            menu.clear();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (savedItemsExist())
           getActivity().getMenuInflater().inflate(R.menu.menu_saved_filters, menu);
        else{
            menu.clear();
        }
        super.onPrepareOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (savedItemsExist()) {
            int id = item.getItemId();
            if (id == R.id.action_call_to_delete_filter) {
                nestedFragmentsSwitcher.onSwitchToNextFragment();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_savedfilters_list, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mEmptyTextView = (TextView) view.findViewById(android.R.id.empty);

        updateUI();

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FilterItem filterName = filterItems.get(position);
        FilterTabbedActivity.savedFilterItem = filterName;
        ((FilterTabbedActivity)getActivity()).setSavedFilter(filterName);
    }

    private boolean savedItemsExist() {
        boolean result = false;
        try {
            if (mAdapter!=null && mAdapter.getCount()>0)
                result = true;
        }catch (NullPointerException e){e.printStackTrace();}
        return result;
    }

    @Override
    public void onSwitchToNextFragment() {

    }

    public  void setNestedFragmentsSwitcher(NestedFragmentsSwitcher nestedFragmentsSwitcher) {
        this.nestedFragmentsSwitcher = nestedFragmentsSwitcher;
    }


}
