package mobapply.freightexchange.library.arrayadapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.model.SortingStates;

/**
 * Created by Ivan Dunskiy on 4/6/15.
   Copyright (c) 2015 Rarus. All rights reserved.
 */
public class OrdersAdapterFixed extends ArrayAdapter<Order>{
    private final List<Order> ordersList;
    private final Context mContext;
    ViewHolder viewHolder = null;
    private boolean isCityToHighlighted;
    private boolean isCityFromHighlighted;
    private boolean isCountryToHighlighted;
    private boolean isCountryFromHighlighted;
    private boolean isZipCodeFromHighlighted;
    private boolean isZipCodeToHighlighted;
    private boolean isGoodsHighlighted;
    private boolean isInfoHighlighted;
    private boolean isDateHighlighted;
    private TextView order_weight;
    private boolean isWeightHighlighted;

    public OrdersAdapterFixed(Context context, int resource, List<Order> objects) {
        super(context, resource, objects);
        this.ordersList = objects;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // ViewHolder буферизирует оценку различных полей шаблона элемента
        // Очищает сущетсвующий шаблон, если параметр задан
        // Работает только если базовый шаблон для всех классов один и тот же
        View rowView = convertView;
        if (rowView == null) {

            LayoutInflater inflater = LayoutInflater.from(mContext);
            rowView = inflater.inflate(R.layout.order_item, null, true);
            viewHolder = new ViewHolder();
            rowView.setTag(viewHolder);

            viewHolder.orderId = (TextView) rowView.findViewById(R.id.order_id);

            viewHolder.orderFromCity = (TextView) rowView.findViewById(R.id.order_from_city);
            viewHolder.orderFromCountry = (TextView) rowView.findViewById(R.id.order_from_country);
            viewHolder.orderFromIndex = (TextView) rowView.findViewById(R.id.order_from_index);

            viewHolder.orderToCity = (TextView) rowView.findViewById(R.id.order_to_city);
            viewHolder.orderToCountry = (TextView) rowView.findViewById(R.id.order_to_country);
            viewHolder.orderToIndex = (TextView) rowView.findViewById(R.id.order_to_index);

            viewHolder.orderGoods = (TextView) rowView.findViewById(R.id.order_goods);
            viewHolder.orderWeight = (TextView) rowView.findViewById(R.id.order_weight);
            viewHolder.orderInfo = (TextView) rowView.findViewById(R.id.order_info);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        Order order = ordersList.get(position);
        viewHolder.orderId.setText(Utils.getFormattedDate(mContext,order.getAppointmentFrom())+  System.getProperty("line.separator") +
                Utils.getFormattedTime(mContext, order.getAppointmentFrom()));

        viewHolder.orderFromCity.setText(order.getDepartureAddress().getCity());
        viewHolder.orderFromCountry.setText(order.getDepartureAddress().getCountry());
        /*
         * Displaying only 2 digits in index if security option turn on
         */
        if (Utils.getCurrentConfiguration(mContext) !=null &&
                Utils.getCurrentConfiguration(mContext).isAnonymousOrders())
        {
            viewHolder.orderFromIndex.setText(order.getDepartureAddress().getZipCode().substring(0,2));
        }
        else
            viewHolder.orderFromIndex.setText(order.getDepartureAddress().getZipCode());

        viewHolder.orderToCity.setText(order.getDestinationAddress().getCity());
        viewHolder.orderToCountry.setText(order.getDestinationAddress().getCountry());
        if (Utils.getCurrentConfiguration(mContext) !=null &&
                Utils.getCurrentConfiguration(mContext).isAnonymousOrders())
        {
            viewHolder.orderToIndex.setText(order.getDestinationAddress().getZipCode().substring(0,2));
        }
        else
            viewHolder.orderToIndex.setText(order.getDestinationAddress().getZipCode());

        viewHolder.orderWeight.setText(Double.toString(order.getActualWeight()));
        viewHolder.orderGoods.setText(order.getGood());
        viewHolder.orderInfo.setText(order.getAdditionalInfo());

        if (isCityToHighlighted)
        {
            sortingStateView(SortingStates.CITY_TO);
        }
        if (isCityFromHighlighted)
        {
            sortingStateView(SortingStates.CITY_FROM);
        }
        if (isCountryFromHighlighted)
        {
            sortingStateView(SortingStates.COUNTRY_FROM);
        }
        if (isCountryToHighlighted)
        {
            sortingStateView(SortingStates.COUNTRY_TO);
        }
        if (isZipCodeToHighlighted)
        {
            sortingStateView(SortingStates.ZIPCODE_TO);
        }
        if (isZipCodeFromHighlighted)
        {
            sortingStateView(SortingStates.ZIPCODE_FROM);
        }
        if (isGoodsHighlighted) {
            sortingStateView(SortingStates.GOODS);
        }
        if (isInfoHighlighted){
            sortingStateView(SortingStates.INFO);
        }
        if (isDateHighlighted){
            sortingStateView(SortingStates.DATE);
        }
        if (isWeightHighlighted){
            sortingStateView(SortingStates.WEIGHT);
        }

        return rowView;
    }

    public void highlighLabel(SortingStates sortingStates) {
        switch (sortingStates){
            case CITY_TO:
                isCityToHighlighted = true;
                isCountryToHighlighted = false;
                isZipCodeToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case CITY_FROM:
                isCityFromHighlighted = true;
                isCountryFromHighlighted = false;
                isZipCodeFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case COUNTRY_TO:
                isCountryToHighlighted = true;
                isCityToHighlighted = false;
                isZipCodeToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case COUNTRY_FROM:
                isCountryFromHighlighted = true;
                isCityFromHighlighted = false;
                isZipCodeFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case ZIPCODE_FROM:
                isZipCodeFromHighlighted = true;
                isCountryFromHighlighted = false;
                isCityFromHighlighted = false;
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case ZIPCODE_TO:
                isZipCodeToHighlighted = true;
                isCityToHighlighted = false;
                isCountryToHighlighted = false;
                highlightOffFrom();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case GOODS:
                isGoodsHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isInfoHighlighted = false;
                isDateHighlighted = false;
                isWeightHighlighted = false;
                break;
            case INFO:
                isInfoHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isDateHighlighted = false;
                isWeightHighlighted = false;
                break;
            case DATE:
                isDateHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isInfoHighlighted = false;
                isWeightHighlighted = false;
                break;
            case WEIGHT:
                isWeightHighlighted = true;
                highlightOffFrom();
                highlightOffTo();
                isGoodsHighlighted = false;
                isInfoHighlighted = false;
                isDateHighlighted = false;
                break;

        }
        notifyDataSetChanged();
        
    }


    static class ViewHolder {

        private  TextView orderFromCountry;
        private  TextView orderFromIndex;
        private  TextView orderToCity;
        private  TextView orderToCountry;
        private  TextView orderToIndex;
        private  TextView orderFromCity;
        public   TextView orderId;
        public   TextView orderGoods;
        public   TextView orderInfo;

        public TextView orderWeight;
    }

    private void removeHighlightFrom()
    {
        viewHolder.orderFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.orderFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.orderFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
    }
    private void removeHighlightTo(){
        viewHolder.orderToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.orderToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
        viewHolder.orderToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
    }
    private void highlightOffFrom(){
        isZipCodeFromHighlighted = false;
        isCountryFromHighlighted = false;
        isCityFromHighlighted = false;
    }
    private void highlightOffTo(){
        isZipCodeToHighlighted = false;
        isCityToHighlighted = false;
        isCountryToHighlighted = false;
    }

    private  void sortingStateView(SortingStates sortingStates)
    {
        switch (sortingStates){
            case CITY_TO:
                viewHolder.orderToCity.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.orderToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case CITY_FROM:
                viewHolder.orderFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.orderFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case COUNTRY_TO:
                viewHolder.orderToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.orderToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case COUNTRY_FROM:
                viewHolder.orderFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.orderFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case ZIPCODE_TO:
                viewHolder.orderToIndex.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.orderToCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderToCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightFrom();
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case ZIPCODE_FROM:
                viewHolder.orderFromIndex.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                viewHolder.orderFromCountry.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderFromCity.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                removeHighlightTo();
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case DATE:
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case INFO:
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case WEIGHT:
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
            case GOODS:
                viewHolder.orderGoods.setBackgroundColor(mContext.getResources().getColor(R.color.gray_textview_highlight));
                removeHighlightTo();
                removeHighlightFrom();
                viewHolder.orderWeight.setBackgroundColor(mContext.getResources().getColor(R.color.text_white_color));
                break;
        }
    }

}
