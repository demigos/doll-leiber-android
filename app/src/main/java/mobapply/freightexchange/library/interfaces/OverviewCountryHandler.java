package mobapply.freightexchange.library.interfaces;

/**
 * Created by Ivan Dunskiy on 5/23/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * Interface for tranfering choosed direction from
 *  {@link mobapply.freightexchange.customviews.TableMainLayout}
 *  to
 *  {@link mobapply.freightexchange.ui.fragments.OverviewByCountryFragment}
 */
public interface OverviewCountryHandler {
    void onDirectionChoosed(String fromCountry, String toCountry);
}
