package mobapply.freightexchange.views;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.MyProposal;
import mobapply.freightexchange.model.statuses.ProposalStatuses;
import mobapply.freightexchange.ui.activities.SpecificOrderActivity;

/**
 * Created by Ivan Dunskiy on 6/15/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class SpecificProposalView {
    private final Context mContext;
    private EditText edit_proposal;
    private TextView specific_order_load;
    private TextView specific_order_weight_goods;
    private TextView specific_order_number;
    private TextView specific_order_dpre_date;
    private TextView specific_order_contact;
    private TextView specific_order_pac_quantity;
    private TextView specific_order_type;
    private TextView specific_order_charge_weight;
    private TextView specific_order_appoint_till;
    private TextView specific_order_dispocode;
    private TextView specific_order_volume_weight;
    private TextView specific_order_loading_metre;
    private TextView specific_order_volume;
    private TextView specific_order_hazpoints;
    private TextView specific_additional_info;
    private AuthenticationRequester authenticationRequester;
    private MyProposal mSpecificProposal;
    private String uuid;
    private Button btn_reject_proposal;
    private EditText specific_order_edit;
    private TextView proposal_price;
    private TextView specific_order_dpre_city;
    private TextView specific_order_dest_city;

    private TextView specific_order_from_city;
    private TextView specific_order_from_country;
    private TextView specific_order_from_index;
    private TextView specific_order_to_city;
    private TextView specific_order_to_country;
    private TextView specific_order_to_index;
    private LinearLayout specific_proposal_btn_layout;
    private String costOfFreight;
    private SharedPreferences preferences;

    public SpecificProposalView(Context context, View view)
    {
        mContext = context;
        preferences = mContext.getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        specific_order_load = (TextView)view.findViewById(R.id.specific_order_load);

        specific_order_weight_goods = (TextView)view.findViewById(R.id.specific_order_weight_goods);

        specific_order_from_city  = (TextView)view.findViewById(R.id.specific_order_from_city);
        specific_order_from_country  = (TextView)view.findViewById(R.id.specific_order_from_country);
        specific_order_from_index  = (TextView)view.findViewById(R.id.specific_order_from_index);

        specific_order_to_city  = (TextView)view.findViewById(R.id.specific_order_to_city);
        specific_order_to_country  = (TextView)view.findViewById(R.id.specific_order_to_country);
        specific_order_to_index  = (TextView)view.findViewById(R.id.specific_order_to_index);

        btn_reject_proposal = (Button)view.findViewById(R.id.btn_action_proposal);
        btn_reject_proposal.setText(mContext.getString(R.string.btn_reject_proposal));
        /*
         * Defining the specific order fields
         */
        specific_order_number = (TextView)view.findViewById(R.id.specific_order_number);
        specific_order_dpre_date = (TextView)view.findViewById(R.id.specific_order_dpre_date);
        specific_order_contact = (TextView)view.findViewById(R.id.specific_order_contact);
        specific_order_pac_quantity = (TextView)view.findViewById(R.id.specific_order_quantity);
        specific_order_type = (TextView)view.findViewById(R.id.specific_order_type);
        specific_order_charge_weight = (TextView)view.findViewById(R.id.specific_order_charge_weight);
        specific_order_appoint_till = (TextView)view.findViewById(R.id.specific_order_appoint_till);
        specific_order_dispocode = (TextView)view.findViewById(R.id.specific_order_dispocode);
        specific_order_volume_weight = (TextView)view.findViewById(R.id.specific_order_volume_weight);
        specific_order_loading_metre = (TextView)view.findViewById(R.id.specific_order_loading_metre);
        specific_order_volume = (TextView)view.findViewById(R.id.specific_order_volume);
        specific_order_hazpoints = (TextView)view.findViewById(R.id.specific_order_hazpoints);
        specific_additional_info = (TextView)view.findViewById(R.id.specific_additional_info);
        specific_order_dpre_city= (TextView)view.findViewById(R.id.specific_order_dpre_city);
        specific_order_dest_city= (TextView)view.findViewById(R.id.specific_order_dest_city);
        proposal_price= (TextView)view.findViewById(R.id.proposal_price);

        specific_order_edit  = (EditText)view.findViewById(R.id.proposal_edit);

        specific_proposal_btn_layout = (LinearLayout)view.findViewById(R.id.specific_proposal_btn_layout);
        specific_order_edit.setVisibility(View.GONE);
    }

    public void displayProposal(MyProposal mProposal) {
        mSpecificProposal = mProposal;
        ((SpecificOrderActivity)mContext).getSupportActionBar().setTitle(mContext.getString(R.string.specific_order_title) + " "+mProposal.getMyProposalOrder().getNumber());
        if ( !mProposal.getStatus().equalsIgnoreCase(ProposalStatuses.PENDING.name()))
            specific_proposal_btn_layout.setVisibility(View.GONE);
        else
            specific_proposal_btn_layout.setVisibility(View.VISIBLE);

        specific_order_load.setText(Utils.getFormattedDate(mContext,mProposal.getMyProposalOrder().getAppointmentFrom()));

        if (mProposal.getMyProposalOrder().getDepartureAddress()!=null )
        {
            specific_order_from_city.setText(mProposal.getMyProposalOrder().getDepartureAddress().getCity());
            specific_order_from_country.setText(mProposal.getMyProposalOrder().getDepartureAddress().getCountry());
            specific_order_from_index.setText(mProposal.getMyProposalOrder().getDepartureAddress().getZipCode());
        }
        if (mProposal.getMyProposalOrder().getDestinationAddress()!=null )
        {
            specific_order_to_city.setText(mProposal.getMyProposalOrder().getDestinationAddress().getCity());
            specific_order_to_country.setText(mProposal.getMyProposalOrder().getDestinationAddress().getCountry());
            specific_order_to_index.setText(mProposal.getMyProposalOrder().getDestinationAddress().getZipCode());
        }
        final SpannableStringBuilder sb = new SpannableStringBuilder(Double.toString(mProposal.getMyProposalOrder().getActualWeight()) + " kg of " +
                mProposal.getMyProposalOrder().getGood());
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(bss, 0, Double.toString(mProposal.getMyProposalOrder().getActualWeight()).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);


        specific_order_weight_goods.setText(sb);

        proposal_price.setText("  " + Float.toString(mProposal.getCostOfFreight()) + "  " + proposal_price.getText());
        costOfFreight = Float.toString(mProposal.getCostOfFreight());
        specific_order_number.setText("# " + mProposal.getMyProposalOrder().getNumber());
        specific_order_dpre_date.setText(Utils.getFormattedDate(mContext, mProposal.getMyProposalOrder().getAppointmentFrom()));

        specific_order_dpre_city.setText(mProposal.getMyProposalOrder().getDepartureAddress().getCity());
        specific_order_dest_city.setText(mProposal.getMyProposalOrder().getDestinationAddress().getCity());

        specific_order_contact.setText(mProposal.getMyProposalOrder().getCustomerContactPerson());
        specific_order_pac_quantity.setText(Integer.toString(mProposal.getMyProposalOrder().getPackageQuantity()));
        specific_order_type.setText(mProposal.getMyProposalOrder().getPackageType());
        specific_order_charge_weight.setText(Double.toString(mProposal.getMyProposalOrder().getChargeWeight()));
        specific_order_appoint_till.setText(Utils.getFormattedDate(mContext, mProposal.getMyProposalOrder().getAppointmentTill()));
        specific_order_dispocode.setText(mProposal.getMyProposalOrder().getDispositionCode());
        specific_order_volume_weight.setText(Double.toString(mProposal.getMyProposalOrder().getVolumeWeight()));
        specific_order_loading_metre.setText(Double.toString(mProposal.getMyProposalOrder().getLoadingMetre()));
        specific_order_volume.setText(Double.toString(mProposal.getMyProposalOrder().getVolume()));
        specific_order_hazpoints.setText(Integer.toString(mProposal.getMyProposalOrder().getHazmatPoints()));
        specific_additional_info.setText(mProposal.getMyProposalOrder().getAdditionalInfo());

    }

    public MyProposal getmSpecificProposal() {
        return mSpecificProposal;
    }
}
