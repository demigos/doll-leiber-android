package mobapply.freightexchange.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;


import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.ui.activities.MainActivity;

/**
 * Created by Ivan Dunskiy on 4/28/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * DialogFragment for logging out operation
 */
public class LogOutFragment extends DialogFragment {

    private SharedPreferences preferences;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View layout = inflater.inflate(R.layout.fragment_log_out, null);
        preferences= getActivity().getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(layout)
                .setIcon(R.drawable.ic_launcher)
                .setTitle(getString(R.string.text_log_out))
                .create();
        Button btn_log_out_yes = (Button) layout.findViewById(R.id.btn_log_out_yes);
        Button btn_log_out_no = (Button) layout.findViewById(R.id.btn_log_out_no);
        btn_log_out_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogOutFragment.this.dismiss();
                ((MainActivity) getActivity()).syncDrawerState();
            }
        });
        btn_log_out_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferences.edit().remove(Constants.SHARED_CURRENT_USERNAME).commit();
                preferences.edit().remove(Constants.SHARED_CURRENT_ACCESS_TOKEN).commit();
                getActivity().finish();
                getActivity().overridePendingTransition(0, R.anim.top_slide_in_down);
            }
        });
        return alertDialog;

    }


}
