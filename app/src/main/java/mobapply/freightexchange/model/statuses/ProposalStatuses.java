package mobapply.freightexchange.model.statuses;

/**
 * Created by Ivan Dunskiy on 4/18/15.
 *
 * Enum for possible My Proposal statuses
 */
public  enum  ProposalStatuses {
    PENDING{
        @Override
        public String toString() {
           return "pending";
        }
    },
    ACCEPTED{
        @Override
        public String toString() {
            return "accepted";
        }
    },
    CANCELLED{
        @Override
        public String toString() {
            return "cancelled";
        }
    },
    REJECTED{
        @Override
        public String toString() {
            return "rejected";
        }
    },
    REPLACED{
        @Override
        public String toString() {
            return "replaced";
        }
    },
    REFUSED{
        @Override
        public String toString() {
            return "refused";
        }
    },
    NOT_ACTUAL{
        @Override
        public String toString() {
            return "not_actual";
        }
    },
    FAILED{
        @Override
        public String toString() {
            return "failed";
        }
    },

}
