package mobapply.freightexchange.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.gc.materialdesign.views.ButtonFloat;

import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.arrayadapters.MyOrdersAdapterFixed;
import mobapply.freightexchange.library.interfaces.SortingItems;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.model.SortingStates;
import mobapply.freightexchange.ui.activities.FilterTabbedActivity;
import mobapply.freightexchange.ui.activities.SpecificOrderActivity;

/**
 * Created by Ivan Dunskiy on 6/4/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 *
 * View class for {@link mobapply.freightexchange.ui.fragments.OrdersFragment} implementation
 */
public class MyOrdersView implements View.OnClickListener{


    private static final String TAG = "OrdersView" ;
    private final ListView mListView;
    private final ButtonFloat mBtn_float_orderlist;
    private final AutoCompleteTextView label_order_goods;
    private final AutoCompleteTextView label_order_to;
    private final AutoCompleteTextView label_order_from;
    private Context mContext;
    List<Order> mMyOrderList;
    MyOrdersAdapterFixed mAdapter;

    SortingItems sortingItems;


    public MyOrdersView(Context context, View rootView) {
        mContext = context;
        mListView = (ListView) rootView.findViewById(R.id.orders_recycler_view);
        label_order_goods = (AutoCompleteTextView) rootView.findViewById(R.id.label_order_goods);
        label_order_to = (AutoCompleteTextView) rootView.findViewById(R.id.label_order_to);
        label_order_from = (AutoCompleteTextView) rootView.findViewById(R.id.label_order_from);
        mBtn_float_orderlist = (ButtonFloat) rootView.findViewById(R.id.btn_float_orderlist);
        mBtn_float_orderlist.setBackgroundColor(Color.parseColor("#f40d8a"));
        mBtn_float_orderlist.setRippleColor(Color.parseColor("#f40d8a"));

        label_order_to.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                label_order_to.setText(mContext.getString(R.string.label_to));
                if (position==0)
                {
                    mAdapter.highlighLabel(SortingStates.CITY_TO);
                    sortingItems.sortItems(SortingStates.CITY_TO);
                }
                else if (position == 1)
                {
                    mAdapter.highlighLabel(SortingStates.COUNTRY_TO);
                    sortingItems.sortItems(SortingStates.COUNTRY_TO);
                }
                else
                {
                    mAdapter.highlighLabel(SortingStates.ZIPCODE_TO);
                    sortingItems.sortItems(SortingStates.ZIPCODE_TO);
                }

            }
        });
        label_order_from.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                label_order_from.setText(mContext.getString(R.string.label_from));
                if (position==0)
                {
                    mAdapter.highlighLabel(SortingStates.CITY_FROM);
                    sortingItems.sortItems(SortingStates.CITY_FROM);
                }
                else if (position == 1)
                {
                    mAdapter.highlighLabel(SortingStates.COUNTRY_FROM);
                    sortingItems.sortItems(SortingStates.COUNTRY_FROM);
                }
                else
                {
                    mAdapter.highlighLabel(SortingStates.ZIPCODE_FROM);
                    sortingItems.sortItems(SortingStates.ZIPCODE_FROM);
                }

            }
        });

        label_order_goods.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                label_order_goods.setText(mContext.getString(R.string.label_goods));
                if (position == 0) {
                    mAdapter.highlighLabel(SortingStates.GOODS);
                    sortingItems.sortItems(SortingStates.GOODS);
                } else
                {
                    mAdapter.highlighLabel(SortingStates.WEIGHT);
                    sortingItems.sortItems(SortingStates.WEIGHT);
                }

            }
        });
    }

    public void displayOrders(final Context mContext, final List<Order> orderList)
    {

        mBtn_float_orderlist.setOnClickListener(this);
        mMyOrderList = orderList;
        mAdapter = new MyOrdersAdapterFixed(mContext, R.layout.my_proposal_item, orderList);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent a = new Intent(mContext, SpecificOrderActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CHOOSED_MY_ORDER, orderList.get(position).getUuid());
                a.putExtras(bundle);
                mContext.startActivity(a);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_float_orderlist:
                Intent a = new Intent(mContext, FilterTabbedActivity.class);
                ((Activity)mContext).startActivityForResult(a, Constants.REQUESTCODE_FILTER);
                break;
        }
    }

    public List<Order> getOrderList() {
        return mMyOrderList;
    }

    public MyOrdersAdapterFixed getOrdersAdapter() {
        return mAdapter;
    }

    public void notifyDataChanged() {
        mAdapter.notifyDataSetChanged();

    }

    public void invalidateSortingArrays() {
            String[] goodsArray =  mContext.getResources().getStringArray(R.array.sort_goods);
            ArrayAdapter goodsSpinnerArrayAdapter = new ArrayAdapter(mContext,
                    R.layout.spinner_item_layout,goodsArray
            );
            label_order_goods.setDropDownWidth(getTheMostWideText(goodsArray));
            label_order_goods.setAdapter(goodsSpinnerArrayAdapter);
            String[] locationsArray =  mContext.getResources().getStringArray(R.array.sort_locations);
            final ArrayAdapter fromSpinnerArrayAdapter = new ArrayAdapter(mContext,
                    R.layout.spinner_item_layout,
                    locationsArray);
            label_order_to.setDropDownWidth(getTheMostWideText(locationsArray));
            label_order_to.setAdapter(fromSpinnerArrayAdapter);
            label_order_from.setDropDownWidth(getTheMostWideText(locationsArray));
            label_order_from.setAdapter(fromSpinnerArrayAdapter);

    }

    private int getTheMostWideText(String[] array){
        int theMostWide = 0;
        for (int i = 0; i < array.length-1; i++)
        {
            if (array[i].length() > array[i+1].length())
            {
                theMostWide = array[i].length();
            }
            else
                theMostWide = array[i+1].length();
        }
        return  theMostWide * 25;
    }
    public void setiSortingItems(SortingItems sortingItems) {
        this.sortingItems = sortingItems;
    }
}
