package mobapply.freightexchange.library;

import com.google.android.gms.maps.model.LatLng;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mobapply.freightexchange.model.Order;

/**
 * Created by Ivan Dunskiy on 5/15/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * This class serves for orders interaction, with synchronization implementation
 */
public class OrderManager {
    private static Map<String, Order> orders ;
//    private static OrderManager  mInstance = new OrderManager();
    private static Map<String, LatLng> latLngMap ;
    private static OrderManager  mInstance;
    public static OrderManager getInstance() {
        if (mInstance == null) {
            synchronized (OrderManager.class) {
                if (mInstance == null) {
                    mInstance = new OrderManager();
                    latLngMap = Collections.synchronizedMap(new HashMap<String, LatLng>());
                    orders = Collections.synchronizedMap(
                            new HashMap<String, Order>());
                }
            }
        }
        return mInstance;
    }


    public synchronized void applyGeoData(int flag , String uuid, LatLng latLng) {
        Order foundOrder = orders.get(uuid);
        switch (flag){
            case Constants.FLAG_ADDRESS_DEPARTURE:
                foundOrder.setDeparturePoint(latLng);
                break;
            case Constants.FLAG_ADDRESS_DESTINATION:
                foundOrder.setDestinationPoint(latLng);
                break;
        }
    }

    public synchronized  Map<String, Order> getOrders() {
        return orders;
    }

    public synchronized   Map<String, LatLng> getLatLngMap() {
        return latLngMap;
    }
}
