package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.Toast;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.ui.activities.MainActivity;


public class SettingsFragment extends Fragment implements View.OnClickListener {
    private CheckedTextView settings_send_reports;
    private CheckedTextView settings_overview_by_country;
    private Button btn_save_settings;
    private SharedPreferences preferences;

    boolean mOverviewByCountryFlag;
    boolean mSendReportsFlag;

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_settings, container, false);
        preferences = getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);

        settings_send_reports = (CheckedTextView)view.findViewById(R.id.settings_send_reports);
        settings_overview_by_country = (CheckedTextView)view.findViewById(R.id.settings_overview_by_country);
        btn_save_settings = (Button)view.findViewById(R.id.btn_save_settings);

        settings_send_reports.setOnClickListener(this);
        settings_overview_by_country.setOnClickListener(this);
        btn_save_settings.setOnClickListener(this);


        boolean isOverviewChecked = preferences.getBoolean(Constants.SHARED_SETTINGS_OVERVIEW, false);
        settings_overview_by_country.setChecked(isOverviewChecked);

        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.settings_send_reports:
                    mSendReportsFlag = !mSendReportsFlag;
                    settings_send_reports.setChecked(mSendReportsFlag);
              break;
            case R.id.settings_overview_by_country:
                    mOverviewByCountryFlag = !mOverviewByCountryFlag;
                    settings_overview_by_country.setChecked(mOverviewByCountryFlag);
              break;
            case R.id.btn_save_settings:
                if (mOverviewByCountryFlag){
                    preferences.edit().putBoolean(Constants.SHARED_SETTINGS_OVERVIEW, true).commit();
                }
                else
                    preferences.edit().putBoolean(Constants.SHARED_SETTINGS_OVERVIEW, false).commit();
                if (mSendReportsFlag) {
                    preferences.edit().putBoolean(Constants.SHARED_SETTINGS_CRASH_REPORTS, true).commit();
                }

                Toast.makeText(getActivity(), getString(R.string.toast_settings_save), Toast.LENGTH_SHORT).show();
                ((MainActivity)getActivity()).syncDrawerState();
              break;
        }
    }



}
