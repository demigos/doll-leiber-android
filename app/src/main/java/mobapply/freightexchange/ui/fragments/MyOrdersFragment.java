package mobapply.freightexchange.ui.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.interfaces.SortingItems;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.model.SortingDirections;
import mobapply.freightexchange.model.SortingStates;
import mobapply.freightexchange.providers.MyOrdersListProvider;
import mobapply.freightexchange.ui.activities.MainActivity;
import mobapply.freightexchange.views.MyOrdersView;

public class MyOrdersFragment extends Fragment implements MyOrdersListProvider.MyOrdersCallback, View.OnClickListener, SortingItems {

    private MyOrdersView mOrdersView;
    private MyOrdersListProvider mOrdersProvider;
    private SwipeRefreshLayout mSwipeContainer;

    private TextView label_order_date;
    private AutoCompleteTextView label_order_goods;
    private TextView label_order_info;
    private AutoCompleteTextView label_order_to;
    private AutoCompleteTextView label_order_from;

    boolean mDateDecrease;
    boolean mAdditionalDecrease;
    boolean mCityToDecrease;
    private boolean mCountryToDecrease;
    private boolean mCityFromDecrease;
    private boolean  mCountryFromDecrease;
    private boolean mZipcodeFromDecrease;
    private boolean mZipcodeToDecrease;
    private boolean mGoodsToDecrease;
    private boolean mWeightToDecrease;

    public static MyOrdersFragment newInstance() {
        MyOrdersFragment fragment = new MyOrdersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_orders_list, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.text_fragment_my_orders_title));
        ((MainActivity) getActivity()).getSupportActionBar().show();
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        mSwipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        this.setHasOptionsMenu(true);

        mOrdersView = new MyOrdersView(getActivity(), view);
        mOrdersView.setiSortingItems(this);
        mOrdersProvider = new MyOrdersListProvider(getActivity());
        mOrdersProvider.loadMyOrders(this);

        label_order_date = (TextView) view.findViewById(R.id.label_order_date);
        label_order_goods = (AutoCompleteTextView) view.findViewById(R.id.label_order_goods);
        label_order_info = (TextView) view.findViewById(R.id.label_order_info);
        label_order_to = (AutoCompleteTextView) view.findViewById(R.id.label_order_to);
        label_order_from = (AutoCompleteTextView) view.findViewById(R.id.label_order_from);

        label_order_date.setOnClickListener(this);
        label_order_goods.setOnClickListener(this);
        label_order_info.setOnClickListener(this);
        label_order_to.setOnClickListener(this);
        label_order_from.setOnClickListener(this);

        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                mSwipeContainer.setRefreshing(false);
                mOrdersProvider.loadMyOrders(MyOrdersFragment.this);
            }
        });
        return view;

    }

    @Override
    public void onMyOrdersLoaded(List<Order> orderList) {
        mOrdersView.displayOrders(getActivity(), orderList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.label_order_date:
                if (!mDateDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        public int compare(Order o1, Order o2) {
                            if (o1.getAppointmentFrom() == 0 || o2.getAppointmentFrom() == 0)
                                return 0;
                            return Long.valueOf(o1.getAppointmentFrom()).compareTo(Long.valueOf(o2.getAppointmentFrom()));
                        }
                    });
                    mOrdersView.getOrdersAdapter().highlighLabel(SortingStates.DATE);
                    mOrdersView.notifyDataChanged();
                    setSortingImage(label_order_date, SortingDirections.INCREASE);
                    mDateDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        public int compare(Order o1, Order o2) {
                            if (o1.getAppointmentFrom() == 0 || o2.getAppointmentFrom() == 0)
                                return 0;
                            return Long.valueOf(o2.getAppointmentFrom()).compareTo(Long.valueOf(o1.getAppointmentFrom()));
                        }
                    });
                    mOrdersView.notifyDataChanged();
                    setSortingImage(label_order_date,SortingDirections.DECREASE);
                    mDateDecrease = false;
                }
                break;
            case R.id.label_order_goods:
                mOrdersView.invalidateSortingArrays();
                label_order_goods.showDropDown();
                break;

            case R.id.label_order_to:
                mOrdersView.invalidateSortingArrays();
                label_order_to.showDropDown();
                break;

            case R.id.label_order_from:
                mOrdersView.invalidateSortingArrays();
                label_order_from.showDropDown();
                break;

            case R.id.label_order_info:
                if (!mAdditionalDecrease)
                {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s1.getAdditionalInfo().compareToIgnoreCase(s2.getAdditionalInfo());
                        }
                    });
                    setSortingImage(label_order_info,SortingDirections.INCREASE);
                    mAdditionalDecrease = true;
                }
                else
                {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s2.getAdditionalInfo().compareToIgnoreCase(s1.getAdditionalInfo());
                        }
                    });
                    setSortingImage(label_order_info,SortingDirections.DECREASE);
                    mAdditionalDecrease = false;
                }
                mOrdersView.getOrdersAdapter().highlighLabel(SortingStates.INFO);
                mOrdersView.notifyDataChanged();
                break;
        }
    }

    private void setSortingImage(TextView textView, SortingDirections directions){
        switch (directions){
            case INCREASE:
                Drawable img = getResources().getDrawable( R.drawable.ic_sort_increasing );
                int h = img.getIntrinsicHeight();
                int w = img.getIntrinsicWidth();
                img.setBounds( 0, 0, w, h );
                textView.setCompoundDrawables( img, null,null , null);
                break;
            case DECREASE:
                Drawable img2 = getResources().getDrawable( R.drawable.ic_sort_decreasing );
                int h2 = img2.getIntrinsicHeight();
                int w2 = img2.getIntrinsicWidth();
                img2.setBounds( 0, 0, w2, h2 );
                textView.setCompoundDrawables(img2 , null, null , null);
                break;
        }
        switch (textView.getId()){
            case R.id.label_order_date:
                label_order_from.setCompoundDrawables( null, null, null, null);
                label_order_to.setCompoundDrawables( null, null, null, null);
                label_order_goods.setCompoundDrawables( null, null, null, null);
                label_order_info.setCompoundDrawables( null, null, null, null);
                break;
            case R.id.label_order_from:
                label_order_date.setCompoundDrawables( null, null, null, null);
                label_order_to.setCompoundDrawables( null, null, null, null);
                label_order_goods.setCompoundDrawables( null, null, null, null);
                label_order_info.setCompoundDrawables( null, null, null, null);
                break;
            case R.id.label_order_to:
                label_order_date.setCompoundDrawables( null, null, null, null);
                label_order_from.setCompoundDrawables( null, null, null, null);
                label_order_goods.setCompoundDrawables( null, null, null, null);
                label_order_info.setCompoundDrawables( null, null, null, null);
                break;
            case R.id.label_order_goods:
                label_order_date.setCompoundDrawables( null, null, null, null);
                label_order_from.setCompoundDrawables( null, null, null, null);
                label_order_to.setCompoundDrawables( null, null, null, null);
                label_order_info.setCompoundDrawables( null, null, null, null);
                break;
            case R.id.label_order_info:
                label_order_date.setCompoundDrawables( null, null, null, null);
                label_order_from.setCompoundDrawables( null, null, null, null);
                label_order_to.setCompoundDrawables( null, null, null, null);
                label_order_goods.setCompoundDrawables( null, null, null, null);
                break;

        }

    }


    @Override
    public void sortItems(SortingStates sortingStates) {
        switch (sortingStates){
            case CITY_TO:
                if (!mCityToDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s1.getDestinationAddress().getCity().compareToIgnoreCase(s2.getDestinationAddress().getCity());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.INCREASE);
                    mCityToDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s2.getDestinationAddress().getCity().compareToIgnoreCase(s1.getDestinationAddress().getCity());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.DECREASE);
                    mCityToDecrease = false;
                }
                mOrdersView.notifyDataChanged();
                break;
            case CITY_FROM:
                if (!mCityFromDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s1.getDepartureAddress().getCity().compareToIgnoreCase(s2.getDepartureAddress().getCity());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.INCREASE);
                    mCityFromDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s2.getDepartureAddress().getCity().compareToIgnoreCase(s1.getDepartureAddress().getCity());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.DECREASE);
                    mCityFromDecrease = false;
                }
                mOrdersView.notifyDataChanged();
                break;
            case COUNTRY_TO:
                if (!mCountryToDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s1.getDestinationAddress().getCountry().compareToIgnoreCase(s2.getDestinationAddress().getCountry());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.INCREASE);
                    mCountryToDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s2.getDestinationAddress().getCountry().compareToIgnoreCase(s1.getDestinationAddress().getCountry());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.DECREASE);
                    mCountryToDecrease = false;
                }
                mOrdersView.notifyDataChanged();
                break;
            case COUNTRY_FROM:
                if (!mCountryFromDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s1.getDepartureAddress().getCountry().compareToIgnoreCase(s2.getDepartureAddress().getCountry());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.INCREASE);
                    mCountryFromDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s2.getDepartureAddress().getCountry().compareToIgnoreCase(s1.getDepartureAddress().getCountry());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.DECREASE);
                    mCountryFromDecrease = false;
                }
                mOrdersView.notifyDataChanged();
                break;
            case ZIPCODE_FROM:
                if (!mZipcodeFromDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s1.getDepartureAddress().getZipCode().compareToIgnoreCase(s2.getDepartureAddress().getZipCode());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.INCREASE);
                    mZipcodeFromDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s2.getDepartureAddress().getZipCode().compareToIgnoreCase(s1.getDepartureAddress().getZipCode());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.DECREASE);
                    mZipcodeFromDecrease = false;
                }
                mOrdersView.notifyDataChanged();
                break;
            case ZIPCODE_TO:
                if (!mZipcodeToDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s1.getDestinationAddress().getZipCode().compareToIgnoreCase(s2.getDestinationAddress().getZipCode());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.INCREASE);
                    mZipcodeToDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s2.getDestinationAddress().getZipCode().compareToIgnoreCase(s1.getDestinationAddress().getZipCode());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.DECREASE);
                    mZipcodeToDecrease = false;
                }
                mOrdersView.notifyDataChanged();
                break;
            case GOODS:
                if (!mGoodsToDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s1.getGood().compareToIgnoreCase(s2.getGood());
                        }
                    });
                    setSortingImage(label_order_goods,SortingDirections.INCREASE);
                    mGoodsToDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return s2.getGood().compareToIgnoreCase(s1.getGood());
                        }
                    });
                    setSortingImage(label_order_goods,SortingDirections.DECREASE);
                    mGoodsToDecrease = false;
                }
                mOrdersView.notifyDataChanged();
                break;
            case WEIGHT:
                if (!mWeightToDecrease) {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return Double.compare(s1.getActualWeight(), s2.getActualWeight());
                        }
                    });
                    setSortingImage(label_order_goods,SortingDirections.INCREASE);
                    mWeightToDecrease = true;
                }
                else {
                    Collections.sort(mOrdersView.getOrderList(), new Comparator<Order>() {
                        @Override
                        public int compare(Order s1, Order s2) {
                            return Double.compare(s2.getActualWeight(), s1.getActualWeight());
                        }
                    });
                    setSortingImage(label_order_goods,SortingDirections.DECREASE);
                    mWeightToDecrease = false;
                }
                mOrdersView.notifyDataChanged();
                break;
        }
    }

}
