package mobapply.freightexchange.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by  Ivan Dunskiy  on 4/2/15.
 *
 * Class for Filter creation possibility
 */
public class FilterItem implements Parcelable {

    private final String name;
    private final String value;
    private boolean hasToBeDeleted;

    public FilterItem(String name , String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public boolean isHasToBeDeleted() {
        return hasToBeDeleted;
    }

    public void setHasToBeDeleted(boolean hasToBeDeleted) {
        this.hasToBeDeleted = hasToBeDeleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FilterItem that = (FilterItem) o;

        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.value);
    }

    private FilterItem(Parcel in) {
        this.name = in.readString();
        this.value = in.readString();
    }

    public static final Parcelable.Creator<FilterItem> CREATOR = new Parcelable.Creator<FilterItem>() {
        public FilterItem createFromParcel(Parcel source) {
            return new FilterItem(source);
        }

        public FilterItem[] newArray(int size) {
            return new FilterItem[size];
        }
    };


}
