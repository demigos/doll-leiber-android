package mobapply.freightexchange.library.arrayadapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.model.DrawerItem;

/**
 * Created by Ivan Dunskiy on 4/16/15.
 *
 * Adapter for Navigation Drawer list displaying
 */
public class CustomDrawerAdapter  extends ArrayAdapter<DrawerItem> {

    private final Context context;
    private final List<DrawerItem> drawerItemList;
    private final int layoutResID;

    public CustomDrawerAdapter(Context context,
                               ArrayList<DrawerItem> listItems) {
        super(context, R.layout.custom_drawer_item, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = R.layout.custom_drawer_item;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.ItemName = (TextView) view
                    .findViewById(R.id.drawer_itemName);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            view.setTag(drawerHolder);

        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();

        }

        DrawerItem dItem = this.drawerItemList.get(position);
        drawerHolder.icon.setImageDrawable(view.getResources().getDrawable(
                dItem.getImgResID()));
        drawerHolder.ItemName.setText(dItem.getItemName());

        return view;
    }


    private static class DrawerItemHolder {
        TextView ItemName;
        ImageView icon;
    }
}