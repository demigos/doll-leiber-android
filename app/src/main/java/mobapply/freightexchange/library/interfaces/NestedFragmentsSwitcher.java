package mobapply.freightexchange.library.interfaces;

/**
 * Created by Ivan Dunskiy on 7/9/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public interface NestedFragmentsSwitcher {
    void onSwitchToNextFragment();
}
