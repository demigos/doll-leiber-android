package mobapply.freightexchange.customviews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;

/**
 * Created by Ivan Dunskiy on 5/16/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * Adapter for map markers PopUp displaying with specific order information
 */
public class PopupMapAdapter implements GoogleMap.InfoWindowAdapter {

    private final LayoutInflater inflater;

    public PopupMapAdapter(LayoutInflater inflater, Context context) {
        this.inflater=inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View popup = inflater.inflate(R.layout.map_tooltip, null);
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            TextView tooltip_order_number = (TextView) popup.findViewById(R.id.tooltip_order_number);
            TextView tooltip_departure_city = (TextView) popup.findViewById(R.id.tooltip_departure_city);
            TextView tooltip_dest_city = (TextView) popup.findViewById(R.id.tooltip_dest_city);
            JSONObject data = null;
            try {
                data = new JSONObject(marker.getSnippet());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (data != null) {
                try {
                    tooltip_order_number.setText(data.get(Constants.KEY_TOOLTIP_ORDER_NUMBER).toString());
                    tooltip_departure_city.setText(data.get(Constants.KEY_TOOLTIP_DEPARTURE_CITY).toString());
                    tooltip_dest_city.setText(data.get(Constants.KEY_TOOLTIP_DEST_CITY).toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
        return popup;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
