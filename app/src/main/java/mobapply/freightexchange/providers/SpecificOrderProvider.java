package mobapply.freightexchange.providers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import org.json.JSONException;
import org.json.JSONObject;

import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.Order;

/**
 * Created by Ivan Dunskiy on 6/4/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class SpecificOrderProvider {
    private static final String TAG = "OrdersListProvider";
    private Context mContext;
    private String orderUUID ;
    private Order mSpecificOrder;

    public SpecificOrderProvider(Context mContext, String orderUUID) {
        this.orderUUID = orderUUID;
        this.mContext = mContext;

    }

        /*
         * Class for loading and processing orders
         */
    public void loadOrder(final SpecificOrderCallback callback) {
        String url = Utils.getRequestUrl(mContext, WebRequestTypes.ALL_ORDERS) + orderUUID;
        Utils.sendNewJsonVolleyRequest(mContext, Request.Method.GET , url, null, new VolleyCallback() {
            @Override
            public void onSuccess(String  stringObject) {
                try {
                    ((Activity)(mContext)).runOnUiThread(new OrderParsingHelper(new JSONObject(stringObject), callback));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
   Sending message method to the server . Flag determines when the message was sent - by the order
   cancellation or just regular sending method.
   */
    public void sendProposal(final SpecificOrderCallback callback, String uuid, final String userUUID, final String costOfFreight) {
        String url =
                  AuthenticationRequester.URL_ALL_ORDERS
//                Utils.getRequestUrl(mContext, WebRequestTypes.ALL_ORDERS)
                + uuid+ "/proposals";

        Log.i(TAG + " userUUID", userUUID);
        JSONObject params = new JSONObject();
        try {
            params.put("userUuid", userUUID);
            params.put("costOfFreight",costOfFreight);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utils.sendNewJsonVolleyRequest(mContext, Request.Method.POST , url, params, new VolleyCallback() {
            @Override
            public void onSuccess(String  stringObject) {
                callback.onProposalSended(costOfFreight);
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });

    }



    public interface SpecificOrderCallback {
        void onOrderLoaded(Order mOrder);
        void onProposalSended(String amount);
    }
    /*
     * Class for data parsing and displaying
     */
    class OrderParsingHelper implements Runnable {
        // Defines the code to run for this task
        JSONObject receivedJsonArray;
        SpecificOrderCallback ordersCallback;

        OrderParsingHelper(JSONObject receivedJsonArray, SpecificOrderCallback ordersCallback) {
            this.receivedJsonArray = receivedJsonArray;
            this.ordersCallback = ordersCallback;
        }

        public void run() {
            mSpecificOrder = JSONParser.parseSpecificOrder(receivedJsonArray);
            ordersCallback.onOrderLoaded(mSpecificOrder);
        }
    }
}
