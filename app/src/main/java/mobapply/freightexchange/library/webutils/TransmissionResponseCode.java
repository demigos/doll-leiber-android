package mobapply.freightexchange.library.webutils;

/**
 * Created by Ivan Dunskiy on 07.03.2015.
 *
 * Interface for http requests handling and passing
 */
public interface TransmissionResponseCode {
    public void onMessageSuccess(String str);
    public void onMessageError(int errorCode, String message);
}
