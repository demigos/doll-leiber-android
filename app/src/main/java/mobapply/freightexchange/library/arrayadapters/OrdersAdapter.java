package mobapply.freightexchange.library.arrayadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.model.Order;

/**
 * Created by Ivan Dunskiy on 4/6/15.
 Copyright (c) 2015 Rarus. All rights reserved.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    private final List<Order> ordersList;
    private final Context mContext;

    public OrdersAdapter(Context context, List<Order> itemsData) {
        this.ordersList = itemsData;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item, parent,false);

        // create ViewHolder

        return new ViewHolder(itemLayoutView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        Order order = ordersList.get(position);
        viewHolder.orderId.setText(Utils.getFormattedDate(mContext, order.getAppointmentFrom())+  System.getProperty("line.separator") +
                Utils.getFormattedTime(mContext, order.getAppointmentFrom()));

        viewHolder.orderFromCity.setText(order.getDepartureAddress().getCity());
        viewHolder.orderFromCountry.setText(order.getDepartureAddress().getCountry());

        viewHolder.orderFromIndex.setText(order.getDepartureAddress().getZipCode());

        viewHolder.orderToCity.setText(order.getDestinationAddress().getCity());
        viewHolder.orderToCountry.setText(order.getDestinationAddress().getCountry());
        viewHolder.orderToIndex.setText(order.getDestinationAddress().getZipCode());

        viewHolder.orderGoods.setText(order.getGood());
        viewHolder.orderInfo.setText(order.getAdditionalInfo());

    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView orderFromCountry;
        private final TextView orderFromIndex;
        private final TextView orderToCity;
        private final TextView orderToCountry;
        private final TextView orderToIndex;
        private final TextView orderFromCity;
        public final TextView orderId;
        public final TextView orderGoods;
        public final TextView orderInfo;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            orderId = (TextView) itemLayoutView.findViewById(R.id.order_id);

            orderFromCity = (TextView) itemLayoutView.findViewById(R.id.order_from_city);
            orderFromCountry = (TextView) itemLayoutView.findViewById(R.id.order_from_country);
            orderFromIndex = (TextView) itemLayoutView.findViewById(R.id.order_from_index);

            orderToCity = (TextView) itemLayoutView.findViewById(R.id.order_to_city);
            orderToCountry = (TextView) itemLayoutView.findViewById(R.id.order_to_country);
            orderToIndex = (TextView) itemLayoutView.findViewById(R.id.order_to_index);

            orderGoods = (TextView) itemLayoutView.findViewById(R.id.order_goods);
            orderInfo = (TextView) itemLayoutView.findViewById(R.id.order_info);
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return ordersList.size();
    }

}
