package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.interfaces.DateSet;
import mobapply.freightexchange.library.interfaces.SavedFilterCommunicator;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.FilterItem;
import mobapply.freightexchange.ui.activities.FilterTabbedActivity;

public class FilterFragment extends Fragment implements View.OnClickListener, View.OnTouchListener, DateSet, SavedFilterCommunicator {

    private static final String TAG = "FilterFragment";

    @Override
    public void passSavedFilter(FilterItem filterItem) {
        this.parseSavedFilter(filterItem);
    }

    public  enum  FilterType {
        COUNTRIES,
        CITIES,
        ZIPCODES,
        DATE_FROM,
        DATE_TO
    }
    private EditText filter_from_countries;
    private EditText filter_from_zips;
    private EditText filter_from_cities;
    private EditText filter_to_countries;
    private EditText filter_to_zips;
    private EditText filter_to_cities;
    private EditText filter_load_at;
    private EditText filter_load_to;
    private CheckedTextView chb_filter_save;
    private Button btn_filter_results;

    private ArrayList<String> countriesList;
    private String load_to_date;

    private boolean isSaveChecked;
    private String mNewFilterName;


    public static FilterFragment newInstance() {
        FilterFragment fragment = new FilterFragment();
        return fragment;
    }

    public FilterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_filter, container, false);
        this.setHasOptionsMenu(false);
        filter_from_countries = (EditText)view.findViewById(R.id.filter_from_countries);
        filter_from_zips = (EditText)view.findViewById(R.id.filter_from_zips);
        filter_from_cities = (EditText)view.findViewById(R.id.filter_from_cities);

        filter_to_countries = (EditText)view.findViewById(R.id.filter_to_countries);
        filter_to_zips = (EditText)view.findViewById(R.id.filter_to_zips);
        filter_to_cities = (EditText)view.findViewById(R.id.filter_to_cities);

        filter_load_at  = (EditText)view.findViewById(R.id.filter_load_at);
        filter_load_to = (EditText)view.findViewById(R.id.filter_load_to);

        Button btn_reset_filter = (Button)view.findViewById(R.id.btn_reset_filter);
        btn_filter_results  = (Button)view.findViewById(R.id.btn_filter_results);

        chb_filter_save = (CheckedTextView)view.findViewById(R.id.chb_filter_save);

        btn_filter_results.setOnClickListener(this);
        btn_reset_filter.setOnClickListener(this);
        chb_filter_save.setOnClickListener(this);

        countriesList= getAllCountries();

        filter_from_countries.setOnTouchListener(this);
        filter_from_zips.setOnTouchListener(this);
        filter_from_cities.setOnTouchListener(this);

        filter_to_countries.setOnTouchListener(this);
        filter_to_zips.setOnTouchListener(this);
        filter_to_cities.setOnTouchListener(this);

        filter_load_at.setOnTouchListener(this);
        filter_load_to.setOnTouchListener(this);

        if (filter_load_at.getText().toString().isEmpty())
            filter_load_at.setText(getCurrentTime());
        return view;
    }

    void createDialogList(ArrayList<String> mList, final EditText view, FilterType filterType)
    {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                getActivity());
        if (filterType == FilterType.COUNTRIES  || filterType == FilterType.CITIES ||
                filterType == FilterType.ZIPCODES) {
            if (filterType == FilterType.ZIPCODES)
                builderSingle.setTitle(getString(R.string.title_filter_zipcodes));
            else if (filterType == FilterType.CITIES)
                builderSingle.setTitle(getString(R.string.title_filter_cities));
            else builderSingle.setTitle(getString(R.string.title_filter_countries));

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                    getActivity(),
                    android.R.layout.select_dialog_singlechoice, mList);
            builderSingle.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            view.setText(arrayAdapter.getItem(which));
                            filterOrders();
                        }
                    });
            builderSingle.show();
        }
        else if (filterType == FilterType.DATE_TO)
        {
            String[] dateToArray = getResources().getStringArray(R.array.date_to_dialog_array);

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                    getActivity(),
                    android.R.layout.select_dialog_singlechoice, dateToArray);
            builderSingle.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case 0:
                                    DatePickerFragment dialogFrag =
                                            DatePickerFragment.newInstance(FilterType.DATE_TO);
                                    dialogFrag.setTargetFragment(FilterFragment.this, Constants.REQUESTCODE_DATETIME);
                                    dialogFrag.show(getActivity().getSupportFragmentManager(),"DatePickerFragment");
                                    break;
                            }
                            if (which != 0 )
                            {
                                switch (which){
                                    case 1:
                                        load_to_date = calculateDateTo(3);
                                        filterOrders();
                                        break;
                                    case 2:
                                        load_to_date = calculateDateTo(7);
                                        filterOrders();
                                        break;
                                    case 3:
                                        load_to_date = calculateDateTo(14);
                                        filterOrders();
                                        break;
                                    case 4:
                                        load_to_date = calculateDateTo(30);
                                        filterOrders();
                                        break;
                                    case 5:
                                        load_to_date = calculateDateTo(91);
                                        filterOrders();
                                        break;
                                }
                                view.setText(arrayAdapter.getItem(which));
                            }
                        }
                    });
            builderSingle.show();
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((FilterTabbedActivity)activity).savedFilterCommunicator = this;
    }

    private void filterOrders() {
        Log.i(TAG + " request", Utils.getUrlWithParameters(AuthenticationRequester.URL_ORDER_FILTER, generateFilterRequest()));

        Utils.sendNewJsonVolleyRequest(getActivity(), Request.Method.HEAD, Utils.getUrlWithParameters(Utils.getRequestUrl(getActivity(), WebRequestTypes.FILTER), generateFilterRequest()) , null, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                btn_filter_results.setText(result + " result");

            }
            @Override
            public void onErrorResponse(String error) {

            }
        });

    }

    private JSONObject generateFilterRequest() {
        JSONObject jsonFilterRequest = new JSONObject();
        try {
            if (filter_from_countries != null && !filter_from_countries.getText().toString().isEmpty())
                jsonFilterRequest.put(Constants.FILTER_DEPARTURE_COUNTRY, filter_from_countries.getText().toString());

            if (filter_from_cities != null && !filter_from_cities.getText().toString().isEmpty())
                jsonFilterRequest.put(Constants.FILTER_DEPARTURE_CITY, filter_from_cities.getText().toString());

            if (filter_from_zips!= null && !filter_from_zips.getText().toString().isEmpty())
                jsonFilterRequest.put(Constants.FILTER_DEPARTURE_ZIPCODE, filter_from_zips.getText().toString());

            if (filter_to_countries != null && !filter_to_countries.getText().toString().isEmpty())
                jsonFilterRequest.put(Constants.FILTER_DESTINATION_COUNTRY, filter_to_countries.getText().toString());

            if (filter_to_cities != null && !filter_to_cities.getText().toString().isEmpty())
                jsonFilterRequest.put(Constants.FILTER_DESTINATION_CITY, filter_to_cities.getText().toString());

            if (filter_to_zips!= null && !filter_to_zips.getText().toString().isEmpty())
                jsonFilterRequest.put(Constants.FILTER_DESTINATION_ZIPCODE, filter_to_zips.getText().toString());

            if (filter_load_at!= null && !filter_load_at.getText().toString().isEmpty())
                jsonFilterRequest.put(Constants.FILTER_DATE_FROM, filter_load_at.getText().toString());

            if (filter_load_to!= null && !filter_load_to.getText().toString().isEmpty())
                jsonFilterRequest.put(Constants.FILTER_DATE_TO, load_to_date);

            jsonFilterRequest.put(Constants.FILTER_ORDER_STATUS, "NEW");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonFilterRequest;
    }

    private ArrayList<String> getAllCountries(){
        final ArrayList<String> mList = new ArrayList<>();
        Utils.sendNewJsonVolleyRequest(getActivity(), Request.Method.GET,
                Utils.getRequestUrl(getActivity(), WebRequestTypes.ALL_COUNTRIES), null, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            mList.add(jsonArray.getString(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onErrorResponse(String error) {

            }
        });
        return mList;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_reset_filter:
                if (filter_from_countries!=null)
                    filter_from_countries.getText().clear();
                if (filter_from_cities!=null)
                    filter_from_cities.getText().clear();
                if (filter_from_zips!=null)
                    filter_from_zips.getText().clear();
                if (filter_to_countries!=null)
                    filter_to_countries.getText().clear();
                if (filter_to_cities!=null)
                    filter_to_cities.getText().clear();
                if (filter_to_zips!=null)
                    filter_to_zips.getText().clear();
                if (filter_load_at!=null)
                    filter_load_at.getText().clear();
                if (filter_load_to!=null)
                    filter_load_to.getText().clear();
                btn_filter_results.setText(getString(R.string.text_btn_filter_results));
                break;
            case R.id.btn_filter_results:
                Intent a = new Intent();
                a.putExtra(Constants.BUNDLE_FILTER_STRING, this.generateFilterRequest().toString());
                getActivity().setResult(Activity.RESULT_OK, a);
                if (isSaveChecked)
                    saveNewFilter();
                getActivity().finish();
                break;

            case R.id.chb_filter_save:
                isSaveChecked = !isSaveChecked;
                if (isSaveChecked)
                {
                    showDialogFilterSave();
                    chb_filter_save.setChecked(true);
                }
                else
                    chb_filter_save.setChecked(false);

                break;

        }
    }

    private void saveNewFilter() {
        if (mNewFilterName!=null && !mNewFilterName.isEmpty())
        {

            Utils.addFilterItem(getActivity(), new FilterItem(mNewFilterName, this.generateFilterRequest().toString()));

        }

    }

    private void showDialogFilterSave() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

        alert.setTitle(getString(R.string.title_filter_save));
        alert.setMessage(getString(R.string.text_filter_save_message));
        // Set an EditText view to get user input
        final EditText input = new EditText(getActivity());
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                mNewFilterName = input.getText().toString();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getActionMasked();
        switch (action) {

            case MotionEvent.ACTION_UP:
                switch (v.getId()) {
                    case R.id.filter_from_countries:
                        createDialogList(countriesList,(EditText)v, FilterType.COUNTRIES);
                        break;
                    case R.id.filter_from_zips:
                        if (filter_from_countries !=null && !filter_from_countries.getText().toString().isEmpty())
                        {
                            fetchZipsCities(filter_from_countries.getText().toString(),
                                    (EditText)v, FilterType.ZIPCODES);
                        }
                        break;
                    case R.id.filter_from_cities:
                        if (filter_from_countries !=null && !filter_from_countries.getText().toString().isEmpty())
                        {
                            fetchZipsCities(filter_from_countries.getText().toString(),
                                    (EditText) v, FilterType.CITIES);
                        }
                        break;
                    case R.id.filter_to_countries:
                        createDialogList(countriesList,(EditText)v, FilterType.COUNTRIES);
                        break;
                    case R.id.filter_to_zips:
                        if (filter_to_countries !=null && !filter_to_countries.getText().toString().isEmpty())
                        {
                            fetchZipsCities(filter_to_countries.getText().toString(),
                                    (EditText)v, FilterType.ZIPCODES);
                        }
                        break;
                    case R.id.filter_to_cities:
                        if (filter_to_countries !=null && !filter_to_countries.getText().toString().isEmpty())
                        {
                            fetchZipsCities(filter_to_countries.getText().toString(),
                                    (EditText)v, FilterType.CITIES);
                        }
                        break;
                    case R.id.filter_load_at:
                        DatePickerFragment dialogFrag =
                                DatePickerFragment.newInstance(FilterType.DATE_FROM);
                        dialogFrag.setTargetFragment(FilterFragment.this, Constants.REQUESTCODE_DATETIME);
                        dialogFrag.show(getActivity().getSupportFragmentManager(),"DatePickerFragment");
                        break;
                    case R.id.filter_load_to:
                        createDialogList(null, (EditText)v, FilterType.DATE_TO );
                        break;
                }
                break;

        }
        return true;
    }
    private ArrayList<String> fetchZipsCities(String country, final EditText v, final FilterType type)
    {

        final ArrayList<String> mList = new ArrayList<>();
        String url = null;
        switch (type)
        {
            case CITIES:
                url = Utils.getRequestUrl(getActivity(), WebRequestTypes.ALL_CITIES) + "?country="+ country;
                break;
            case ZIPCODES:
                url = Utils.getRequestUrl(getActivity(), WebRequestTypes.ALL_ZIPCODES) + "?country="+ country;
                break;

        }
        Utils.sendNewJsonVolleyRequest(getActivity(), Request.Method.GET, url , null, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i=0; i<jsonArray.length(); i++) {
                            mList.add( jsonArray.getString(i) );
                        }
                        switch (type){
                            case CITIES:
                                createDialogList(mList,  v, FilterType.CITIES);
                                break;
                            case ZIPCODES:
                                createDialogList(mList,  v, FilterType.ZIPCODES);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onErrorResponse(String error) {

            }
        });
        return mList;
    }

    @Override
    public void onDateSet(FilterType type, String date) {
        switch (type)
        {
            case COUNTRIES:
                break;
            case CITIES:
                break;
            case ZIPCODES:
                break;
            case DATE_FROM :
                Log.i(TAG, date);
//                filter_load_at.setText(date);
//                filterOrders();
                break;

            case DATE_TO :
                filter_load_to.setText(date);
                load_to_date = date;
                filterOrders();
                break;
        }

    }

    public void parseSavedFilter(FilterItem filter)
    {
        JSONObject parsedObject;
        try {
            parsedObject = new JSONObject(filter.getValue());
            Iterator iter = parsedObject.keys();
            while(iter.hasNext()){
                String key = (String)iter.next();
                String value = parsedObject.getString(key);
                switch (key){
                    case Constants.FILTER_DEPARTURE_COUNTRY:
                        filter_from_countries.setText(value);
                        break;
                    case Constants.FILTER_DEPARTURE_ZIPCODE:
                        filter_from_zips.setText(value);
                        break;
                    case Constants.FILTER_DEPARTURE_CITY:
                        filter_from_cities.setText(value);
                        break;
                    case Constants.FILTER_DESTINATION_COUNTRY:
                        filter_to_countries.setText(value);
                        break;
                    case Constants.FILTER_DESTINATION_ZIPCODE:
                        filter_to_zips.setText(value);
                        break;
                    case Constants.FILTER_DESTINATION_CITY:
                        filter_to_cities.setText(value);
                    case Constants.FILTER_DATE_FROM:
                        filter_load_at.setText(value);
                    case Constants.FILTER_DATE_TO:
                        filter_load_to.setText(value);
                        break;
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        filterOrders();
    }

    private String getCurrentTime(){
        SimpleDateFormat sdf = new SimpleDateFormat(getString(R.string.format_date));
        return sdf.format(new Date());
    }
    private String calculateDateTo(int days){
        String dt = filter_load_at.getText().toString();  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat(getString(R.string.format_date));
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_MONTH, days);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        // dd-MM-yyyy
        SimpleDateFormat sdf1 = new SimpleDateFormat(getString(R.string.format_date));
        return sdf1.format(c.getTime());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case Constants.REQUESTCODE_DATETIME:

                if (resultCode == Activity.RESULT_OK) {
                    if (data.getStringExtra(Constants.BUNDLE_DATE_FROM) != null && !data.getStringExtra(Constants.BUNDLE_DATE_FROM).isEmpty())
                    {
                        filter_load_at.setText(data.getStringExtra(Constants.BUNDLE_DATE_FROM));
                        filterOrders();
                    }
                    else if (data.getStringExtra(Constants.BUNDLE_DATE_TO) != null && !data.getStringExtra(Constants.BUNDLE_DATE_TO).isEmpty() )
                    {
                        filter_load_to.setText(data.getStringExtra(Constants.BUNDLE_DATE_TO));
                        filterOrders();
                    }
                }

                break;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }



}
