package mobapply.freightexchange.library.interfaces;

import mobapply.freightexchange.model.SortingStates;

/**
 * Created by Ivan Dunskiy on 7/14/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public interface SortingItems {
    void sortItems(SortingStates sortingStates);
}
