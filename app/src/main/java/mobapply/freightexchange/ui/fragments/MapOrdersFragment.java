package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import mobapply.freightexchange.R;
import mobapply.freightexchange.customviews.PopupMapAdapter;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.OrderManager;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.ui.activities.SpecificOrderActivity;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;


public class MapOrdersFragment extends Fragment implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapLongClickListener{
    private static final String TAG = "MapOrdersFragment";

    private MapView mMapView;
    private GoogleApiClient mLocationClient;
    private GoogleMap map;
    private int status;
    private final static int  CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private final ArrayList<Polygon> polygonsArrayList = new ArrayList<>();
    private TextView textview_map_sliding;

    public MapOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_map_orders, container, false);
        status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        mMapView = (MapView) view.findViewById(R.id.mapview);
        textview_map_sliding = (TextView) view.findViewById(R.id.textview_map_sliding);

        textview_map_sliding.postDelayed(new Runnable() {
            public void run() {
                textview_map_sliding.setVisibility(View.GONE);
            }
        }, 5000);

        mMapView.onCreate(savedInstanceState);
        mLocationClient =   new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        map = mMapView.getMap();
        MapsInitializer.initialize(this.getActivity());
        if (map != null) {
              map.getUiSettings().setMyLocationButtonEnabled(false);
              map.getUiSettings().setZoomControlsEnabled(false);
              map.setMyLocationEnabled(true);
              LatLng latLng = new LatLng(52.519660, 13.410030);
              CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 4);
              map.animateCamera(cameraUpdate);
              map.setOnMapLongClickListener(this);
         }
//        addOrdersToMap(OrderManager.getInstance().getOrders());
        clearMap();
        return view;

    }


    @Override
    public void onStart() {
        super.onStart();
        mLocationClient.connect();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        map.clear();
    }

    @Override
    public void onResume() {
        Utils.checkGoogleServices(getActivity(), status);
        displayOrders();
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_map, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_map_all_action) {
//            Toast.makeText(getActivity(), " im here", Toast.LENGTH_SHORT).show();
            getAllOrders();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayOrders() {
        mMapView.onResume();
        for (Object o : OrderManager.getInstance().getOrders().entrySet()) {
            Map.Entry thisEntry = (Map.Entry) o;
            final Order order = (Order) thisEntry.getValue();
            LatLng departure = order.getDeparturePoint();
            LatLng destination = order.getDestinationPoint();
            if (departure != null && destination != null) {
                if (map != null) {
                    try {
                        map.addPolyline(new PolylineOptions()
                                .add(departure, destination)
                                .width(2)
                                .color(Color.parseColor("#848383")));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView!=null)
            mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        getActivity(),
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
	                /*
	                 * Thrown if Google Play services canceled the original
	                 * PendingIntent
	                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
	            /*
	             * If no resolution is available, display a dialog to the
	             * user with the error.
	             */
            showErrorDialog(connectionResult.getErrorCode());
        }

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    private void showErrorDialog(int errorCode) {
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                errorCode,
                getActivity(),
                CONNECTION_FAILURE_RESOLUTION_REQUEST);
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            mobapply.freightexchange.ui.fragments.ErrorDialogFragment errorFragment = new mobapply.freightexchange.ui.fragments.ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            errorFragment.show(getActivity().getSupportFragmentManager(),
                    "Location Updates");
        }

    }

    void clearMap()
    {
        if (map!=null)
            map.clear();
    }

    void addOrdersToMap(Map<String, Order> orderMapmap)
    {
        if ( map !=null) {
            if (orderMapmap != null && !orderMapmap.isEmpty()) {
                Iterator entries = orderMapmap.entrySet().iterator();
                while (entries.hasNext()) {
                    map.clear();
                    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage(getString(R.string.dialog_progress_message));
                    progressDialog.show();
                    Map.Entry thisEntry = (Map.Entry) entries.next();
                    final Order order = (Order) thisEntry.getValue();
                    String finalDepartureUrl = Constants.URL_GEOCODING + getAddressUrlString(Constants.FLAG_ADDRESS_DEPARTURE, order);
                    String finalDestinationUrl = Constants.URL_GEOCODING + getAddressUrlString(Constants.FLAG_ADDRESS_DESTINATION, order);
                    getVolleyJSONObjectResponse(finalDepartureUrl, new VolleyCallback() {
                        @Override
                        public void onSuccess(String jsonObject) {
                            try {
                                handleVolleyResponse(new JSONObject(jsonObject), order, Constants.FLAG_ADDRESS_DEPARTURE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onErrorResponse(String error) {


                        }
                    });
                    getVolleyJSONObjectResponse(finalDestinationUrl, new VolleyCallback() {
                        @Override
                        public void onSuccess(String jsonObject) {
                            try {
                                handleVolleyResponse(new JSONObject(jsonObject), order, Constants.FLAG_ADDRESS_DESTINATION);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            progressDialog.dismiss();
                        }


                        @Override
                        public void onErrorResponse(String error) {
                            progressDialog.dismiss();
                            Log.i(TAG + " error", error);
//                            Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        }


    }

    private void handleVolleyResponse(JSONObject jsonObject, Order order, int flag) {
        JSONObject jsonGeometry;
        LatLng latLng;

        try {
            JSONArray objectsArray = jsonObject.getJSONArray("results");
            if (!objectsArray.isNull(0))
            {
                JSONObject point = objectsArray.getJSONObject(0);
                jsonGeometry = point.getJSONObject("geometry");
                JSONObject jsonLocation = jsonGeometry.getJSONObject("location");
                latLng = new LatLng(Double.parseDouble(jsonLocation.getString("lat")),
                        Double.parseDouble(jsonLocation.getString("lng")));
                OrderManager.getInstance().applyGeoData(flag, order.getUuid(), latLng);
                OrderManager.getInstance().getLatLngMap().put(order.getUuid(), latLng);

                switch (flag) {
                    case Constants.FLAG_ADDRESS_DEPARTURE:
                        order.setDeparturePoint(latLng);
                        setMarker(order, Constants.FLAG_ADDRESS_DEPARTURE, latLng);
                        break;
                    case Constants.FLAG_ADDRESS_DESTINATION:
                        order.setDestinationPoint(latLng);
                        setMarker(order, Constants.FLAG_ADDRESS_DESTINATION, latLng);
                        break;
                }

                displayOrders();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    void setMarker(Order order, int flag, LatLng position) {
        JSONObject mapInfo = new JSONObject();
        MarkerOptions options = new MarkerOptions();
        options.position(position);
        try {

            mapInfo.put(Constants.KEY_TOOLTIP_ORDER_NUMBER, order.getNumber());
            mapInfo.put(Constants.KEY_TOOLTIP_DESTINATION, flag);
            switch (flag)
            {
                case Constants.FLAG_ADDRESS_DEPARTURE:
                    options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green));
                    break;
                case Constants.FLAG_ADDRESS_DESTINATION:
                    options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_mypoint));
                    break;
            }
            mapInfo.put(Constants.KEY_TOOLTIP_DEST_CITY, order.getDestinationAddress().getCity());
            mapInfo.put(Constants.KEY_TOOLTIP_DEPARTURE_CITY, order.getDepartureAddress().getCity());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        options.title(order.getUuid());
        options.snippet(mapInfo.toString());
        try {
            if (position != null) {
                map.addMarker(options);
                map.setInfoWindowAdapter(new PopupMapAdapter(getActivity().getLayoutInflater(), getActivity()));
                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent a = new Intent(getActivity(), SpecificOrderActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.KEY_CHOOSED_ORDER,
                                marker.getTitle());
                        a.putExtras(bundle);
                        startActivityForResult(a, Constants.REQUESTCODE_MAP);
                        marker.hideInfoWindow();
                    }
                });
            }
        }
        catch (NullPointerException e){e.printStackTrace();}
    }


    private void getVolleyJSONObjectResponse( final String url, final VolleyCallback iVolleyCallback)
    {
        Utils.sendNewJsonVolleyRequest(getActivity(), Request.Method.GET, url , null, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                iVolleyCallback.onSuccess(result);
            }

            @Override
            public void onErrorResponse(String error) {
                iVolleyCallback.onErrorResponse(error);
            }
        });

    }
    private void getVolleyJSONArrayResponse( String url, final VolleyCallback iVolleyCallback)
    {

        Utils.sendNewJsonVolleyRequest(getActivity(), Request.Method.GET , url, null, new VolleyCallback() {
            @Override
            public void onSuccess(String  stringObject) {
                try {
                    iVolleyCallback.onSuccess(new JSONArray(stringObject).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private String getAddressUrlString(int flag, Order order) {

        StringBuilder addressUrl = new StringBuilder();
        addressUrl.append("address=");
        switch (flag) {
            case Constants.FLAG_ADDRESS_DEPARTURE:
            try {
                boolean isAdded=false;
                if (order.getDepartureAddress().getStreet() != null && !order.getDepartureAddress().getStreet().equalsIgnoreCase(""))
                    addressUrl.append(URLEncoder.encode(order.getDepartureAddress().getStreet(), "UTF-8")).append(", ");
                if (order.getDepartureAddress().getHouseNumber() != null && !order.getDepartureAddress().getHouseNumber().equalsIgnoreCase(""))
                    addressUrl.append(URLEncoder.encode(order.getDepartureAddress().getHouseNumber(), "UTF-8")).append(", ");
                if (order.getDepartureAddress().getZipCode() != null && !order.getDepartureAddress().getZipCode().equalsIgnoreCase(""))
                {
                    addressUrl.append(URLEncoder.encode(order.getDepartureAddress().getZipCode(), "UTF-8")).append(",");
                    isAdded = true;
                }
                if (order.getDepartureAddress().getCity() != null && !order.getDepartureAddress().getCity().equalsIgnoreCase(""))
                    addressUrl.append(URLEncoder.encode(order.getDepartureAddress().getCity(), "UTF-8"));
                if (order.getDepartureAddress().getCountryCode() != null && !order.getDepartureAddress().getCountryCode().equalsIgnoreCase(""))
                    addressUrl.append("&region=").append(order.getDepartureAddress().getCountryCode());

                if (order.getDepartureAddress().getZipCode() != null && !order.getDepartureAddress().getZipCode().equalsIgnoreCase("") && !isAdded)
                    addressUrl.append("&components=postal_code:" + URLEncoder.encode(order.getDepartureAddress().getZipCode(),"UTF-8"));
                addressUrl.append("&sensor=false");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            break;
            case Constants.FLAG_ADDRESS_DESTINATION:
            try {
                boolean isAdded=false;
                if (order.getDestinationAddress().getStreet() != null && !order.getDestinationAddress().getStreet().equalsIgnoreCase(""))
                    addressUrl.append(URLEncoder.encode(order.getDestinationAddress().getStreet(), "UTF-8")).append(", ");
                if (order.getDestinationAddress().getHouseNumber() != null && !order.getDestinationAddress().getHouseNumber().equalsIgnoreCase(""))
                    addressUrl.append(URLEncoder.encode(order.getDestinationAddress().getHouseNumber(), "UTF-8")).append(", ");
                if (order.getDestinationAddress().getZipCode() != null && !order.getDestinationAddress().getZipCode().equalsIgnoreCase(""))
                {
                    addressUrl.append(URLEncoder.encode(order.getDestinationAddress().getZipCode(), "UTF-8")).append(",");
                    isAdded = true;
                }
                if (order.getDestinationAddress().getCity() != null && !order.getDestinationAddress().getCity().equalsIgnoreCase(""))
                    addressUrl.append(URLEncoder.encode(order.getDestinationAddress().getCity(), "UTF-8"));
                if (order.getDestinationAddress().getCountryCode() != null && !order.getDestinationAddress().getCountryCode().equalsIgnoreCase(""))
                    addressUrl.append("&region=").append(order.getDestinationAddress().getCountryCode());

                if (order.getDestinationAddress().getZipCode() != null && !order.getDestinationAddress().getZipCode().equalsIgnoreCase("") && !isAdded)
                    addressUrl.append("&components=postal_code:" + URLEncoder.encode(order.getDestinationAddress().getZipCode(), "UTF-8"));
                addressUrl.append("&sensor=false");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
        return addressUrl.toString();
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        String finalUrl = Constants.URL_GEOCODING + "latlng=" + Double.toString(latLng.latitude) + ","+Double.toString(latLng.longitude)
                + "&sensor=false";
        this.getVolleyJSONObjectResponse(finalUrl, new VolleyCallback() {
            @Override
            public void onSuccess(String stringObject) {

                try {
                    JSONObject jsonObject = new JSONObject(stringObject);
                    if (jsonObject.getString("status").equalsIgnoreCase("OK")) {
                        JSONArray resultsArray = jsonObject.getJSONArray("results");
                        JSONObject jsonAddress = resultsArray.getJSONObject(0);
                        JSONArray address_components = jsonAddress.getJSONArray("address_components");
                        for (int i = 0; i < address_components.length(); i++) {
                            JSONObject objects = address_components.getJSONObject(i);
                            JSONArray types = objects.getJSONArray("types");
                            if (types.get(0).toString().equalsIgnoreCase("country")) {
                                String iso = objects.getString("short_name");
                                listAssetFiles(iso2CountryCodeToIso3CountryCode(iso));
                            }

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (StringIndexOutOfBoundsException e) {
                    Log.i("TAG ", e.getLocalizedMessage());
                }
            }

            @Override
            public void onErrorResponse(String error) {

            }
        });
    }

    private void listAssetFiles(String isoName) {
        String[] fileNames;
        try {
            fileNames = getActivity().getAssets().list("countries");

            for(String name:fileNames){
                if (name.contains(isoName)) {
                    getFilteredOrders( isoName , name);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void getFilteredOrders(final String iso, final String name){
        String departureUrl  = AuthenticationRequester.URL_NEW_ORDERS + "&" + Constants.FILTER_DEPARTURE_COUNTRY_ISO  + "=" + iso;
        final ArrayList<Order> departureList = new ArrayList<>();
        getVolleyJSONArrayResponse(departureUrl, new VolleyCallback() {
            @Override
            public void onSuccess(String stringObject) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(stringObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jsonArray != null && !jsonArray.isNull(0)) {
                    departureList.addAll(JSONParser.parseOrders(jsonArray));
                    map.clear();
                    Message msg = Message.obtain();
                    final Bundle bundle = new Bundle();
                    bundle.putString(Constants.KEY_RECEIVED_ORDER, jsonArray.toString());
                    msg.setData(bundle);
                    orderJSONObserver.sendMessage(msg);
                    addOrdersToMap(Utils.getOrdersHashMap(departureList));
                    drawChoosedCountry(name);
                } else
                    Toast.makeText(getActivity(), getString(R.string.toast_no_orders_in_country) + " " + getCountryNameFromIso3(iso), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onErrorResponse(String error) {

            }
        });

    }

    private String iso2CountryCodeToIso3CountryCode(String iso2CountryCode){
        Locale locale = new Locale("", iso2CountryCode);
        return locale.getISO3Country();
    }

    private String getCountryNameFromIso3(String iso3CountryCode){
        Locale locale = new Locale("", iso3CountryCode);
        return locale.getDisplayCountry();
    }

    private void drawChoosedCountry(String jsonCountryName) {
        try {
            if (polygonsArrayList != null && !polygonsArrayList.isEmpty()) {
                for (Polygon poly : polygonsArrayList) {
                    poly.remove();
                }
            }
            InputStream is = getActivity().getAssets().open("countries/" + jsonCountryName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String parsedCountry = new String(buffer, "UTF-8");
            try {

                JSONObject countryObject = new JSONObject(parsedCountry);

                JSONArray featuresArray = countryObject.getJSONArray("features");
                JSONObject feature = featuresArray.getJSONObject(0);
                JSONObject geometryJSON = feature.getJSONObject("geometry");
                JSONArray coordinates = geometryJSON.getJSONArray("coordinates");
                PolygonOptions polylineOptions = new PolygonOptions().geodesic(true).fillColor(getResources().getColor(R.color.countries_highlight)).strokeWidth(2);
                if (coordinates.length() <= 1) {
                    for (int i = 0; i < coordinates.getJSONArray(0).length(); i++) {
                        JSONArray coordinatesFirst = coordinates.getJSONArray(0).getJSONArray(i);
                        double latitude = coordinatesFirst.getDouble(0);
                        double longtitude = coordinatesFirst.getDouble(1);
                        polylineOptions.add(new LatLng(longtitude, latitude));
                    }
                    com.google.android.gms.maps.model.Polygon poly = map.addPolygon(polylineOptions);
                    polygonsArrayList.add(poly);
                } else {
                    for (int i = 0; i < coordinates.length(); i++) {
                        PolygonOptions polylineOptions2 = new PolygonOptions().geodesic(true).fillColor(getResources().getColor(R.color.countries_highlight)).strokeWidth(2);
                        JSONArray coordinatesFirst = coordinates.getJSONArray(i).getJSONArray(0);
                        for (int a = 0; a < coordinatesFirst.length(); a++) {
                            if (coordinatesFirst.optJSONArray(a) != null) {
                                JSONArray coordintesArr = coordinatesFirst.getJSONArray(a);
                                double latitude = coordintesArr.getDouble(0);
                                double longtitude = coordintesArr.getDouble(1);
                                polylineOptions2.add(new LatLng(longtitude, latitude));
                            }
                        }
                        if (polylineOptions2 != null) {
                            com.google.android.gms.maps.model.Polygon poly = map.addPolygon(polylineOptions2);
                            polygonsArrayList.add(poly);
                        }
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAllOrders(){
        getVolleyJSONArrayResponse(AuthenticationRequester.URL_NEW_ORDERS, new VolleyCallback() {
            @Override
            public void onSuccess(String stringObject) {
                JSONArray jsonArray  = null;
                try {
                    jsonArray = new JSONArray(stringObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jsonArray !=null && !jsonArray.isNull(0)) {
                    ArrayList<Order> orderArrayList = (ArrayList<Order>) JSONParser.parseOrders(jsonArray);
                    addOrdersToMap(Utils.getOrdersHashMap(orderArrayList));
                }
            }

            @Override
            public void onErrorResponse(String error) {

            }
        });

    }
    private static final Handler orderJSONObserver = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

        }

    };
}
