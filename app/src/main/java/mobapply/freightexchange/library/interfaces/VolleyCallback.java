package mobapply.freightexchange.library.interfaces;
/**
 * Created by Ivan Dunskiy on 5/14/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * Interface for maintaining Volley library responses
 */
public interface VolleyCallback {
    void onSuccess(String result);
    void onErrorResponse(String error);
}
