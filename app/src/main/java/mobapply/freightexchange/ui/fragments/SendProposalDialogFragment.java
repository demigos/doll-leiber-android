package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.ui.activities.MainActivity;

/**
 * Created by Ivan Dunskiy on 4/28/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * DialogFragment for logging out operation
 */
public class SendProposalDialogFragment extends DialogFragment {

    private SharedPreferences preferences;
    private Button btn_send_proposal_with_price;
    private EditText edit_proposal_price;
    private Button  btn_send_proposal_show_interest;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View layout = inflater.inflate(R.layout.fragment_send_proposal, null);
        preferences= getActivity().getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(layout)
                .setIcon(R.drawable.ic_launcher)
                .setTitle(getString(R.string.text_send_proposal_dialog))
                .create();


        btn_send_proposal_with_price = (Button)layout.findViewById(R.id.btn_send_proposal_with_price);
        edit_proposal_price = (EditText)layout.findViewById(R.id.edit_proposal_price);
        btn_send_proposal_show_interest = (Button)layout.findViewById(R.id.btn_send_proposal_show_interest);

        btn_send_proposal_with_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edit_proposal_price.getText().toString().equalsIgnoreCase("")){
                    SendProposalDialogFragment.this.dismiss();
                    Intent a = new Intent();
                    a.putExtra(Constants.BUNDLE_PROPOSAL_PRICE,edit_proposal_price.getText().toString() );
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Constants.RESULTCODE_PROPOSAL_PRICE, a);

                }
                else
                    Toast.makeText(getActivity(), getActivity().getString(R.string.toast_empty_proposal), Toast.LENGTH_SHORT).show();
            }
        });

        btn_send_proposal_show_interest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    SendProposalDialogFragment.this.dismiss();
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Constants.RESULTCODE_PROPOSAL_INTEREST, getActivity().getIntent());

            }
        });

        return alertDialog;

    }


}
