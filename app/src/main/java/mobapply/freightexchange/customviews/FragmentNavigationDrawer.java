package mobapply.freightexchange.customviews;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Iterator;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.arrayadapters.CustomDrawerAdapter;
import mobapply.freightexchange.model.DrawerItem;
import mobapply.freightexchange.ui.activities.FilterTabbedActivity;
import mobapply.freightexchange.ui.activities.MainActivity;
import mobapply.freightexchange.ui.fragments.LogOutFragment;
import mobapply.freightexchange.ui.fragments.MyProposalsFragment;
import mobapply.freightexchange.ui.fragments.OrdersFragment;
import mobapply.freightexchange.ui.fragments.OverviewByCountryFragment;
import mobapply.freightexchange.ui.fragments.SavedFiltersFragment;

/**
 * Created by  Ivan Dunskiy on 4/11/15.
 *
 * Custom DrawerLayout with Fragments logic interaction implementation
 */
public class FragmentNavigationDrawer  extends DrawerLayout {
    private ListView lvDrawer;
    private Toolbar toolbar;
    private ArrayList<FragmentNavItem> drawerNavItems;
    private int drawerContainerRes;
    private final ArrayList<DrawerItem> dataList = new ArrayList<>();
    private CustomDrawerAdapter mDrawerAdapter;
    private Context mContext;
    private Activity activity;
    public FragmentNavigationDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext= context;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public FragmentNavigationDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FragmentNavigationDrawer(Context context) {
        super(context);
    }

    public void setupDrawerConfiguration(ListView drawerListView, Toolbar drawerToolbar) {
        // Setup navigation items array
        drawerNavItems = new ArrayList<>();
        // Set the adapter for the list view

        mDrawerAdapter = new CustomDrawerAdapter(mContext,
                dataList);
        this.drawerContainerRes = R.id.flContent;
        // Setup drawer list view and related adapter
        lvDrawer = drawerListView;
        // Setup toolbar
        toolbar = drawerToolbar;
        lvDrawer.setAdapter(mDrawerAdapter);
        // Setup item listener
        lvDrawer.setOnItemClickListener(new FragmentDrawerItemListener());
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar icon
        ActionBarDrawerToggle drawerToggle = setupDrawerToggle();
        setDrawerListener(drawerToggle);
        // Setup action buttons
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    public void addNavItem(DrawerItem mDrawerItem, String windowTitle, Class<? extends Fragment> fragmentClass) {
            mDrawerAdapter.add(mDrawerItem);
            drawerNavItems.add(new FragmentNavItem(windowTitle, fragmentClass));
    }

    public void addNavItemToPosition(DrawerItem mDrawerItem, int position, String windowTitle, Class<? extends Fragment> fragmentClass) {
        dataList.add(position,mDrawerItem);
        mDrawerAdapter.notifyDataSetChanged();
        drawerNavItems.add(position, new FragmentNavItem(windowTitle, fragmentClass));
    }

    public void removeOverviewByCountry() {

        Iterator<FragmentNavItem> fragmentNavItemIterator = drawerNavItems.iterator();
        while (fragmentNavItemIterator.hasNext()) {
            FragmentNavItem fragmentNavItem = fragmentNavItemIterator.next();
            if ( fragmentNavItem.getFragmentClass().getName().equalsIgnoreCase(OverviewByCountryFragment.class.getName()))
            {
                fragmentNavItemIterator.remove();
            }
        }
        dataList.remove(4);
        mDrawerAdapter.notifyDataSetChanged();
        syncDrawerTitleState();

    }

    /**
     * Swaps fragments in the main content view
     */
    public void selectDrawerItem(int position) {

            // Create a new fragment and specify the planet to show based on
            // position
            FragmentNavItem navItem = drawerNavItems.get(position);
            Fragment fragment = null;
            try {
                fragment = navItem.getFragmentClass().newInstance();
                Bundle args = navItem.getFragmentArgs();
                if (args != null) {
                    fragment.setArguments(args);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

            if ( position==drawerNavItems.size()-1){
                ((LogOutFragment)fragment).show(fragmentManager, "LogOutFrag");
            }
            else
                fragmentManager.beginTransaction().replace(drawerContainerRes, fragment).commit();

            // Update the title, and close the drawer
            //        lvDrawer.setItemChecked(position, true);
            setTitle(navItem.getTitle());
            closeDrawer(lvDrawer);
    }



    /**
     * Change the ActionBar title if needed
     */
    public void syncDrawerTitleState(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.Fragment mFragment = fragmentManager.findFragmentById(drawerContainerRes);
        if (mFragment instanceof OrdersFragment)
            setTitle(getActivity().getString(R.string.label_all_orders));
        else if (mFragment instanceof MyProposalsFragment)
            setTitle(getActivity().getString(R.string.label_my_proposals));

    }


    public void setContext(Context context)
    {
        this.mContext = context;
    }

    private FragmentActivity getActivity() {
        return (FragmentActivity) getContext();
    }

    private ActionBar getSupportActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }

    private class FragmentDrawerItemListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // Adding headerView to the listView the count have to starts with position-1
            int mRealPosition = position - 1 ;

            if (mRealPosition == 3)
            {
                if (mContext instanceof MainActivity) {
                    Intent a = new Intent(mContext, FilterTabbedActivity.class);
                    activity.startActivityForResult(a, Constants.REQUESTCODE_FILTER);
                    closeDrawer(lvDrawer);
                }
            }
            else
                selectDrawerItem(mRealPosition);
        }
    }

    private class FragmentNavItem {
        private final Class<? extends Fragment> fragmentClass;
        private final String title;
        private final Bundle fragmentArgs;


        public FragmentNavItem(String title, Class<? extends Fragment> fragmentClass) {
            this.fragmentClass = fragmentClass;
            this.fragmentArgs = null;
            this.title = title;
        }

        public Class<? extends Fragment> getFragmentClass() {
            return fragmentClass;
        }

        public String getTitle() {
            return title;
        }

        public Bundle getFragmentArgs() {
            return fragmentArgs;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof FragmentNavItem)) return false;

            FragmentNavItem that = (FragmentNavItem) o;

            if (!title.equals(that.title)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return title.hashCode();
        }
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(getActivity(), this, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    public boolean isOverviewByCountryContains(){
        boolean result = false;
        for (FragmentNavItem drawerItem : drawerNavItems){
            if ( drawerItem.getFragmentClass().getName().equalsIgnoreCase(OverviewByCountryFragment.class.getName()))
                result = true;

        }
        return result;
    }

}