package mobapply.freightexchange.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobapply.freightexchange.R;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class CreateFilterFragment extends Fragment  {
    public CreateFilterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_filter, container, false);
        return view;
    }


}
