package mobapply.freightexchange.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Created by  Ivan Dunskiy  on 4/6/15.
 *
 * Model for Order object
 */
public class Order implements Parcelable{
    @SerializedName("number")
    private final
    String number;
    @SerializedName("uuid")
    private final
    String uuid;
    @SerializedName("departureAddress")
    private final
    Address departureAddress;
    @SerializedName("destinationAddress")
    private final
    Address destinationAddress;
    @SerializedName("good")
    private final
    String good;
    @SerializedName("actualWeight")
    private final
    double actualWeight;
    @SerializedName("note1")
    private final
    String note1;
    @SerializedName("customerContactPerson")
    private final
    String customerContactPerson;
    @SerializedName("packageQuantity")
    private final
    int packageQuantity;
    @SerializedName("packageType")
    private final
    String packageType;
    @SerializedName("appointmentFrom")
    private final
    long appointmentFrom;
    @SerializedName("appointmentTill")
    private final
    long appointmentTill;
    @SerializedName("dispositionCode")
    private final
    String dispositionCode;
    @SerializedName("volume")
    private final
    double volume;
    @SerializedName("volumeWeight")
    private final
    double volumeWeight;
    @SerializedName("quantity")
    private final
    double quantity;
    @SerializedName("hazmatPoints")
    private final
    int hazmatPoints;
    @SerializedName("loadingMetre")
    private final
    double loadingMetre;
    @SerializedName("chargeWeight")
    private final
    double chargeWeight;
    @SerializedName("initialPrice")
    private double initialPrice;
    @SerializedName("status")
    private String orderStatus;
      private LatLng departurePoint ;
     private LatLng destinationPoint ;

    public Order(String number, String uuid, Address departureAddress, Address destinationAddress, String good, double actualWeight,
                 String note1, String customerContactPerson, int packageQuantity, String packageType,
                 long appointmentFrom, long appointmentTill,
                 String dispositionCode, double volume, double volumeWeight, double quantity, int hazmatPoints, double loadingMetre,
                 double chargeWeight, double initialPrice, String status) {
        this.number = number;
        this.uuid = uuid;
        this.departureAddress = departureAddress;
        this.destinationAddress = destinationAddress;
        this.good = good;
        this.actualWeight = actualWeight;
        this.note1 = note1;
        this.customerContactPerson = customerContactPerson;
        this.packageQuantity = packageQuantity;
        this.packageType = packageType;
        this.appointmentFrom = appointmentFrom;
        this.appointmentTill = appointmentTill;
        this.dispositionCode = dispositionCode;
        this.volume = volume;
        this.volumeWeight = volumeWeight;
        this.quantity = quantity;
        this.hazmatPoints = hazmatPoints;
        this.loadingMetre = loadingMetre;
        this.chargeWeight = chargeWeight;
        this.initialPrice = initialPrice;
        this.orderStatus = orderStatus;
    }

    public  LatLng getDeparturePoint() {
        return departurePoint;
    }

    public  void setDeparturePoint(LatLng departurePoint) {
        this.departurePoint = departurePoint;
    }

    public  LatLng getDestinationPoint() {
        return destinationPoint;
    }

    public  void setDestinationPoint(LatLng destinationPoint) {
        this.destinationPoint = destinationPoint;
    }

    @Override
    public String toString() {
        return "Order{" +
                "number='" + number + '\'' +
                ", uuid='" + uuid + '\'' +
                ", departureAddress=" + departureAddress +
                ", destinationAddress=" + destinationAddress +
                ", good='" + good + '\'' +
                ", actualWeight=" + actualWeight +
                ", note1='" + note1 + '\'' +
                ", customerContactPerson='" + customerContactPerson + '\'' +
                ", packageQuantity=" + packageQuantity +
                ", packageType='" + packageType + '\'' +
                ", appointmentFrom='" + appointmentFrom + '\'' +
                ", appointmentTill='" + appointmentTill + '\'' +
                ", dispositionCode='" + dispositionCode + '\'' +
                ", volume=" + volume +
                ", volumeWeight=" + volumeWeight +
                ", quantity=" + quantity +
                ", hazmatPoints=" + hazmatPoints +
                '}';
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public double getInitialPrice() {
        return initialPrice;
    }

    public double getChargeWeight() {
        return chargeWeight;
    }

    public double getLoadingMetre() {
        return loadingMetre;
    }

    public String getNumber() {
        return number;
    }

    public String getUuid() {
        return uuid;
    }

    public Address getDepartureAddress() {
        return departureAddress;
    }

    public Address getDestinationAddress() {
        return destinationAddress;
    }

    public String getGood() {
        return good;
    }

    public double getActualWeight() {
        return actualWeight;
    }

    public long getAppointmentFrom() {
        return appointmentFrom;
    }

    public String getAdditionalInfo() {
        return note1;
    }

    public String getCustomerContactPerson() {
        return customerContactPerson;
    }

    public int getPackageQuantity() {
        return packageQuantity;
    }

    public String getPackageType() {
        return packageType;
    }

    public long getAppointmentTill() {
        return appointmentTill;
    }

    public String getDispositionCode() {
        return dispositionCode;
    }

    public double getVolume() {
        return volume;
    }

    public double getVolumeWeight() {
        return volumeWeight;
    }

    public double getQuantity() {
        return quantity;
    }

    public int getHazmatPoints() {
        return hazmatPoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        return uuid.equals(order.uuid);

    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.number);
        dest.writeString(this.uuid);
        dest.writeParcelable(this.departureAddress, 0);
        dest.writeParcelable(this.destinationAddress, 0);
        dest.writeString(this.good);
        dest.writeDouble(this.actualWeight);
        dest.writeString(this.note1);
        dest.writeString(this.customerContactPerson);
        dest.writeInt(this.packageQuantity);
        dest.writeString(this.packageType);
        dest.writeLong(this.appointmentFrom);
        dest.writeLong(this.appointmentTill);
        dest.writeString(this.dispositionCode);
        dest.writeDouble(this.volume);
        dest.writeDouble(this.volumeWeight);
        dest.writeDouble(this.quantity);
        dest.writeInt(this.hazmatPoints);
        dest.writeDouble(this.loadingMetre);
        dest.writeDouble(this.chargeWeight);
        dest.writeDouble(this.initialPrice);
        dest.writeString(this.orderStatus);
        dest.writeParcelable(this.departurePoint, flags);
        dest.writeParcelable(this.destinationPoint, flags);
    }

    private Order(Parcel in) {
        this.number = in.readString();
        this.uuid = in.readString();
        this.departureAddress = in.readParcelable(Address.class.getClassLoader());
        this.destinationAddress = in.readParcelable(Address.class.getClassLoader());
        this.good = in.readString();
        this.actualWeight = in.readDouble();
        this.note1 = in.readString();
        this.customerContactPerson = in.readString();
        this.packageQuantity = in.readInt();
        this.packageType = in.readString();
        this.appointmentFrom = in.readLong();
        this.appointmentTill = in.readLong();
        this.dispositionCode = in.readString();
        this.volume = in.readDouble();
        this.volumeWeight = in.readDouble();
        this.quantity = in.readDouble();
        this.hazmatPoints = in.readInt();
        this.loadingMetre = in.readDouble();
        this.chargeWeight = in.readDouble();
        this.initialPrice = in.readDouble();
        this.orderStatus = in.readString();
        this.departurePoint = in.readParcelable(LatLng.class.getClassLoader());
        this.destinationPoint = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}
