package mobapply.freightexchange.views;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.model.statuses.OrderStatuses;
import mobapply.freightexchange.ui.activities.SpecificOrderActivity;

/**
 * Created by Ivan Dunskiy on 6/15/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class MySpecificOrderView {
    private static final String TAG = "MySpecificOrderView";
    private final Context mContext;
    private final RelativeLayout my_order_status_layout;
    private final Button btn_cancel_order;
    private final Button btn_change_order_status;
    private final LinearLayout specific_order_message_layout;
    private final ScrollView specific_main_scrollview;


    private EditText edit_proposal;
    private TextView specific_order_load;
    private TextView specific_order_weight_goods;
    private TextView specific_order_number;
    private TextView specific_order_dpre_date;
    private TextView specific_order_contact;
    private TextView specific_order_type;
    private TextView specific_order_charge_weight;
    private TextView specific_order_appoint_till;
    private TextView specific_order_dispocode;
    private TextView specific_order_volume_weight;
    private TextView specific_order_loading_metre;
    private TextView specific_order_volume;
    private TextView specific_order_hazpoints;
    private TextView specific_order_quantity;
    private TextView specific_additional_info;
    private TextView specific_order_dpre_city;
    private TextView specific_order_dest_city;
    private TextView specific_order_from_city;
    private TextView specific_order_from_country;
    private TextView specific_order_from_index;
    private TextView specific_order_to_city;
    private TextView specific_order_to_country;
    private TextView specific_order_to_index;
    private final TextView specific_order_status;
    private final TextView specific_order_initial_price;
    private SharedPreferences preferences;
    Button btn_send_proposal;
    Order mSpecificOrder;
    public MySpecificOrderView(Context context, View rootView)
    {
        mContext = context;
        preferences = mContext.getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
         /*
         * Defining the specific order fields
         */
        edit_proposal = (EditText)rootView.findViewById(R.id.proposal_edit);
        specific_order_load = (TextView)rootView.findViewById(R.id.specific_order_load);

        specific_order_from_city  = (TextView)rootView.findViewById(R.id.specific_order_from_city);
        specific_order_from_country  = (TextView)rootView.findViewById(R.id.specific_order_from_country);
        specific_order_from_index  = (TextView)rootView.findViewById(R.id.specific_order_from_index);

        specific_order_to_city  = (TextView)rootView.findViewById(R.id.specific_order_to_city);
        specific_order_to_country  = (TextView)rootView.findViewById(R.id.specific_order_to_country);
        specific_order_to_index  = (TextView)rootView.findViewById(R.id.specific_order_to_index);

        specific_order_weight_goods = (TextView)rootView.findViewById(R.id.specific_order_weight_goods);


        specific_order_number = (TextView)rootView.findViewById(R.id.specific_order_number);
        specific_order_dpre_date = (TextView)rootView.findViewById(R.id.specific_order_dpre_date);
        specific_order_initial_price = (TextView)rootView.findViewById(R.id.specific_order_initial_price);
        specific_order_status = (TextView)rootView.findViewById(R.id.specific_order_status);
        specific_order_contact = (TextView)rootView.findViewById(R.id.specific_order_contact);
        specific_order_type = (TextView)rootView.findViewById(R.id.specific_order_type);
        specific_order_charge_weight = (TextView)rootView.findViewById(R.id.specific_order_charge_weight);
        specific_order_appoint_till = (TextView)rootView.findViewById(R.id.specific_order_appoint_till);
        specific_order_dispocode = (TextView)rootView.findViewById(R.id.specific_order_dispocode);
        specific_order_volume_weight = (TextView)rootView.findViewById(R.id.specific_order_volume_weight);
        specific_order_loading_metre = (TextView)rootView.findViewById(R.id.specific_order_loading_metre);
        specific_order_volume = (TextView)rootView.findViewById(R.id.specific_order_volume);
        specific_order_hazpoints = (TextView)rootView.findViewById(R.id.specific_order_hazpoints);
        specific_order_quantity = (TextView)rootView.findViewById(R.id.specific_order_quantity);
        specific_additional_info = (TextView)rootView.findViewById(R.id.specific_additional_info);
        specific_order_dpre_city= (TextView)rootView.findViewById(R.id.specific_order_dpre_city);
        specific_order_dest_city= (TextView)rootView.findViewById(R.id.specific_order_dest_city);

        specific_main_scrollview = (ScrollView)rootView.findViewById(R.id.specific_main_scrollview);
        my_order_status_layout = (RelativeLayout)rootView.findViewById(R.id.my_order_status_layout);
        specific_order_message_layout = (LinearLayout)rootView.findViewById(R.id.specific_order_message_layout);
        btn_cancel_order  =  (Button) rootView.findViewById(R.id.btn_action_proposal);
        btn_change_order_status  =  (Button) rootView.findViewById(R.id.btn_change_order_status);

    }
    public void displayOrder(Order mSpecificOrder) {
        this.mSpecificOrder = mSpecificOrder;
        ((SpecificOrderActivity) mContext).getSupportActionBar().setTitle(mContext.getString(R.string.specific_order_title) + " " + mSpecificOrder.getNumber());
        specific_order_load.setText(Utils.getFormattedDate(mContext, mSpecificOrder.getAppointmentFrom()));

        if (mSpecificOrder.getDepartureAddress()!=null )
        {
            specific_order_from_city.setText(mSpecificOrder.getDepartureAddress().getCity());
            specific_order_from_country.setText(mSpecificOrder.getDepartureAddress().getCountry());
            specific_order_from_index.setText(mSpecificOrder.getDepartureAddress().getZipCode());
        }
        if (mSpecificOrder.getDestinationAddress()!=null )
        {
            specific_order_to_city.setText(mSpecificOrder.getDestinationAddress().getCity());
            specific_order_to_country.setText(mSpecificOrder.getDestinationAddress().getCountry());
            specific_order_to_index.setText(mSpecificOrder.getDestinationAddress().getZipCode());
        }
        final SpannableStringBuilder sb = new SpannableStringBuilder(Double.toString(mSpecificOrder.getActualWeight()) + " kg of " +
                mSpecificOrder.getGood());
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(bss, 0, Double.toString(mSpecificOrder.getActualWeight()).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        specific_order_weight_goods.setText(sb);

        specific_order_number.setText("# " + mSpecificOrder.getNumber());
        specific_order_dpre_date.setText(Utils.getFormattedDate(mContext, mSpecificOrder.getAppointmentFrom()));
        specific_order_dpre_city.setText(mSpecificOrder.getDepartureAddress().getCity());
        specific_order_dest_city.setText(mSpecificOrder.getDestinationAddress().getCity());
        specific_order_initial_price.setText(Double.toString(mSpecificOrder.getInitialPrice()));
        specific_order_status.setText(mSpecificOrder.getOrderStatus());
        specific_order_contact.setText(mSpecificOrder.getCustomerContactPerson());
        specific_order_type.setText(mSpecificOrder.getPackageType());
        specific_order_charge_weight.setText(Double.toString(mSpecificOrder.getChargeWeight()));
        specific_order_appoint_till.setText(Utils.getFormattedDate(mContext, mSpecificOrder.getAppointmentTill()));
        specific_order_dispocode.setText(mSpecificOrder.getDispositionCode());
        specific_order_volume_weight.setText(Double.toString(mSpecificOrder.getVolumeWeight()));
        specific_order_loading_metre.setText(Double.toString(mSpecificOrder.getLoadingMetre()));
        specific_order_volume.setText(Double.toString(mSpecificOrder.getVolume()));
        specific_order_hazpoints.setText(Integer.toString(mSpecificOrder.getHazmatPoints()));
        specific_order_quantity.setText(Double.toString(mSpecificOrder.getQuantity()));
        specific_additional_info.setText(mSpecificOrder.getAdditionalInfo());
        if (mSpecificOrder.getOrderStatus().equalsIgnoreCase(OrderStatuses.DONE.name()) |
                mSpecificOrder.getOrderStatus().equalsIgnoreCase(OrderStatuses.CANCELLED.name()) |
                mSpecificOrder.getOrderStatus().equalsIgnoreCase(OrderStatuses.NOT_ACTUAL.name()) |
                !Utils.getCurrentConfiguration(mContext).isOrdersProgressStatus())
        {
            my_order_status_layout.setVisibility(View.GONE);

        }
        else
        {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) specific_main_scrollview
                    .getLayoutParams();
            layoutParams.bottomMargin = 60;
            layoutParams.setMargins(0, 0, 0, 60);
            specific_main_scrollview.setLayoutParams(layoutParams);

            my_order_status_layout.setVisibility(View.VISIBLE);
        }

        if (Utils.getCurrentConfiguration(mContext).isOrdersProgressMessages())
            specific_order_message_layout.setVisibility(View.VISIBLE);
        else
            specific_order_message_layout.setVisibility(View.GONE);

        checkCurrentOrderStatus();
    }
    public void checkCurrentOrderStatus(){
        btn_change_order_status.setText(this.getNewOrderStatusText());
    }

    public String getNewOrderStatus() {
        OrderStatuses orderStatus = OrderStatuses.valueOf(mSpecificOrder.getOrderStatus());
        String nextStatus = null;
        switch (orderStatus){
            case ASSIGNED:
                nextStatus = OrderStatuses.ACKNOWLEDGED.name();
                break;
            case ACKNOWLEDGED:
                nextStatus = OrderStatuses.IN_PROGRESS.name();
                break;
            case IN_PROGRESS:
                nextStatus = OrderStatuses.DONE.name();
                break;
        }
        return nextStatus;
    }

    public String getNewOrderStatusText(){
        OrderStatuses orderStatus = OrderStatuses.valueOf(mSpecificOrder.getOrderStatus());
        String nextStatus = null;
        switch (orderStatus){
            case ASSIGNED:
                nextStatus = mContext.getString(R.string.btn_order_status_acknowledged);
                break;
            case ACKNOWLEDGED:
                nextStatus = mContext.getString(R.string.btn_order_status_in_progress);
                break;
            case IN_PROGRESS:
                nextStatus = mContext.getString(R.string.btn_order_status_done);
                break;
        }
        return nextStatus;
    }


    public Order getMySpecificOrder() {
        return mSpecificOrder;
    }
}
