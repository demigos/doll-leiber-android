package mobapply.freightexchange.library.webutils;

import android.content.Context;

import org.json.JSONObject;


/**
 * Created by Ivan Dunskiy on 07.03.2015.
 *
 * Class - helper for main client-server operations execution
 */
public class AuthenticationRequester {
    public static final int CODE_SUCCESS = 200;
    private static final int CODE_SUCCESS_DELETE = 204;
    private static final String OK = "ok";

    private static final String HOST = "";
    public static final String URL_USERS =  HOST + "users/";
    public static final String URL_ALL_ORDERS = HOST + "orders/";
    public static final String URL_NEW_ORDERS = HOST + "orders/?orderStatuses=NEW";
    public static final String URL_PROPOSALS =  HOST + "proposals/";

    public static final String URL_GET_COUNTRIES =  HOST + "addresses/countries";
    public static final String URL_GET_CITIES =  HOST + "addresses/cities";
    public static final String URL_GET_ZIPCODES =  HOST + "addresses/zipCodes";

    public static final String URL_LOGIN =  HOST + "users/accessTokens";

    public static final String URL_ORDER_FILTER =  HOST + "orders?";
    public static final String URL_OVERVIEW_BY_COUNTRY=  HOST + "orders?orderStatuses=NEW&grouped=true";

}
