package mobapply.freightexchange.library;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import mobapply.freightexchange.model.MyProposal;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.model.OverviewItem;

/**
 * Created by Ivan Dunskiy on 09.03.2015.
 *
 * Class-helper for JSON parsing
 */
public class JSONParser {

    public static List<OverviewItem> parseOverviewItems(JSONArray jsArray) {
        Type listType = new TypeToken<List<OverviewItem>>() {}.getType();
        Gson gson = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
        return ((List<OverviewItem>)gson.fromJson(jsArray.toString(), listType));

    }
    public static List<Order> parseOrders(JSONArray jsonObject)
    {
        Type listType = new TypeToken<List<Order>>() {}.getType();

        List<Order> orders;
        Gson gson = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
        orders = gson.fromJson(jsonObject.toString(), listType);
        return orders;
    }

    public static List<MyProposal> parseProposals(JSONArray jsonObject)
    {
        Type listType = new TypeToken<List<MyProposal>>() {}.getType();
        List<MyProposal> proposals;
        Gson gson = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
        proposals = gson.fromJson(jsonObject.toString(), listType);
        return proposals;
    }

    public static Order parseSpecificOrder(JSONObject order) {
        Gson gson  = new GsonBuilder().registerTypeAdapterFactory(new NullDoubleToEmptyAdapterFactory<>()).create();
        return gson.fromJson(order.toString(), Order.class);
    }

    public static MyProposal parseSpecificProposal(JSONObject json) {
        Gson gson  = new GsonBuilder().registerTypeAdapterFactory(new NullDoubleToEmptyAdapterFactory<>()).create();
        return gson.fromJson(json.toString(), MyProposal.class);
    }


    private static class NullStringToEmptyAdapterFactory<T> implements TypeAdapterFactory {
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

            Class<T> rawType = (Class<T>) type.getRawType();
            if (rawType != String.class) {
                return null;
            }
            return (TypeAdapter<T>) new StringAdapter();
        }
    }

    private static class StringAdapter extends TypeAdapter<String> {

        @Override
        public void write(com.google.gson.stream.JsonWriter writer, String value) throws IOException {
            if (value == null) {
                writer.nullValue();
                return;
            }
            writer.value(value);
        }

        @Override
        public String read(com.google.gson.stream.JsonReader reader) throws IOException {
            if (reader.peek() == com.google.gson.stream.JsonToken.NULL) {
                reader.nextNull();
                return "No Data ";
            }
            return reader.nextString();
        }
    }



    private static class NullDoubleToEmptyAdapterFactory<T> implements TypeAdapterFactory {
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

            Class<T> rawType = (Class<T>) type.getRawType();
            if (rawType != Double.class) {
                return null;
            }
            return (TypeAdapter<T>) new DoubleAdapter();
        }
    }

    private static class DoubleAdapter extends TypeAdapter<Double> {

        @Override
        public void write(com.google.gson.stream.JsonWriter writer, Double value) throws IOException {
            if (Double.toString(value).equalsIgnoreCase("")) {
                writer.nullValue();
                return;
            }
            writer.value(value);
        }

        @Override
        public Double read(com.google.gson.stream.JsonReader reader) throws IOException {
            if (reader.peek() == com.google.gson.stream.JsonToken.NULL) {
                reader.nextNull();
                return 0.0;
            }
            return reader.nextDouble();
        }
    }
}
