package mobapply.freightexchange.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misha Nester on 15.05.15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * Model for OverView By Country item
 */
public class OverviewItem implements Parcelable {
    @SerializedName("fromCountry")
    private final
    String fromCountry;
    @SerializedName("toCountry")
    private final
    String toCountry;
    @SerializedName("count")
    private final
    String count;

    @Override
    public String toString() {
        return fromCountry +System.getProperty("line.separator") +
                toCountry + System.getProperty("line.separator") +
                count;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fromCountry);
        dest.writeString(this.toCountry);
        dest.writeString(this.count);
    }

    private OverviewItem(Parcel in) {
        this.fromCountry = in.readString();
        this.toCountry = in.readString();
        this.count = in.readString();
    }

    public static final Creator<OverviewItem> CREATOR = new Creator<OverviewItem>() {
        public OverviewItem createFromParcel(Parcel source) {
            return new OverviewItem(source);
        }

        public OverviewItem[] newArray(int size) {
            return new OverviewItem[size];
        }
    };

    public String getFromCountry() {
        return fromCountry;
    }

    public String getToCountry() {
        return toCountry;
    }

    public String getCount() {
        return count;
    }
}
