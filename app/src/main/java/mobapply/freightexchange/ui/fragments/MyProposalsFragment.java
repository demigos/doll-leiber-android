package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.interfaces.SortingItems;
import mobapply.freightexchange.model.MyProposal;
import mobapply.freightexchange.model.SortingDirections;
import mobapply.freightexchange.model.SortingStates;
import mobapply.freightexchange.providers.MyProposalsListProvider;
import mobapply.freightexchange.ui.activities.MainActivity;
import mobapply.freightexchange.views.MyProposalsView;

/**
 * Created by Ivan Dunskiy on 4/6/15.
   Copyright (c) 2015 Rarus. All rights reserved.
 */
public class MyProposalsFragment extends Fragment  implements MyProposalsListProvider.MyProposalsCallback, View.OnClickListener, SortingItems {
    private ArrayList<MyProposal> proposalArrayList = new ArrayList<>();
    private SharedPreferences preferences;
     private RecyclerView recyclerView;
    private ProgressDialog mProgressDialog;
    private MyProposalsListProvider mProposalsProvider;
    private MyProposalsView mProposalsView;
    private SwipeRefreshLayout mSwipeContainer;

    private TextView label_order_date;
    private AutoCompleteTextView label_order_goods;
    private TextView label_order_info;
    private AutoCompleteTextView label_order_to;
    private AutoCompleteTextView label_order_from;

    boolean mDateDecrease;
    boolean mAdditionalDecrease;
    boolean mCityToDecrease;
    private boolean mCountryToDecrease;
    private boolean mCityFromDecrease;
    private boolean  mCountryFromDecrease;
    private boolean mZipcodeFromDecrease;
    private boolean mZipcodeToDecrease;
    private boolean mGoodsToDecrease;
    private boolean mWeightToDecrease;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =   LayoutInflater.from(container.getContext())
                .inflate(R.layout.fragment_my_proposals, container, false);
        ((MainActivity) getActivity()). getSupportActionBar().show();
        preferences= getActivity().getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        mProposalsView = new MyProposalsView(getActivity(),view);
        mProposalsProvider = new MyProposalsListProvider(getActivity());
        mSwipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        mProposalsProvider.loadProposals(this);
        mProposalsView.setiSortingItems(this);
        label_order_date = (TextView) view.findViewById(R.id.label_order_date);
        label_order_goods = (AutoCompleteTextView) view.findViewById(R.id.label_order_goods);
        label_order_info = (TextView) view.findViewById(R.id.label_order_info);
        label_order_to = (AutoCompleteTextView) view.findViewById(R.id.label_order_to);
        label_order_from = (AutoCompleteTextView) view.findViewById(R.id.label_order_from);

        label_order_date.setOnClickListener(this);
        label_order_goods.setOnClickListener(this);
        label_order_info.setOnClickListener(this);
        label_order_to.setOnClickListener(this);
        label_order_from.setOnClickListener(this);

        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                mSwipeContainer.setRefreshing(false);
                mProposalsProvider.loadProposals(MyProposalsFragment.this);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode==Constants.REQUESTCODE_REJECT_PROPOSAL)
            {
                mProposalsProvider.loadProposals(this);
            }
        }
    }

    public void reloadProposals()
    {
        mProposalsProvider.loadProposals(this);
    }

    @Override
    public void onProposalsLoaded(List<MyProposal> proposalList) {
        mProposalsView.displayProposals(getActivity(), proposalList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.label_order_date:
                if (!mDateDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        public int compare(MyProposal o1, MyProposal o2) {
                            if (o1.getMyProposalOrder().getAppointmentFrom() == 0 || o2.getMyProposalOrder().getAppointmentFrom() == 0)
                                return 0;
                            return Long.valueOf(o1.getMyProposalOrder().getAppointmentFrom()).compareTo(Long.valueOf(o2.getMyProposalOrder().getAppointmentFrom()));
                        }
                    });
                    mProposalsView.getProposalsAdapter().highlighLabel(SortingStates.DATE);
                    mProposalsView.notifyDataChanged();
                    setSortingImage(label_order_date, SortingDirections.INCREASE);
                    mDateDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        public int compare(MyProposal o1, MyProposal o2) {
                            if (o1.getMyProposalOrder().getAppointmentFrom() == 0 || o2.getMyProposalOrder().getAppointmentFrom() == 0)
                                return 0;
                            return Long.valueOf(o2.getMyProposalOrder().getAppointmentFrom()).compareTo(Long.valueOf(o1.getMyProposalOrder().getAppointmentFrom()));
                        }
                    });
                    mProposalsView.notifyDataChanged();
                    setSortingImage(label_order_date,SortingDirections.DECREASE);
                    mDateDecrease = false;
                }
                break;
            case R.id.label_order_goods:
                mProposalsView.invalidateSortingArrays();
                label_order_goods.showDropDown();
                break;

            case R.id.label_order_to:
                mProposalsView.invalidateSortingArrays();
                label_order_to.showDropDown();
                break;

            case R.id.label_order_from:
                mProposalsView.invalidateSortingArrays();
                label_order_from.showDropDown();
                break;

            case R.id.label_order_info:
                if (!mAdditionalDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return Float.valueOf(s1.getCostOfFreight()).compareTo(Float.valueOf(s2.getCostOfFreight()));
                        }
                    });
                    setSortingImage(label_order_info,SortingDirections.INCREASE);
                    mAdditionalDecrease = true;
                }
                else
                {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return Float.valueOf(s2.getCostOfFreight()).compareTo(Float.valueOf(s1.getCostOfFreight()));
                        }
                    });
                    setSortingImage(label_order_info,SortingDirections.DECREASE);
                    mAdditionalDecrease = false;
                }
                mProposalsView.getProposalsAdapter().highlighLabel(SortingStates.INFO);
                mProposalsView.notifyDataChanged();
                break;
        }
    }
    @Override
    public void sortItems(SortingStates sortingStates) {
        switch (sortingStates){
            case CITY_TO:
                if (!mCityToDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s1.getMyProposalOrder().getDestinationAddress().getCity().compareToIgnoreCase(s2.getMyProposalOrder().getDestinationAddress().getCity());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.INCREASE);
                    mCityToDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s2.getMyProposalOrder().getDestinationAddress().getCity().compareToIgnoreCase(s1.getMyProposalOrder().getDestinationAddress().getCity());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.DECREASE);
                    mCityToDecrease = false;
                }
                mProposalsView.notifyDataChanged();
                break;
            case CITY_FROM:
                if (!mCityFromDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s1.getMyProposalOrder().getDepartureAddress().getCity().compareToIgnoreCase(s2.getMyProposalOrder().getDepartureAddress().getCity());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.INCREASE);
                    mCityFromDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s2.getMyProposalOrder().getDepartureAddress().getCity().compareToIgnoreCase(s1.getMyProposalOrder().getDepartureAddress().getCity());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.DECREASE);
                    mCityFromDecrease = false;
                }
                mProposalsView.notifyDataChanged();
                break;
            case COUNTRY_TO:
                if (!mCountryToDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s1.getMyProposalOrder().getDestinationAddress().getCountry().compareToIgnoreCase(s2.getMyProposalOrder().getDestinationAddress().getCountry());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.INCREASE);
                    mCountryToDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s2.getMyProposalOrder().getDestinationAddress().getCountry().compareToIgnoreCase(s1.getMyProposalOrder().getDestinationAddress().getCountry());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.DECREASE);
                    mCountryToDecrease = false;
                }
                mProposalsView.notifyDataChanged();
                break;
            case COUNTRY_FROM:
                if (!mCountryFromDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s1.getMyProposalOrder().getDepartureAddress().getCountry().compareToIgnoreCase(s2.getMyProposalOrder().getDepartureAddress().getCountry());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.INCREASE);
                    mCountryFromDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s2.getMyProposalOrder().getDepartureAddress().getCountry().compareToIgnoreCase(s1.getMyProposalOrder().getDepartureAddress().getCountry());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.DECREASE);
                    mCountryFromDecrease = false;
                }
                mProposalsView.notifyDataChanged();
                break;
            case ZIPCODE_FROM:
                if (!mZipcodeFromDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s1.getMyProposalOrder().getDepartureAddress().getZipCode().compareToIgnoreCase(s2.getMyProposalOrder().getDepartureAddress().getZipCode());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.INCREASE);
                    mZipcodeFromDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s2.getMyProposalOrder().getDepartureAddress().getZipCode().compareToIgnoreCase(s1.getMyProposalOrder().getDepartureAddress().getZipCode());
                        }
                    });
                    setSortingImage(label_order_from,SortingDirections.DECREASE);
                    mZipcodeFromDecrease = false;
                }
                mProposalsView.notifyDataChanged();
                break;
            case ZIPCODE_TO:
                if (!mZipcodeToDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s1.getMyProposalOrder().getDestinationAddress().getZipCode().compareToIgnoreCase(s2.getMyProposalOrder().getDestinationAddress().getZipCode());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.INCREASE);
                    mZipcodeToDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s2.getMyProposalOrder().getDestinationAddress().getZipCode().compareToIgnoreCase(s1.getMyProposalOrder().getDestinationAddress().getZipCode());
                        }
                    });
                    setSortingImage(label_order_to,SortingDirections.DECREASE);
                    mZipcodeToDecrease = false;
                }
                mProposalsView.notifyDataChanged();
                break;
            case GOODS:
                if (!mGoodsToDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s1.getMyProposalOrder().getGood().compareToIgnoreCase(s2.getMyProposalOrder().getGood());
                        }
                    });
                    setSortingImage(label_order_goods,SortingDirections.INCREASE);
                    mGoodsToDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return s2.getMyProposalOrder().getGood().compareToIgnoreCase(s1.getMyProposalOrder().getGood());
                        }
                    });
                    setSortingImage(label_order_goods,SortingDirections.DECREASE);
                    mGoodsToDecrease = false;
                }
                mProposalsView.notifyDataChanged();
                break;
            case WEIGHT:
                if (!mWeightToDecrease) {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return Double.compare(s1.getMyProposalOrder().getActualWeight(), s2.getMyProposalOrder().getActualWeight());
                        }
                    });
                    setSortingImage(label_order_goods,SortingDirections.INCREASE);
                    mWeightToDecrease = true;
                }
                else {
                    Collections.sort(mProposalsView.getProposalsList(), new Comparator<MyProposal>() {
                        @Override
                        public int compare(MyProposal s1, MyProposal s2) {
                            return Double.compare(s2.getMyProposalOrder().getActualWeight(), s1.getMyProposalOrder().getActualWeight());
                        }
                    });
                    setSortingImage(label_order_goods,SortingDirections.DECREASE);
                    mWeightToDecrease = false;
                }
                mProposalsView.notifyDataChanged();
                break;
        }
    }

    private void setSortingImage(TextView textView, SortingDirections directions){
        switch (directions){
            case INCREASE:
                Drawable img = getResources().getDrawable( R.drawable.ic_sort_increasing );
                int h = img.getIntrinsicHeight();
                int w = img.getIntrinsicWidth();
                img.setBounds( 0, 0, w, h );
                textView.setCompoundDrawables( img, null,null , null);
                break;
            case DECREASE:
                Drawable img2 = getResources().getDrawable( R.drawable.ic_sort_decreasing );
                int h2 = img2.getIntrinsicHeight();
                int w2 = img2.getIntrinsicWidth();
                img2.setBounds( 0, 0, w2, h2 );
                textView.setCompoundDrawables(img2 , null, null , null);
                break;
        }
        switch (textView.getId()){
            case R.id.label_order_date:
                label_order_from.setCompoundDrawables( null, null, null, null);
                label_order_to.setCompoundDrawables( null, null, null, null);
                label_order_goods.setCompoundDrawables( null, null, null, null);
                label_order_info.setCompoundDrawables( null, null, null, null);
                break;
            case R.id.label_order_from:
                label_order_date.setCompoundDrawables( null, null, null, null);
                label_order_to.setCompoundDrawables( null, null, null, null);
                label_order_goods.setCompoundDrawables( null, null, null, null);
                label_order_info.setCompoundDrawables( null, null, null, null);
                break;
            case R.id.label_order_to:
                label_order_date.setCompoundDrawables( null, null, null, null);
                label_order_from.setCompoundDrawables( null, null, null, null);
                label_order_goods.setCompoundDrawables( null, null, null, null);
                label_order_info.setCompoundDrawables( null, null, null, null);
                break;
            case R.id.label_order_goods:
                label_order_date.setCompoundDrawables( null, null, null, null);
                label_order_from.setCompoundDrawables( null, null, null, null);
                label_order_to.setCompoundDrawables( null, null, null, null);
                label_order_info.setCompoundDrawables( null, null, null, null);
                break;
            case R.id.label_order_info:
                label_order_date.setCompoundDrawables( null, null, null, null);
                label_order_from.setCompoundDrawables( null, null, null, null);
                label_order_to.setCompoundDrawables( null, null, null, null);
                label_order_goods.setCompoundDrawables( null, null, null, null);
                break;

        }

    }

}
