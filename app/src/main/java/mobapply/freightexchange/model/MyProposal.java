package mobapply.freightexchange.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by  Ivan Dunskiy  on 4/6/15.
 *
 * Model for My Proposal object
 */
public class MyProposal implements Parcelable {

    @SerializedName("uuid")
    private final
    String uuid;
    @SerializedName("order")
    private final
    Order myProposalOrder;
    @SerializedName("costOfFreight")
    private final
    float costOfFreight;
    @SerializedName("status")
    private final
    String status;

    public MyProposal(String uuid, Order myProposalOrder, float costOfFreight, String status) {
        this.uuid = uuid;
        this.myProposalOrder = myProposalOrder;
        this.costOfFreight = costOfFreight;
        this.status = status;
    }

    @Override
    public String toString() {
        return "MyProposal{" +
                "uuid='" + uuid + '\'' +
                ", myProposalOrder=" + myProposalOrder +
                ", costOfFreight=" + costOfFreight +
                ", status='" + status + '\'' +
                '}';
    }

    public String getUuid() {
        return uuid;
    }

    public Order getMyProposalOrder() {
        return myProposalOrder;
    }

    public float getCostOfFreight() {
        return costOfFreight;
    }

    public String getStatus() {
        return status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uuid);
        dest.writeParcelable(this.myProposalOrder, 0);
        dest.writeFloat(this.costOfFreight);
        dest.writeString(this.status);
    }

    private MyProposal(Parcel in) {
        this.uuid = in.readString();
        this.myProposalOrder = in.readParcelable(Order.class.getClassLoader());
        this.costOfFreight = in.readFloat();
        this.status = in.readString();
    }

    public static final Parcelable.Creator<MyProposal> CREATOR = new Parcelable.Creator<MyProposal>() {
        public MyProposal createFromParcel(Parcel source) {
            return new MyProposal(source);
        }

        public MyProposal[] newArray(int size) {
            return new MyProposal[size];
        }
    };
}
