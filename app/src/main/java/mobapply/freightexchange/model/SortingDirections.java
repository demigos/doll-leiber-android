package mobapply.freightexchange.model;

/**
 * Created by Ivan Dunskiy on 7/19/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public enum SortingDirections {
    INCREASE{
        @Override
        public String toString() {
            return "increase";
        }
    },
    DECREASE{
        @Override
        public String toString() {
            return "decrease";
        }
    },
}
