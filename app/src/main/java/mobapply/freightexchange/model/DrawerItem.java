package mobapply.freightexchange.model;

/**
 * Created by Ivan Dunskiy on 4/16/15.
 *
 * Model for Navigation Drawer list item
 */
public class DrawerItem{
        private final String ItemName;
        private final int imgResID;

public DrawerItem(String itemName, int imgResID) {
        super();
        ItemName = itemName;
        this.imgResID = imgResID;
        }

public String getItemName() {
        return ItemName;
        }

    public int getImgResID() {
        return imgResID;
        }

}