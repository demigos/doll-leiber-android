package mobapply.freightexchange.customviews;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.interfaces.OverviewCountryHandler;
import mobapply.freightexchange.model.OverviewItem;

/**
 * Created by Ivan Dunskiy on 15.05.15.
 *
 * Table Layout for OverView By Country items displaying
 */
public class TableMainLayout extends RelativeLayout implements View.OnClickListener {
    private TableLayout tableA;
    private TableLayout tableB;
    private TableLayout tableC;
    private TableLayout tableD;
    private HorizontalScrollView horizontalScrollViewB;
    private HorizontalScrollView horizontalScrollViewD;
    private ScrollView scrollViewC;
    private ScrollView scrollViewD;
    private final Context context;

    private ArrayList<String> headersArray = new ArrayList<>();
    private ArrayList<String> rowsArray = new ArrayList<>();
    private ArrayList<OverviewItem> overviewList = new ArrayList<>();
    private final List<SampleObject> sampleObjects;
    private final int[] headerCellsWidth;
    private OverviewCountryHandler overviewCountryHandlerListener;
    public TableMainLayout(Context context, ArrayList<String> headersArray, ArrayList<String> rowsArray, ArrayList<OverviewItem> overviewList) {
        super(context);
        this.headersArray = headersArray;
        this.rowsArray = rowsArray;
        this.overviewList = overviewList;
        sampleObjects = this.sampleObjects();
        this.headersArray.add("");
        headerCellsWidth = new int[headersArray.size()];
        this.context = context;
        // initialize the main components (TableLayouts, HorizontalScrollView, ScrollView)
        this.initComponents();
        this.setComponentsId();
        this.setScrollViewAndHorizontalScrollViewTag();

        // no need to assemble component A, since it is just a table
        this.horizontalScrollViewB.addView(this.tableB);
        this.scrollViewC.addView(this.tableC);
        this.scrollViewD.addView(this.horizontalScrollViewD);
        this.horizontalScrollViewD.addView(this.tableD);

        // add the components to be part of the main layout
        this.addComponentToMainLayout();
        this.setBackgroundColor(Color.WHITE);

        // add some table rows
        this.addTableRowToTableA();
        this. addTableRowToTableB();
        this.resizeHeaderHeight();
        this.getTableRowHeaderCellWidth();
        this.generateTableC_AndTable_B();
        this.resizeBodyTableRowHeight();
    }

    // this is just the sample data
    List<SampleObject> sampleObjects() {
        List<SampleObject> sampleObjects = new ArrayList<>();

        for (int i = 0; i < rowsArray.size(); i++) {
            SampleObject sampleObject = new SampleObject(headersArray.size());

            for (int k = 0; k < overviewList.size(); k++) {
                if (rowsArray.get(i).equals(overviewList.get(k).getFromCountry())) {
                    for (int h = 0; h < headersArray.size(); h++) {
                        if (headersArray.get(h).equals(overviewList.get(k).getToCountry())) {
                            sampleObject.replaceItem(h, overviewList.get(k).getCount());
                            sampleObject.replaceItem(0, rowsArray.get(i));
                            break;
                        }
                    }

                    boolean objectHasVaule = false;

                    for (int s = 0; s < sampleObjects.size(); s++) {
                        if (sampleObjects.get(s).rowItems[0].equals(overviewList.get(k).getFromCountry())) {
                            objectHasVaule = true;
                        }
                    }
                    if (!objectHasVaule) {
                        sampleObjects.add(sampleObject);
                    }
                }
            }
        }

        return sampleObjects;
    }

    // initalized components
    private void initComponents() {
        this.tableA = new TableLayout(this.context);
        this.tableB = new TableLayout(this.context);
        this.tableC = new TableLayout(this.context);
        this.tableD = new TableLayout(this.context);

        this.horizontalScrollViewB = new MyHorizontalScrollView(this.context);
        this.horizontalScrollViewD = new MyHorizontalScrollView(this.context);
        horizontalScrollViewB.setHorizontalScrollBarEnabled(false);

        this.scrollViewC = new MyScrollView(this.context);
        this.scrollViewD = new MyScrollView(this.context);
        this.scrollViewC.setVerticalScrollBarEnabled(false);

        this.tableA.setBackgroundColor(Color.LTGRAY);
        this.horizontalScrollViewB.setBackgroundColor(Color.LTGRAY);
    }

    // set essential component IDs
    private void setComponentsId() {
        this.tableA.setId(1);
        this.horizontalScrollViewB.setId(2);
        this.scrollViewC.setId(3);
        this.scrollViewD.setId(4);
    }

    // set tags for some horizontal and vertical scroll view
    private void setScrollViewAndHorizontalScrollViewTag() {
        this.horizontalScrollViewB.setTag("horizontal scroll view b");
        this.horizontalScrollViewD.setTag("horizontal scroll view d");

        this.scrollViewC.setTag("scroll view c");
        this.scrollViewD.setTag("scroll view d");
    }

    // we add the components here in our TableMainLayout
    private void addComponentToMainLayout() {
        // RelativeLayout params were very useful here
        // the addRule method is the key to arrange the components properly
        RelativeLayout.LayoutParams componentB_Params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        componentB_Params.addRule(RelativeLayout.RIGHT_OF, this.tableA.getId());

        RelativeLayout.LayoutParams componentC_Params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        componentC_Params.addRule(RelativeLayout.BELOW, this.tableA.getId());

        RelativeLayout.LayoutParams componentD_Params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
        componentD_Params.addRule(RelativeLayout.RIGHT_OF, this.scrollViewC.getId());
        componentD_Params.addRule(RelativeLayout.BELOW, this.horizontalScrollViewB.getId());

        // 'this' is a relative layout,
        // we extend this table layout as relative layout as seen during the creation of this class
        this.addView(this.tableA);
        this.addView(this.horizontalScrollViewB, componentB_Params);
        this.addView(this.scrollViewC, componentC_Params);
        this.addView(this.scrollViewD, componentD_Params);
    }

    private void addTableRowToTableA() {
        this.tableA.addView(this.componentATableRow());
    }

    private void addTableRowToTableB() {
        this.tableB.addView(this.componentBTableRow());
    }

    // generate table row of table A
    TableRow componentATableRow() {

        TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
        params.setMargins(0, 0, 1, 1);

        TableRow componentATableRow = new TableRow(this.context);
        TextView textView = this.headerTextView(this.headersArray.get(0));
        textView.setPadding(50, 0, 50, 0);
        textView.setLayoutParams(params);
        textView.setTag(00);
        componentATableRow.addView(textView);

        return componentATableRow;
    }

    // generate table row of table B
    TableRow componentBTableRow() {
        TableRow componentBTableRow = new TableRow(this.context);
        TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        params.setMargins(0, 0, 1, 1);

        for(int x = 1; x < headersArray.size(); x++){
            TextView textView = this.headerTextView(headersArray.get(x));
            textView.setLayoutParams(params);
            textView.setTag("0." + x);
            componentBTableRow.addView(textView);
        }

        return componentBTableRow;
    }

    // generate table row of table C and table D
    private void generateTableC_AndTable_B() {
        int i = 1;
        for(SampleObject sampleObject : this.sampleObjects){
            TableRow tableRowForTableC = this.tableRowForTableC(sampleObject, i);
            TableRow taleRowForTableD = this.taleRowForTableD(sampleObject, i);

            tableRowForTableC.setBackgroundColor(Color.LTGRAY);
            taleRowForTableD.setBackgroundColor(Color.LTGRAY);

            this.tableC.addView(tableRowForTableC);
            this.tableD.addView(taleRowForTableD);
            i++;
        }
    }

    // a TableRow for table C
    TableRow tableRowForTableC(SampleObject sampleObject, int i) {
        TableRow.LayoutParams params = new TableRow.LayoutParams( this.headerCellsWidth[0],LayoutParams.MATCH_PARENT);
        params.setMargins(0, 0, 1, 1);

        TableRow tableRowForTableC = new TableRow(this.context);
        TextView textView = this.bodyTextView("von " + sampleObject.rowItems[0]);
        textView.setTag(i + ".0");
        tableRowForTableC.addView(textView, params);

        return tableRowForTableC;
    }

    TableRow taleRowForTableD(SampleObject sampleObject, int i) {
        TableRow taleRowForTableD = new TableRow(this.context);
        int loopCount = ((TableRow)this.tableB.getChildAt(0)).getChildCount();

        for(int x = 1 ; x < loopCount; x++){
            TableRow.LayoutParams params = new TableRow.LayoutParams(headerCellsWidth[x],LayoutParams.MATCH_PARENT);
            params.setMargins(0, 0, 1, 1);

            if (x == (loopCount - 1))
                params.setMargins(0, 0, 1, 1);

            TextView textViewB = this.bodyTextView(sampleObject.rowItems[x]);
            textViewB.setTag(i + "." + x);
            taleRowForTableD.addView(textViewB, params);
        }

        return taleRowForTableD;
    }

    // table cell standard TextView
    TextView bodyTextView(String label) {
        TextView bodyTextView = new TextView(this.context);
        bodyTextView.setBackgroundColor(Color.WHITE);
        bodyTextView.setText(label);
        bodyTextView.setGravity(Gravity.CENTER);
        bodyTextView.setTextColor(getResources().getColor(R.color.overview_body_text));
        bodyTextView.setPadding(35, 20, 35, 20);
        bodyTextView.setOnClickListener(this);

        return bodyTextView;
    }

    // header standard TextView
    TextView headerTextView(String label) {
        TextView headerTextView = new TextView(this.context);
        headerTextView.setBackgroundColor(getResources().getColor(R.color.overview_table_title));
        headerTextView.setText(label);
        headerTextView.setShadowLayer(1.5f, -1, 1, Color.LTGRAY);
        headerTextView.setGravity(Gravity.CENTER);
        headerTextView.setTextColor(getResources().getColor(R.color.overview_title_text));
        headerTextView.setPadding(35, 35, 35, 35);

        return headerTextView;
    }

    // resizing TableRow height starts here
    void resizeHeaderHeight() {
        TableRow productNameHeaderTableRow = (TableRow) this.tableA.getChildAt(0);
        TableRow productInfoTableRow = (TableRow)  this.tableB.getChildAt(0);

        int rowAHeight = this.viewHeight(productNameHeaderTableRow);
        int rowBHeight = this.viewHeight(productInfoTableRow);

        TableRow tableRow = rowAHeight < rowBHeight ? productNameHeaderTableRow : productInfoTableRow;
        int finalHeight = rowAHeight > rowBHeight ? rowAHeight : rowBHeight;

        this.matchLayoutHeight(tableRow, finalHeight);
    }

    void getTableRowHeaderCellWidth() {
        int tableAChildCount = ((TableRow)this.tableA.getChildAt(0)).getChildCount();
        int tableBChildCount = ((TableRow)this.tableB.getChildAt(0)).getChildCount();

        for (int x = 0; x < (tableAChildCount + tableBChildCount); x++) {
            if(x == 0) {
                this.headerCellsWidth[x] = this.viewWidth(((TableRow)this.tableA.getChildAt(0)).getChildAt(x));

            } else {
                this.headerCellsWidth[x] = this.viewWidth(((TableRow)this.tableB.getChildAt(0)).getChildAt(x - 1));
            }
        }
    }

    // resize body table row height
    void resizeBodyTableRowHeight() {
        int tableC_ChildCount = this.tableC.getChildCount();

        for (int x = 0; x < tableC_ChildCount; x++) {
            TableRow productNameHeaderTableRow = (TableRow) this.tableC.getChildAt(x);
            TableRow productInfoTableRow = (TableRow)  this.tableD.getChildAt(x);

            int rowAHeight = this.viewHeight(productNameHeaderTableRow);
            int rowBHeight = this.viewHeight(productInfoTableRow);

            TableRow tableRow = rowAHeight < rowBHeight ? productNameHeaderTableRow : productInfoTableRow;
            int finalHeight = rowAHeight > rowBHeight ? rowAHeight : rowBHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }
    }

    // match all height in a table row
    // to make a standard TableRow height
    private void matchLayoutHeight(TableRow tableRow, int height) {
        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {
            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return ;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }
    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {
        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }

        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    @Override
    public void onClick(View v) {
        String d = v.getTag().toString();
        String fromWhere = rowsArray.get(Integer.parseInt(d.substring(0, d.indexOf("."))) - 1);
        String whereTo = headersArray.get(Integer.parseInt(d.substring(d.indexOf(".") + 1, d.length())));
        overviewCountryHandlerListener.onDirectionChoosed(fromWhere, whereTo);
    }

    // horizontal scroll view custom class
    class MyHorizontalScrollView extends HorizontalScrollView {

        public MyHorizontalScrollView(Context context) {
            super(context);
        }

        @Override
        protected void onScrollChanged(int l, int t, int oldl, int oldt) {
            String tag = (String) this.getTag();

            if(tag.equalsIgnoreCase("horizontal scroll view b")) {
                horizontalScrollViewD.scrollTo(l, 0);

            } else {
                horizontalScrollViewB.scrollTo(l, 0);
            }
        }
    }

    // scroll view custom class
    class MyScrollView extends ScrollView {

        public MyScrollView(Context context) {
            super(context);
        }

        @Override
        protected void onScrollChanged(int l, int t, int oldl, int oldt) {

            String tag = (String) this.getTag();

            if (tag.equalsIgnoreCase("scroll view c")) {
                scrollViewD.scrollTo(0, t);

            } else {
                scrollViewC.scrollTo(0, t);
            }
        }
    }

    public void setOverviewCountryHandlerListener(OverviewCountryHandler overviewCountryHandlerListener) {
        this.overviewCountryHandlerListener = overviewCountryHandlerListener;
    }
}