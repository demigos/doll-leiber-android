package mobapply.freightexchange.ui.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.interfaces.VolleyCallback;


public class LoginActivity extends Activity implements LoaderCallbacks<Cursor> {

    private static final String TAG = "LoginActivity";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private ImageView btn_settings_host;
    SharedPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        mPreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        mPreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        btn_settings_host = (ImageView)findViewById(R.id.btn_settings_host);

        btn_settings_host.setOnClickListener(new OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();
                View dialoglayout = inflater.inflate(R.layout.fragment_dialog_host, null);
                final EditText hostEdit = (EditText) dialoglayout.findViewById(R.id.edit_host_change);
                new AlertDialog.Builder(LoginActivity.this)
                        .setView(dialoglayout)
                        .setTitle(R.string.label_change_host)
                        .setMessage(R.string.label_change_host_description)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (hostEdit.getText().toString()!=null && !hostEdit.getText().toString().isEmpty())
                                {
                                    mPreferences.edit().putString(Constants.SHARED_CURRENT_HOST,hostEdit.getText().toString()).commit();

                                }
                                else
                                {
                                    mPreferences.edit().putString(Constants.SHARED_CURRENT_HOST,"").commit();
                                    Toast.makeText(LoginActivity.this, getString(R.string.toast_empty_host),Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(R.drawable.ic_launcher)
                        .show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( getLoggedIn()!=null){

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_dropdown_item_1line, getLoggedIn());
            mEmailView.setAdapter(adapter);
        }
    }
    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
                String url = Utils.getRequestUrl(this, WebRequestTypes.LOGIN);
                JSONObject userJson = new JSONObject();
                try {
                    userJson.put("userName", email);
                    userJson.put("password", password);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mPreferences.edit().remove(Constants.SHARED_CURRENT_USERNAME).commit();
                mPreferences.edit().remove(Constants.SHARED_CURRENT_ACCESS_TOKEN).commit();
//                HttpsTrustManager.allowAllSSL();
                Utils.sendNewJsonVolleyRequest(this, Request.Method.POST, url, userJson, new VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.get("userUuid") != null)
                            {
                                    mPreferences.edit().putString(Constants.SHARED_CURRENT_USERNAME, jsonObject.get("userUuid").toString()).commit();
                                    mPreferences.edit().putString(Constants.SHARED_CURRENT_ACCESS_TOKEN, jsonObject.get("accessToken").toString()).commit();
                                    saveUser(email);
                                    JSONObject systemConfig = jsonObject.getJSONObject("systemConfig");
                                    Utils.saveCurrentConfiguration(LoginActivity.this, systemConfig);
                                    Intent a = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(a);
                                    overridePendingTransition(R.anim.slide_in_up,
                                            R.anim.slide_in_down);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorResponse(String error) {

                        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                });


            }

    }

    private void saveUser(String user) {

        if (mPreferences.getInt(Constants.SHARED_LOGGED_IN_SIZE, 0) == 0) {
            mPreferences.edit().putInt(Constants.SHARED_LOGGED_IN_SIZE, 1).commit();
            mPreferences.edit().putString(Constants.SHARED_LOGGED_IN + "0", user).commit();
        } else {

            String[] saved;
            int size = mPreferences.getInt(Constants.SHARED_LOGGED_IN_SIZE, 0);
            saved = new String[size];
            for (int i = 0; i < size; i++) {
                saved[i] = mPreferences .getString(Constants.SHARED_LOGGED_IN + Integer.toString(i), "");
            }
            if (!Arrays.asList(saved).contains(user)) {
                mPreferences.edit().putInt(Constants.SHARED_LOGGED_IN_SIZE, size + 1).commit();
                mPreferences.edit().putString(Constants.SHARED_LOGGED_IN + Integer.toString(size), user).commit();
            }
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    void showProgress() {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(false ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    false ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(false ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(false ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    false ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(false ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(false ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(false ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
    }


    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    /*
     * All logged users detecting
     */
    private String[] getLoggedIn()
    {
        String[] res = null;
        int size = mPreferences.getInt(Constants.SHARED_LOGGED_IN_SIZE, 0);
        Log.i("logged in size", Integer.toString(size));
        if (size != 0)
        {
            res= new String[size];
            for (int i=0;i<size;i++)
            {
                res[i] = mPreferences.getString(Constants.SHARED_LOGGED_IN + Integer.toString(i), "");
            }
        }

        return  res;
    }
}



