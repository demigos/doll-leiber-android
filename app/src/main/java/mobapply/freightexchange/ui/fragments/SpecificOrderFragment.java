package mobapply.freightexchange.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.providers.SpecificOrderProvider;
import mobapply.freightexchange.views.SpecificOrderView;

public class SpecificOrderFragment extends Fragment implements View.OnClickListener, SpecificOrderProvider.SpecificOrderCallback {

    private SpecificOrderProvider mSpecificOrderProvider;
    private String uuid;
    private SharedPreferences preferences;
    private SpecificOrderView mSpecificOrderView;
    private EditText edit_proposal;
    private Button btn_send_proposal;

    public static SpecificOrderFragment newInstance(String uuid) {
        SpecificOrderFragment fragment = new SpecificOrderFragment();
        Bundle args = new Bundle();
        args.putString(Constants.KEY_CHOOSED_ORDER, uuid);
        fragment.setArguments(args);
        return fragment;
    }

    public SpecificOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uuid = getArguments().getString(Constants.KEY_CHOOSED_ORDER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_specific_order, container, false);
        edit_proposal = (EditText)view.findViewById(R.id.proposal_edit);
        btn_send_proposal  =  (Button) view.findViewById(R.id.btn_action_proposal);
        btn_send_proposal.setOnClickListener(this);
        preferences = getActivity().getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        mSpecificOrderView = new SpecificOrderView(getActivity(), view);
        mSpecificOrderProvider = new SpecificOrderProvider(getActivity(),uuid);
        mSpecificOrderProvider.loadOrder(this);


        return view;
    }


    @Override
    public void onOrderLoaded(Order mOrder) {
        mSpecificOrderView.displayOrder(mOrder);
    }

    @Override
    public void onProposalSended(String amount) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_proposal.getWindowToken(), 0);
        SuccessFragment newFragment = new SuccessFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.BUNDLE_PROPOSAL_AMOUNT, amount);
        bundle.putParcelable(Constants.BUNDLE_ORDER_WITH_PROPOSAL, mSpecificOrderView.getmSpecificOrder());
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.setCustomAnimations(R.anim.push_down_in,
                0);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }



    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_action_proposal:

                if (Utils.getCurrentConfiguration(getActivity()).isBidWithPrice() &
                        Utils.getCurrentConfiguration(getActivity()).isShowInterest())
                {

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    SendProposalDialogFragment dialog = new SendProposalDialogFragment();
                    dialog.setTargetFragment(this, Constants.REQUESTCODE_MULTI_PROPOSAL);// creating new object
                    dialog.show(fm, "dialog");
                }
                else if (Utils.getCurrentConfiguration(getActivity()).isBidWithPrice())
                {
                    if (!edit_proposal.getText().toString().equalsIgnoreCase("")) {
                        String proposal = edit_proposal.getText().toString();
                        mSpecificOrderProvider.sendProposal(this, uuid, preferences.getString(Constants.SHARED_CURRENT_USERNAME, ""), proposal);
                    }
                    else
                        Toast.makeText(getActivity(), getActivity().getString(R.string.toast_empty_proposal), Toast.LENGTH_SHORT).show();
                }
                else if (Utils.getCurrentConfiguration(getActivity()).isShowInterest() )
                {
                    mSpecificOrderProvider.sendProposal(this, uuid, preferences.getString(Constants.SHARED_CURRENT_USERNAME, ""), "0");
                }
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case Constants.REQUESTCODE_MULTI_PROPOSAL:

                if (resultCode == Constants.RESULTCODE_PROPOSAL_PRICE) {
                       if (data.getExtras().getString(Constants.BUNDLE_PROPOSAL_PRICE)!=null) {
                           String proposal = data.getExtras().getString(Constants.BUNDLE_PROPOSAL_PRICE);
                           mSpecificOrderProvider.sendProposal(this, uuid, preferences.getString(Constants.SHARED_CURRENT_USERNAME, ""), proposal);
                       }
                } else if (resultCode == Constants.RESULTCODE_PROPOSAL_INTEREST){
                    mSpecificOrderProvider.sendProposal(this, uuid, preferences.getString(Constants.SHARED_CURRENT_USERNAME, ""), "0");
                }

                break;
        }
    }
}



