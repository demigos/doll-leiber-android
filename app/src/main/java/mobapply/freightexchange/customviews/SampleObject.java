package mobapply.freightexchange.customviews;

/**
 * Created by Ivan Dunskiy on 15.05.15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * SampleObject for Overview By Country model
 */
class SampleObject {
    final String[] rowItems;
    public SampleObject(int length){
        rowItems = new String[length+1];
        for (int i = 0; i < rowItems.length; i++) {
            rowItems[i] = "";
        }
    }

    public void replaceItem(int position, String value) {
        rowItems[position] = value;
    }
}
