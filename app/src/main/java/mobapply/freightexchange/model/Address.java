package mobapply.freightexchange.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by  Ivan Dunskiy  on 4/6/15.
 *
 * Class for Address model parsing
 */
public class Address implements Parcelable {

    @SerializedName("country")
    private final
    String country;
    @SerializedName("zipCode")
    private final
    String zipCode;
    @SerializedName("city")
    private final
    String city;
    @SerializedName("countryCode")
    private
    String countryCode;
    @SerializedName("street")
    private
    String street;
    @SerializedName("houseNumber")
    private
    String houseNumber;


    public Address(String country, String zipCode, String city, String countryCode, String street, String houseNumber) {
        this.country = country;
        this.zipCode = zipCode;
        this.city = city;
        this.countryCode = countryCode;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public Address(String country, String zipCode, String city) {
        this.country = country;
        this.zipCode = zipCode;
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getStreet() {
        return street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getCountry() {
        return country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return city +System.getProperty("line.separator") +
                country + System.getProperty("line.separator") +
                zipCode;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.country);
        dest.writeString(this.zipCode);
        dest.writeString(this.city);
        dest.writeString(this.countryCode);
        dest.writeString(this.street);
        dest.writeString(this.houseNumber);
    }

    private Address(Parcel in) {
        this.country = in.readString();
        this.zipCode = in.readString();
        this.city = in.readString();
        this.countryCode = in.readString();
        this.street = in.readString();
        this.houseNumber = in.readString();
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        public Address createFromParcel(Parcel source) {
            return new Address(source);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    };
}
