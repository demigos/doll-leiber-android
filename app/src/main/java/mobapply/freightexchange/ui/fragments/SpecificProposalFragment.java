package mobapply.freightexchange.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.model.MyProposal;
import mobapply.freightexchange.providers.SpecificProposalProvider;
import mobapply.freightexchange.views.SpecificProposalView;


public class SpecificProposalFragment extends Fragment implements View.OnClickListener, SpecificProposalProvider.SpecificProposalCallback {
    private static final String TAG =  " SpecificProposalFragment";


    private String uuid;
    private SharedPreferences preferences;
    private SpecificProposalView mSpecificProposalView;
    private SpecificProposalProvider mSpecificProposalProvider;
    private Button btn_send_proposal;


    public static SpecificProposalFragment newInstance(String uuid) {
        SpecificProposalFragment fragment = new SpecificProposalFragment();
        Bundle args = new Bundle();
        args.putString(Constants.KEY_CHOOSED_PROPOSAL, uuid);
        fragment.setArguments(args);
        return fragment;
    }

    public SpecificProposalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uuid = getArguments().getString(Constants.KEY_CHOOSED_PROPOSAL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_specific_order, container, false);
        btn_send_proposal  =  (Button) view.findViewById(R.id.btn_action_proposal);
        btn_send_proposal.setOnClickListener(this);
        preferences = getActivity().getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        mSpecificProposalView = new SpecificProposalView(getActivity(), view);
        mSpecificProposalProvider = new SpecificProposalProvider(getActivity(),uuid);
        mSpecificProposalProvider.loadProposal(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_action_proposal:
                SpecificProposalProvider.rejectProposal(getActivity(), new SpecificProposalProvider.SpecificProposalCallback() {

                    @Override
                    public void onProposalLoaded(MyProposal mOrder) {

                    }

                    @Override
                    public void onProposalRejected() {
                        RejectFragment newFragment = new RejectFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.BUNDLE_REJECT_AMOUNT, Float.toString(mSpecificProposalView.getmSpecificProposal().getCostOfFreight()));
                        newFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.setCustomAnimations(R.anim.push_down_in,
                                0);
                        transaction.replace(R.id.container, newFragment);
                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();
                    }
                }, mSpecificProposalView.getmSpecificProposal().getUuid());

                break;

        }
    }


    @Override
    public void onProposalLoaded(MyProposal myProposal) {
        mSpecificProposalView.displayProposal(myProposal);
    }

    @Override
    public void onProposalRejected() {

    }
}
