package mobapply.freightexchange.providers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.ui.activities.LoginActivity;

/**
 * Created by Ivan Dunskiy on 6/4/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class MySpecificOrderProvider {
    private static final String TAG = "OrdersListProvider";
    private Context mContext;
    private ProgressDialog mProgressDialog;
    private String orderUUID ;
    private Order mSpecificOrder;
    private SharedPreferences mPreferences;

    public MySpecificOrderProvider(Context mContext, String orderUUID) {
        this.orderUUID = orderUUID;
        this.mContext = mContext;
        mPreferences = mContext.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    /*
     * Class for loading and processing orders
    */
    public void loadOrder(final MySpecificOrderCallback callback) {
        String url =  Utils.getRequestUrl(mContext, WebRequestTypes.ALL_ORDERS)
                + orderUUID;
        Utils.sendNewJsonVolleyRequest(mContext, Request.Method.GET , url, null, new VolleyCallback() {
            @Override
            public void onSuccess(String stringObject) {

                try {
                    ((Activity)(mContext)).runOnUiThread(new OrderParsingHelper(new JSONObject(stringObject), callback));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });

    }
    /*
     Sending message method to the server . Flag determines when the message was sent - by the order
     cancellation or just regular sending method.
     */
    public void sendMessage(final MySpecificOrderCallback callback, final int flag , final String messageText, final String latitude, final String longitude ) {

//        String url = AuthenticationRequester.URL_USERS +  mPreferences.getString(Constants.SHARED_CURRENT_USERNAME, "") + "/orders/" +
//                mSpecificOrder.getUuid() + "/messages";
      String url =  Utils.getRequestUrl(mContext, WebRequestTypes.USERS)
                  +  mPreferences.getString(Constants.SHARED_CURRENT_USERNAME, "") + "/orders/" +
                mSpecificOrder.getUuid() + "/messages";
        JSONObject params = new JSONObject();
        try {
            params.put("messageText",messageText);
            params.put("latitude",latitude);
            params.put("longitude",longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utils.sendNewJsonVolleyRequest(mContext, Request.Method.POST , url, params, new VolleyCallback() {
            @Override
            public void onSuccess(String  stringObject) {
                callback.onMessageSend(flag);
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void changeOrderStatus(final MySpecificOrderCallback callback, final String orderStatus) {
//        String url = AuthenticationRequester.URL_USERS +  mPreferences.getString(Constants.SHARED_CURRENT_USERNAME, "") + "/orders/" +
//                mSpecificOrder.getUuid() ;

        String url = Utils.getRequestUrl(mContext, WebRequestTypes.USERS)
                  +  mPreferences.getString(Constants.SHARED_CURRENT_USERNAME, "") + "/orders/" +
                mSpecificOrder.getUuid() ;
        JSONObject params = new JSONObject();
        try {
            params.put("orderStatus",orderStatus);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utils.sendNewJsonVolleyRequest(mContext, Request.Method.POST , url, params, new VolleyCallback() {
            @Override
            public void onSuccess(String  stringObject) {
                callback.onStatusChanged();
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void displayMessage(String json) {

        Toast.makeText(mContext, json.toString(), Toast.LENGTH_SHORT).show();
    }


    public interface MySpecificOrderCallback {
        void onOrderLoaded(Order mOrder);
        void onStatusChanged();
        void onMessageSend(int flag);
    }
    /*
     * Class for data parsing and displaying
     */
    class OrderParsingHelper implements Runnable {
        // Defines the code to run for this task
        JSONObject receivedJsonArray;
        MySpecificOrderCallback ordersCallback;

        OrderParsingHelper(JSONObject receivedJsonArray, MySpecificOrderCallback ordersCallback) {
            this.receivedJsonArray = receivedJsonArray;
            this.ordersCallback = ordersCallback;
        }

        public void run() {
            mSpecificOrder = JSONParser.parseSpecificOrder(receivedJsonArray);
            ordersCallback.onOrderLoaded(mSpecificOrder);
        }
    }
}
