package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobapply.freightexchange.R;
import mobapply.freightexchange.customviews.TableMainLayout;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.interfaces.OverviewCountryHandler;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.model.OverviewItem;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.library.webutils.TransmissionResponseCode;


public class OverviewByCountryFragment extends Fragment implements OverviewCountryHandler {

    private ArrayList<OverviewItem> overviewList = new ArrayList<>();
    private final ArrayList<String> headersArray = new ArrayList<>();
    private final ArrayList<String> rowsArray = new ArrayList<>();
    private RelativeLayout relContainer;
    private FrameLayout containerFr;
    private TableMainLayout tableMainLayout;

    public OverviewByCountryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =   LayoutInflater.from(container.getContext())
                .inflate(R.layout.fragment_overview_by_country, container, false);
        relContainer = (RelativeLayout)view.findViewById(R.id.realContainer);
        containerFr = (FrameLayout)view.findViewById(R.id.container);
        headersArray.add("nach");
        getOverviews();
        return view;
    }

    private void getOverviews() {
        String url = Utils.getRequestUrl(getActivity(), WebRequestTypes.OVERVIEW_BY_COUNTRY);
        Utils.sendNewJsonVolleyRequest(getActivity(), Request.Method.GET, url, null, new VolleyCallback() {
            @Override
            public void onSuccess(String jsonArray) {
                JSONArray jsArray = null;
                try {
                    jsArray = new JSONArray(jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                overviewList = (ArrayList<OverviewItem>) JSONParser.parseOverviewItems(jsArray);
                Log.d("TAG", "" + overviewList);

                for (int i = 0; i < overviewList.size(); i++) {
                    boolean rowHasItem = false;
                    for (int k = 0; k < rowsArray.size(); k++) {
                        if (rowsArray.get(k).equals(overviewList.get(i).getFromCountry())) {
                            rowHasItem = true;
                        }
                    }

                    if (!rowHasItem)
                        rowsArray.add(overviewList.get(i).getFromCountry());

                    if (rowsArray.size() == 0) {
                        rowsArray.add(overviewList.get(i).getFromCountry());
                    }

                    boolean headerHasItem = false;
                    for (int k = 0; k < headersArray.size(); k++) {
                        if (headersArray.get(k).equals(overviewList.get(i).getToCountry())) {
                            headerHasItem = true;
                        }
                    }

                    if (!headerHasItem)
                        headersArray.add(overviewList.get(i).getToCountry());

                    if (rowsArray.size() == 0) {
                        headersArray.add(overviewList.get(i).getToCountry());
                    }
                }

                createTable(headersArray, rowsArray, overviewList);
            }

            @Override
            public void onErrorResponse(String error) {

            }
        });
    }

    private void createTable(final ArrayList<String> headersArray, final ArrayList<String> rowsArray,
                             final ArrayList<OverviewItem> overviewList) {

        new AsyncTask<String, String, TableMainLayout>() {

            @Override
            protected TableMainLayout doInBackground(String... params) {
                tableMainLayout = new TableMainLayout(getActivity(), headersArray, rowsArray, overviewList);
                tableMainLayout.setOverviewCountryHandlerListener(OverviewByCountryFragment.this);
                return tableMainLayout;
            }

            @Override
            public void onPostExecute(TableMainLayout tableMainLayout) {
                super.onPostExecute(tableMainLayout);
                containerFr.addView(tableMainLayout);
                relContainer.setVisibility(View.GONE);
            }
        }.execute();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDirectionChoosed(String fromCountry, String toCountry) {
        final android.support.v4.app.FragmentManager ft = getActivity().getSupportFragmentManager();
        JSONObject filterJSON  = new JSONObject();
        try {
            filterJSON.put(Constants.FILTER_DEPARTURE_COUNTRY, fromCountry);
            filterJSON.put(Constants.FILTER_DESTINATION_COUNTRY, toCountry);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ft.beginTransaction().replace(R.id.flContent, FilteredOrdersFragment.newInstance(filterJSON.toString())).commit();
    }
}