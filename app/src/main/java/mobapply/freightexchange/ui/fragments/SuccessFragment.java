package mobapply.freightexchange.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.model.MyProposal;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.providers.SpecificProposalProvider;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class SuccessFragment extends Fragment {


    private String proposalAmount;
    private Order mSpecificOrderWithProposal;
    private ArrayList<MyProposal> proposalArrayList;

    public SuccessFragment() {
        // Required empty public constructor
    }

    public static SuccessFragment newInstance(String uuid, Order order ) {
        SuccessFragment fragment = new SuccessFragment();
        Bundle args = new Bundle();
        args.putString(Constants.BUNDLE_PROPOSAL_AMOUNT, uuid);
        args.putParcelable(Constants.BUNDLE_ORDER_WITH_PROPOSAL, order);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            proposalAmount = getArguments().getString(Constants.BUNDLE_PROPOSAL_AMOUNT);
            mSpecificOrderWithProposal = getArguments().getParcelable(Constants.BUNDLE_ORDER_WITH_PROPOSAL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_success, container, false);
        TextView success_price = (TextView) view.findViewById(R.id.success_price);
        ImageView success_image = (ImageView) view.findViewById(R.id.success_image);
        Button btn_success_ok = (Button) view.findViewById(R.id.btn_success_ok);
        Button btn_reject_success_proposal = (Button) view.findViewById(R.id.btn_reject_success_proposal);
        if (proposalAmount!=null && !proposalAmount.isEmpty()){
            success_price.setText(proposalAmount + " " + getString(R.string.euro_label));
        }
        SVG svg = SVGParser.getSVGFromResource(getResources(), R.raw.ic_done_48px, 0xFF000000, 0xFFFFFFFF);
        Drawable drawable = svg.createPictureDrawable();
        success_image.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        success_image.setImageDrawable(drawable);

        btn_success_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        btn_reject_success_proposal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSpecificOrderWithProposal != null) {
                    final SharedPreferences mPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
                    String url = AuthenticationRequester.URL_USERS + mPreferences.getString(Constants.SHARED_CURRENT_USERNAME, "")+ "/proposals";
                    Utils.sendNewJsonVolleyRequest(getActivity(), Request.Method.GET, url, null, new VolleyCallback() {
                        @Override
                        public void onSuccess(String stringObject) {
                            try {
                                JSONArray objects = new JSONArray(stringObject);
                                proposalArrayList = (ArrayList<MyProposal>) JSONParser.parseProposals(objects);
                                MyProposal mLastProposal = proposalArrayList.get(0);
                                SpecificProposalProvider.rejectProposal(getActivity() , new SpecificProposalProvider.SpecificProposalCallback() {
                                    @Override
                                    public void onProposalLoaded(MyProposal mOrder) {

                                    }

                                    @Override
                                    public void onProposalRejected() {
                                        RejectFragment newFragment = RejectFragment.newInstance(proposalAmount);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(Constants.BUNDLE_REJECT_AMOUNT, proposalAmount);
                                        newFragment.setArguments(bundle);
                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                        transaction.setCustomAnimations(R.anim.push_down_in,
                                                0);
                                        transaction.replace(R.id.container, newFragment);
                                        transaction.addToBackStack(null);
                                        // Commit the transaction
                                        transaction.commit();
                                    }
                                }, mLastProposal.getUuid());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(String error) {
                            Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                        }
                    });


                }

            }
        });
        return view;
    }



}
