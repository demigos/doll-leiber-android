package mobapply.freightexchange.providers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.Order;

/**
 * Created by Ivan Dunskiy on 6/4/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class OrdersListProvider {
    private static final String TAG = "OrdersListProvider";
    private Context mContext;
    private ProgressDialog mProgressDialog;
    private SharedPreferences mPreferences;

    public OrdersListProvider(Context mContext) {
        this.mContext = mContext;
    }

  /*
   * Method for loading and processing orders in different from UI thread
   */
    public void loadOrders(final OrdersCallback callback) {
        final String url = Utils.getRequestUrl(mContext, WebRequestTypes.FILTER)
                + "orderStatuses=NEW" ;

        Utils.sendNewJsonVolleyRequest(mContext, Request.Method.GET , url, null, new VolleyCallback() {
            @Override
            public void onSuccess(String  stringObject) {
                try {
                    ((Activity)(mContext)).runOnUiThread(new OrdersParsingHelper(new JSONArray(stringObject), callback));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*
     * Interface to display parsed List of Orders in the View
     */
    public interface OrdersCallback {
        void onOrdersLoaded(List<Order> orderList);
    }

    class OrdersParsingHelper implements Runnable {
        /*
         * The JSON response can be quite big - so parsing executes in another thread too
         */
        JSONArray receivedJsonArray;
        OrdersCallback ordersCallback;

        OrdersParsingHelper(JSONArray receivedJsonArray, OrdersCallback ordersCallback) {
            this.receivedJsonArray = receivedJsonArray;
            this.ordersCallback = ordersCallback;
        }

        public void run() {
            List<Order> ordersList = JSONParser.parseOrders(receivedJsonArray);
            Utils.getOrdersHashMap(ordersList);
            ordersCallback.onOrdersLoaded(ordersList);
        }
    }
}
