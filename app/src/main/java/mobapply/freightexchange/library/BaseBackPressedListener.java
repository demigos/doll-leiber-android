package mobapply.freightexchange.library;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import mobapply.freightexchange.library.interfaces.OnBackPressedListener;

/**
 * Created by Ivan Dunskiy on 5/23/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * Сlass for IOnBackPressedListener interface implementation
 */
public class BaseBackPressedListener implements OnBackPressedListener {

    private final FragmentActivity activity;

    public BaseBackPressedListener(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void doBack() {
        activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

}