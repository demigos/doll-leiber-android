package mobapply.freightexchange.ui.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.arrayadapters.FiltersPagerAdapter;
import mobapply.freightexchange.library.interfaces.DateSet;
import mobapply.freightexchange.library.interfaces.SavedFilterCommunicator;
import mobapply.freightexchange.model.FilterItem;
import mobapply.freightexchange.ui.fragments.FilterFragment;
import mobapply.freightexchange.ui.fragments.SavedFiltersDeleteFragment;
import mobapply.freightexchange.ui.fragments.SavedFiltersFragment;

public class FilterTabbedActivity extends ActionBarActivity implements DateSet{

    private ViewPager mViewPager;
    private FiltersPagerAdapter mSettingsPagerAdapter;
    private String TAG = "FilterTabbedActivity";
    public static FilterItem savedFilterItem;
    public SavedFilterCommunicator savedFilterCommunicator;
    @Override
    public void onBackPressed() {
        if(mViewPager.getCurrentItem() == 1) {
            if (mSettingsPagerAdapter.getItem(1) instanceof SavedFiltersDeleteFragment) {
                ((SavedFiltersDeleteFragment) mSettingsPagerAdapter.getItem(1)).backPressed();
            }
            else if (mSettingsPagerAdapter.getItem(1) instanceof SavedFiltersFragment) {
                finish();
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_tabbed);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mViewPager = (ViewPager)findViewById(R.id.view_pager);
        mSettingsPagerAdapter =
                new FiltersPagerAdapter(
                        getSupportFragmentManager());
        mViewPager.setAdapter(mSettingsPagerAdapter);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setShouldExpand(true);
        tabs.setViewPager(mViewPager);

        if (getIntent().getExtras() !=null && getIntent().getExtras().getParcelable(Constants.BUNDLE_SAVED_FILTER)!=null)
        {
            FilterItem filterItem =  getIntent().getExtras().getParcelable(Constants.BUNDLE_SAVED_FILTER);
            mViewPager.setCurrentItem(0);
            Log.i(TAG, filterItem.toString());

        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDateSet(FilterFragment.FilterType type, String date) {
       if ( mSettingsPagerAdapter.getItem(0) instanceof FilterFragment)
       {
           Log.i(TAG + " get instance",date + " hello " + type.toString() );
           ((FilterFragment)mSettingsPagerAdapter.getItem(0)).onDateSet(type, date);
       }
    }

    public void setSavedFilter(FilterItem filterName) {
        mViewPager.setTag(R.id.TAG_ONLINE_ID, filterName);
        mViewPager.setCurrentItem(0);
        savedFilterItem = filterName;
        Log.i(TAG + " received filter Item", filterName.toString());
        if ( mSettingsPagerAdapter.getItem(0) instanceof FilterFragment)
        {
            savedFilterCommunicator.passSavedFilter(filterName);
//            ((FilterFragment)mSettingsPagerAdapter.getItem(0)).parseSavedFilter(filterName);
        }

    }

}
