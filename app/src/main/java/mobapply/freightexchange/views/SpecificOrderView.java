package mobapply.freightexchange.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.gc.materialdesign.views.ButtonFloat;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.library.webutils.TransmissionResponseCode;
import mobapply.freightexchange.model.Order;
import mobapply.freightexchange.ui.activities.MainActivity;
import mobapply.freightexchange.ui.activities.SpecificOrderActivity;
import mobapply.freightexchange.ui.fragments.SuccessFragment;

/**
 * Created by Ivan Dunskiy on 6/15/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class SpecificOrderView {
    private final Context mContext;
    private final TextView proposal_price;
    private final TextView proposal_label;
    private final Button btn_action_proposal;
    private EditText edit_proposal;
    private TextView specific_order_load;
    private TextView specific_order_weight_goods;
    private TextView specific_order_number;
    private TextView specific_order_dpre_date;
    private TextView specific_order_contact;
    private TextView specific_order_type;
    private TextView specific_order_charge_weight;
    private TextView specific_order_appoint_till;
    private TextView specific_order_dispocode;
    private TextView specific_order_volume_weight;
    private TextView specific_order_loading_metre;
    private TextView specific_order_volume;
    private TextView specific_order_hazpoints;
    private TextView specific_order_quantity;
    private TextView specific_additional_info;
    private TextView specific_order_dpre_city;
    private TextView specific_order_dest_city;
    private TextView specific_order_from_city;
    private TextView specific_order_from_country;
    private TextView specific_order_from_index;
    private TextView specific_order_to_city;
    private TextView specific_order_to_country;
    private TextView specific_order_to_index;
    private SharedPreferences preferences;
    Button btn_send_proposal;
    Order mSpecificOrder;
    public SpecificOrderView(Context context, View rootView)
    {
        mContext = context;
        preferences = mContext.getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        edit_proposal = (EditText)rootView.findViewById(R.id.proposal_edit);
        specific_order_load = (TextView)rootView.findViewById(R.id.specific_order_load);

        specific_order_from_city  = (TextView)rootView.findViewById(R.id.specific_order_from_city);
        specific_order_from_country  = (TextView)rootView.findViewById(R.id.specific_order_from_country);
        specific_order_from_index  = (TextView)rootView.findViewById(R.id.specific_order_from_index);

        specific_order_to_city  = (TextView)rootView.findViewById(R.id.specific_order_to_city);
        specific_order_to_country  = (TextView)rootView.findViewById(R.id.specific_order_to_country);
        specific_order_to_index  = (TextView)rootView.findViewById(R.id.specific_order_to_index);

        specific_order_weight_goods = (TextView)rootView.findViewById(R.id.specific_order_weight_goods);
        proposal_price = (TextView)rootView.findViewById(R.id.proposal_price);
        proposal_label = (TextView)rootView.findViewById(R.id.proposal_label);
        btn_action_proposal = (Button)rootView.findViewById(R.id.btn_action_proposal);
        /*
         * Defining the specific order fields
         */
        specific_order_number = (TextView)rootView.findViewById(R.id.specific_order_number);
        specific_order_dpre_date = (TextView)rootView.findViewById(R.id.specific_order_dpre_date);
        specific_order_contact = (TextView)rootView.findViewById(R.id.specific_order_contact);
        specific_order_type = (TextView)rootView.findViewById(R.id.specific_order_type);
        specific_order_charge_weight = (TextView)rootView.findViewById(R.id.specific_order_charge_weight);
        specific_order_appoint_till = (TextView)rootView.findViewById(R.id.specific_order_appoint_till);
        specific_order_dispocode = (TextView)rootView.findViewById(R.id.specific_order_dispocode);
        specific_order_volume_weight = (TextView)rootView.findViewById(R.id.specific_order_volume_weight);
        specific_order_loading_metre = (TextView)rootView.findViewById(R.id.specific_order_loading_metre);
        specific_order_volume = (TextView)rootView.findViewById(R.id.specific_order_volume);
        specific_order_hazpoints = (TextView)rootView.findViewById(R.id.specific_order_hazpoints);
        specific_order_quantity = (TextView)rootView.findViewById(R.id.specific_order_quantity);
        specific_additional_info = (TextView)rootView.findViewById(R.id.specific_additional_info);
        specific_order_dpre_city= (TextView)rootView.findViewById(R.id.specific_order_dpre_city);
        specific_order_dest_city= (TextView)rootView.findViewById(R.id.specific_order_dest_city);

    }
    public void displayOrder(Order mSpecificOrder) {
        configUI(mContext);
        ((SpecificOrderActivity) mContext).getSupportActionBar().setTitle(mContext.getString(R.string.specific_order_title) + " " + mSpecificOrder.getNumber());
        specific_order_load.setText(Utils.getFormattedDate(mContext, mSpecificOrder.getAppointmentFrom()) +
                System.getProperty("line.separator") +
                Utils.getFormattedTime(mContext, mSpecificOrder.getAppointmentFrom()));

        if (mSpecificOrder.getDepartureAddress()!=null )
        {
            specific_order_from_city.setText(mSpecificOrder.getDepartureAddress().getCity());
            specific_order_from_country.setText(mSpecificOrder.getDepartureAddress().getCountry());
            specific_order_from_index.setText(mSpecificOrder.getDepartureAddress().getZipCode());
        }
        if (mSpecificOrder.getDestinationAddress()!=null )
        {
            specific_order_to_city.setText(mSpecificOrder.getDestinationAddress().getCity());
            specific_order_to_country.setText(mSpecificOrder.getDestinationAddress().getCountry());
            specific_order_to_index.setText(mSpecificOrder.getDestinationAddress().getZipCode());
        }
        final SpannableStringBuilder sb = new SpannableStringBuilder(Double.toString(mSpecificOrder.getActualWeight()) + " kg of " +
                mSpecificOrder.getGood());
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(bss, 0, Double.toString(mSpecificOrder.getActualWeight()).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        specific_order_weight_goods.setText(sb);

        specific_order_number.setText("# " + mSpecificOrder.getNumber());
        specific_order_dpre_date.setText(Utils.getFormattedDate(mContext, mSpecificOrder.getAppointmentFrom()));
        specific_order_dpre_city.setText(mSpecificOrder.getDepartureAddress().getCity());
        specific_order_dest_city.setText(mSpecificOrder.getDestinationAddress().getCity());
        specific_order_contact.setText(mSpecificOrder.getCustomerContactPerson());
        specific_order_type.setText(mSpecificOrder.getPackageType());
        specific_order_charge_weight.setText(Double.toString(mSpecificOrder.getChargeWeight()));
        specific_order_appoint_till.setText(Utils.getFormattedDate(mContext, mSpecificOrder.getAppointmentTill()));
        specific_order_dispocode.setText(mSpecificOrder.getDispositionCode());
        specific_order_volume_weight.setText(Double.toString(mSpecificOrder.getVolumeWeight()));
        specific_order_loading_metre.setText(Double.toString(mSpecificOrder.getLoadingMetre()));
        specific_order_volume.setText(Double.toString(mSpecificOrder.getVolume()));
        specific_order_hazpoints.setText(Integer.toString(mSpecificOrder.getHazmatPoints()));
        specific_order_quantity.setText(Double.toString(mSpecificOrder.getQuantity()));
        specific_additional_info.setText(mSpecificOrder.getAdditionalInfo());
        this.mSpecificOrder = mSpecificOrder;



    }

    public Order getmSpecificOrder() {
        return mSpecificOrder;
    }

    /*
    * This method configures the UI in regarding to current system configuration
    */
    public void configUI(Context mContext) {
        if (Utils.getCurrentConfiguration(mContext).isBidWithPrice() &
                Utils.getCurrentConfiguration(mContext).isShowInterest())
        {
            edit_proposal.setVisibility(View.GONE);
            proposal_price.setVisibility(View.GONE);
            proposal_label.setText(R.string.proposal_multi_config);

        }
        else if (Utils.getCurrentConfiguration(mContext).isBidWithPrice())
        {
            edit_proposal.setVisibility(View.VISIBLE);
            proposal_price.setVisibility(View.VISIBLE);
            proposal_label.setText(R.string.your_proposal_label);
        }
        else if (Utils.getCurrentConfiguration(mContext).isShowInterest())
        {
            edit_proposal.setVisibility(View.GONE);
            proposal_price.setVisibility(View.GONE);
            proposal_label.setText(R.string.proposal_show_interest);
        }

    }
}
