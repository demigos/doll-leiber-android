package mobapply.freightexchange.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.arrayadapters.MyProposalsAdapterFixed;
import mobapply.freightexchange.library.interfaces.SortingItems;
import mobapply.freightexchange.model.MyProposal;
import mobapply.freightexchange.model.SortingStates;
import mobapply.freightexchange.ui.activities.MainActivity;
import mobapply.freightexchange.ui.activities.SpecificOrderActivity;

/**
 * Created by Ivan Dunskiy on 6/4/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 *
 * View class for {@link mobapply.freightexchange.ui.fragments.MyProposalsFragment} implementation
 */
public class MyProposalsView{


    private final AutoCompleteTextView label_order_goods;
    private final AutoCompleteTextView label_order_to;
    private final AutoCompleteTextView label_order_from;

    SortingItems sortingItems;

    private  ListView mListView;
    private final TextView mLabel_order_info;
    private Context mContext;
    MyProposalsAdapterFixed mAdapter;
    private List<MyProposal> proposalList;

    public MyProposalsView(Context context, View rootView) {
        mContext = context;
        mListView = (ListView) rootView.findViewById(R.id.orders_list_view);

        label_order_goods = (AutoCompleteTextView) rootView.findViewById(R.id.label_order_goods);
        label_order_to = (AutoCompleteTextView) rootView.findViewById(R.id.label_order_to);
        label_order_from = (AutoCompleteTextView) rootView.findViewById(R.id.label_order_from);

        mLabel_order_info = (TextView) rootView.findViewById(R.id.label_order_info);
        mLabel_order_info.setGravity(Gravity.CENTER);
        mLabel_order_info.setText(R.string.label_price);
    }

    public void displayProposals(final Context mContext, final List<MyProposal> proposalList)
    {
        this.proposalList = proposalList;
        if (mListView.getAdapter() != null && mListView.getAdapter().getCount()!=0)
        {
            ((MyProposalsAdapterFixed)mListView.getAdapter()).setProposalsList(proposalList);
        }
        else {
            mAdapter = new MyProposalsAdapterFixed(mContext, R.layout.my_proposal_item,proposalList);
            mListView.setAdapter(mAdapter);
        }

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent a = new Intent(mContext, SpecificOrderActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CHOOSED_PROPOSAL, proposalList.get(position).getUuid());
                a.putExtras(bundle);
                ((MainActivity) mContext).startActivityForResult(a, Constants.REQUESTCODE_REJECT_PROPOSAL);
            }
        });
        label_order_to.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                label_order_to.setText(mContext.getString(R.string.label_to));
                if (position==0)
                {
                    mAdapter.highlighLabel(SortingStates.CITY_TO);
                    sortingItems.sortItems(SortingStates.CITY_TO);
                }
                else if (position == 1)
                {
                    mAdapter.highlighLabel(SortingStates.COUNTRY_TO);
                    sortingItems.sortItems(SortingStates.COUNTRY_TO);
                }
                else
                {
                    mAdapter.highlighLabel(SortingStates.ZIPCODE_TO);
                    sortingItems.sortItems(SortingStates.ZIPCODE_TO);
                }

            }
        });
        label_order_from.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                label_order_from.setText(mContext.getString(R.string.label_from));
                if (position==0)
                {
                    mAdapter.highlighLabel(SortingStates.CITY_FROM);
                    sortingItems.sortItems(SortingStates.CITY_FROM);
                }
                else if (position == 1)
                {
                    mAdapter.highlighLabel(SortingStates.COUNTRY_FROM);
                    sortingItems.sortItems(SortingStates.COUNTRY_FROM);
                }
                else
                {
                    mAdapter.highlighLabel(SortingStates.ZIPCODE_FROM);
                    sortingItems.sortItems(SortingStates.ZIPCODE_FROM);
                }

            }
        });

        label_order_goods.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                label_order_goods.setText(mContext.getString(R.string.label_goods));
                if (position == 0) {
                    mAdapter.highlighLabel(SortingStates.GOODS);
                    sortingItems.sortItems(SortingStates.GOODS);
                } else
                {
                    mAdapter.highlighLabel(SortingStates.WEIGHT);
                    sortingItems.sortItems(SortingStates.WEIGHT);
                }

            }
        });
    }


    public MyProposalsAdapterFixed getProposalsAdapter() {
        return mAdapter;
    }

    public List<MyProposal> getProposalsList() {
        return proposalList;
    }

    public void notifyDataChanged() {
        mAdapter.notifyDataSetChanged();
    }

    public void invalidateSortingArrays() {
        String[] goodsArray =  mContext.getResources().getStringArray(R.array.sort_goods);
        ArrayAdapter goodsSpinnerArrayAdapter = new ArrayAdapter(mContext,
                R.layout.spinner_item_layout,goodsArray
        );
        label_order_goods.setDropDownWidth(getTheMostWideText(goodsArray));
        label_order_goods.setAdapter(goodsSpinnerArrayAdapter);
        String[] locationsArray =  mContext.getResources().getStringArray(R.array.sort_locations);
        final ArrayAdapter fromSpinnerArrayAdapter = new ArrayAdapter(mContext,
                R.layout.spinner_item_layout,
                locationsArray);
        label_order_to.setDropDownWidth(getTheMostWideText(locationsArray));
        label_order_to.setAdapter(fromSpinnerArrayAdapter);
        label_order_from.setDropDownWidth(getTheMostWideText(locationsArray));
        label_order_from.setAdapter(fromSpinnerArrayAdapter);

    }

    private int getTheMostWideText(String[] array){
        int theMostWide = 0;
        for (int i = 0; i < array.length-1; i++)
        {
            if (array[i].length() > array[i+1].length())
            {
                theMostWide = array[i].length();
            }
            else
                theMostWide = array[i+1].length();
        }
        return  theMostWide * 25;
    }

    public void setiSortingItems(SortingItems sortingItems) {
        this.sortingItems = sortingItems;
    }
}
