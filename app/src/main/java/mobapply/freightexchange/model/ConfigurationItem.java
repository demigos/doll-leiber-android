package mobapply.freightexchange.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ivan Dunskiy on 7/10/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class ConfigurationItem {

    @SerializedName("showInterest")
    boolean show_interest;
    @SerializedName("bidWithPrice")
    boolean bid_with_price;
    @SerializedName("anonymousOrders")
    boolean anonymous_orders;
    @SerializedName("ordersProgressStatus")
    boolean orders_progress_status;
    @SerializedName("ordersProgressMessages")
    boolean orders_progress_messages;

    public boolean isShowInterest() {
        return show_interest;
    }

    public void setShowInterest(boolean show_interest) {
        this.show_interest = show_interest;
    }

    public boolean isBidWithPrice() {
        return bid_with_price;
    }

    public void isBidWithPrice(boolean bid_with_price) {
        this.bid_with_price = bid_with_price;
    }

    public boolean isAnonymousOrders() {
        return anonymous_orders;
    }

    public void setAnonymousOrders(boolean anonymous_orders) {
        this.anonymous_orders = anonymous_orders;
    }

    public boolean isOrdersProgressStatus() {
        return orders_progress_status;
    }

    public void setOrdersProgressStatus(boolean orders_progress_status) {
        this.orders_progress_status = orders_progress_status;
    }

    public boolean isOrdersProgressMessages() {
        return orders_progress_messages;
    }

    public void setOrdersProgressMessages(boolean orders_progress_messages) {
        this.orders_progress_messages = orders_progress_messages;
    }

    @Override
    public String toString() {
        return "ConfigurationItem{" +
                "show_interest=" + show_interest +
                ", bid_with_price=" + bid_with_price +
                ", anonymous_orders=" + anonymous_orders +
                ", orders_progress_status=" + orders_progress_status +
                ", orders_progress_messages=" + orders_progress_messages +
                '}';
    }
}
