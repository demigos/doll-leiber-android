package mobapply.freightexchange.library;

/**
 * Created by Ivan Dunskiy on 4/1/15.
 *
 * Constants creation
 */
public class Constants {
    public static final String URL_GEOCODING = "http://maps.googleapis.com/maps/api/geocode/json?" ;


    public static final String KEY_CHOOSED_ORDER = "order_was_choosed" ;
    public static final String KEY_CHOOSED_PROPOSAL =  "proposal_was_choosed";
    public static final String KEY_CHOOSED_MY_ORDER = "my_order_was_choosed";
    public static final String DATETYPE_PICKER = "datetype_picker";

    public static final String FILTER_DEPARTURE_COUNTRY = "departureAddress.country";
    public static final String FILTER_DEPARTURE_COUNTRY_ISO = "departureAddress.countryCode";
    public static final String FILTER_DEPARTURE_ZIPCODE = "departureAddress.zipCode";
    public static final String FILTER_DEPARTURE_CITY = "departureAddress.city";

    public static final String FILTER_DESTINATION_COUNTRY= "destinationAddress.country";
    public static final String FILTER_DESTINATION_COUNTRY_ISO = "destinationAddress.countryCode";
    public static final String FILTER_DESTINATION_ZIPCODE= "destinationAddress.zipCode";
    public static final String FILTER_DESTINATION_CITY= "destinationAddress.city";

    public static final String FILTER_DATE_FROM= "departureDate.from";
    public static final String FILTER_DATE_TO= "departureDate.to";


    public static final int REQUESTCODE_FILTER = 23;
    public static final int REQUESTCODE_REJECT_PROPOSAL =  24;
    public static final int REQUESTCODE_MAP = 25;
    public static final int REQUESTCODE_ORDER_CANCEL = 26;
    public static final int REQUESTCODE_MULTI_PROPOSAL = 27;
    public static final int REQUESTCODE_DATETIME = 28;

    public static final String BUNDLE_FILTER_STRING = "string for results filtering";
    public static final String BUNDLE_PROPOSAL_AMOUNT = "proposal_amount" ;
    public static final String BUNDLE_ORDER_WITH_PROPOSAL = "order_with_proposal_amount" ;
    public static final String BUNDLE_REJECT_AMOUNT = "reject_amount" ;
    public static final String BUNDLE_SAVED_FILTER = "saved filter";
    public static final String BUNDLE_CANCELLATION_MESSAGE = "cancellation message";
    /* sending from the dialog with multi proposal sending - with price and showing interest */
    public static final String BUNDLE_PROPOSAL_PRICE = "proposal price from dialog";
    public static final String BUNDLE_DATE_FROM = "date from";
    public static final String BUNDLE_DATE_TO = "date to";


    public static final String SHARED_PREFS_NAME = "InterTrans_sharedPreferences";
    public static final String SHARED_CURRENT_USERNAME = "user logged in";
    public static final String SHARED_LOGGED_IN_SIZE = "logged_in_user_size" ;
    public static final String SHARED_LOGGED_IN = "later_logged_in_user";
    public static final String SHARED_FILTER_LIST = "list of saved filters";
    public static final String SHARED_CURRENT_CONFIG = "current system configuration";
    public static final String SHARED_CURRENT_HOST = "entered host for remote server";


    public static final int RQS_GooglePlayServices =1 ;

    public static final int FLAG_ADDRESS_DEPARTURE = 1;
    public static final int FLAG_ADDRESS_DESTINATION = 2;
    public static final String KEY_TOOLTIP_ORDER_NUMBER = "order_number" ;
    public static final String KEY_TOOLTIP_DESTINATION = "from_or_where_to_go";
    public static final String KEY_TOOLTIP_DEST_CITY = "city_to_go";
    public static final String KEY_RECEIVED_ORDER = "recieved_order";
    public static final String KEY_TOOLTIP_DEPARTURE_CITY = "city_from_go";

    public static final int RESULTCODE_PROPOSAL_PRICE = 312 ;
    public static final int RESULTCODE_PROPOSAL_INTEREST = 313 ;


    public static final String SHARED_SETTINGS_OVERVIEW = "save overview by country setting";
    public static final String SHARED_SETTINGS_CRASH_REPORTS = "save crash reports setting";


    public static final String FILTER_ORDER_STATUS = "orderStatuses";
    public static final String SHARED_CURRENT_ACCESS_TOKEN = "current access token for session";
    public static final String ACTION_REJECT_SUCCESS = "proposal was successfully rejected";

}
