package mobapply.freightexchange.model.statuses;

/**
 * Created by Ivan Dunskiy on 4/28/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * Enum for possible order statuses
 */
public enum OrderStatuses {
    NEW{
        @Override
        public String toString() {
            return "new";
        }
    },
    ASSIGNED{
        @Override
        public String toString() {
            return "assigned";
        }
    },
    ACKNOWLEDGED{
        @Override
        public String toString() {
            return "acknowledged";
        }
    },
    IN_PROGRESS{
        @Override
        public String toString() {
            return "in_progress";
        }
    },
    DONE{
        @Override
        public String toString() {
            return "done";
        }
    },
    CANCELLED{
        @Override
        public String toString() {
            return "cancelled";
        }
    },
    NOT_ACTUAL{
        @Override
        public String toString() {
            return "not_actual";
        }
    },


}