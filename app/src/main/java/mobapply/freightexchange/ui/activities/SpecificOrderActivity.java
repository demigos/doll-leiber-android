package mobapply.freightexchange.ui.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.ui.fragments.MySpecificOrderFragment;
import mobapply.freightexchange.ui.fragments.SpecificOrderFragment;
import mobapply.freightexchange.ui.fragments.SpecificProposalFragment;

public class SpecificOrderActivity extends ActionBarActivity {

    private static final String TAG = "SpecificOrderActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_order);
        if(findViewById(R.id.container) != null)
        {
            if(savedInstanceState != null)
                return;
            if (getIntent().getExtras()!=null && getIntent().getExtras().getString(Constants.KEY_CHOOSED_ORDER)!=null) {
                String uuid = getIntent().getExtras().getString(Constants.KEY_CHOOSED_ORDER);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, SpecificOrderFragment.newInstance(uuid) )
                        .commit();
            }
            else if
                (getIntent().getExtras()!=null && getIntent().getExtras().getString(Constants.KEY_CHOOSED_PROPOSAL)!=null) {
                String uuid = getIntent().getExtras().getString(Constants.KEY_CHOOSED_PROPOSAL);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, SpecificProposalFragment.newInstance(uuid) )
                        .commit();
            }
            else if
                 (getIntent().getExtras()!=null && getIntent().getExtras().getString(Constants.KEY_CHOOSED_MY_ORDER)!=null) {
                String uuid = getIntent().getExtras().getString(Constants.KEY_CHOOSED_MY_ORDER);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, MySpecificOrderFragment.newInstance(uuid) )
                        .commit();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

        }
        return true;
    }
}
