package mobapply.freightexchange.model;

/**
 * Created by Ivan Dunskiy on 7/14/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public enum SortingStates {
    CITY_TO{
        @Override
        public String toString() {
            return "city_to";
        }
    },
    CITY_FROM{
        @Override
        public String toString() {
            return "city_from";
        }
    },
    COUNTRY_TO{
        @Override
        public String toString() {
            return "country_to";
        }
    },
    COUNTRY_FROM{
        @Override
        public String toString() {
            return "country_from";
        }
    },
    ZIPCODE_TO{
        @Override
        public String toString() {
            return "zipcode_to";
        }
    },
    ZIPCODE_FROM{
        @Override
        public String toString() {
            return "zipcode_from";
        }
    },
    GOODS{
        @Override
        public String toString() {
            return "goods";
        }
    },
     WEIGHT{
        @Override
        public String toString() {
            return "weight";
        }
    }, INFO{
        @Override
        public String toString() {
            return "info";
        }
    },
    DATE{
        @Override
        public String toString() {
            return "date";
        }
    }
}
