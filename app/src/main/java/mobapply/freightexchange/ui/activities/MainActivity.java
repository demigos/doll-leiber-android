package mobapply.freightexchange.ui.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;

import mobapply.freightexchange.R;
import mobapply.freightexchange.customviews.FragmentNavigationDrawer;
import mobapply.freightexchange.library.BaseBackPressedListener;
import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.interfaces.OnBackPressedListener;
import mobapply.freightexchange.model.DrawerItem;
import mobapply.freightexchange.ui.fragments.CreateFilterFragment;
import mobapply.freightexchange.ui.fragments.FilteredOrdersFragment;
import mobapply.freightexchange.ui.fragments.LogOutFragment;
import mobapply.freightexchange.ui.fragments.MapOrdersFragment;
import mobapply.freightexchange.ui.fragments.MyOrdersFragment;
import mobapply.freightexchange.ui.fragments.MyProposalsFragment;
import mobapply.freightexchange.ui.fragments.OrdersFragment;
import mobapply.freightexchange.ui.fragments.OverviewByCountryFragment;
import mobapply.freightexchange.ui.fragments.SavedFiltersDeleteFragment;
import mobapply.freightexchange.ui.fragments.SavedFiltersFragment;
import mobapply.freightexchange.ui.fragments.SettingsFragment;

/**
 * Created by Ivan Dunskiy on 4/6/15.
   Copyright (c) 2015 Rarus. All rights reserved.
 */
public class MainActivity extends ActionBarActivity
        implements ActionBar.OnNavigationListener,
        DrawerLayout.DrawerListener,OnBackPressedListener {

    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentNavigationDrawer dlDrawer;
    private ListView lvDrawer;
    private Toolbar mToolbar;
    private BaseBackPressedListener onBackPressedListener;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        //set up full screen
        preferences = getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);

        // Find our drawer view

        dlDrawer = (FragmentNavigationDrawer) findViewById(R.id.drawer_layout);

        // Setup drawer view
        dlDrawer.setContext(this);
        dlDrawer.setActivity(this);
        lvDrawer = (ListView) findViewById(R.id.lvDrawer);
        if (lvDrawer.getHeaderViewsCount() == 0) {
            LayoutInflater inflater = getLayoutInflater();
            View header = inflater.inflate(R.layout.custom_drawer_header, lvDrawer, false);
            lvDrawer.addHeaderView(header, null, false);
            /*
             * Define the width of the Navigation Drawer header width
             */
            final ImageView drawer_image = (ImageView) header.findViewById(R.id.drawer_image);
            ViewTreeObserver vto = drawer_image.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    drawer_image.getViewTreeObserver().removeOnPreDrawListener(this);
                    int finalWidth = drawer_image.getMeasuredWidth();
                    DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) lvDrawer.getLayoutParams();
                    params.width = finalWidth;
                    lvDrawer.setLayoutParams(params);
                    return true;
                }
            });
        }
        dlDrawer.setupDrawerConfiguration((ListView) findViewById(R.id.lvDrawer), mToolbar
        );
        // Add nav items
        dlDrawer.addNavItem(new DrawerItem(getString(R.string.label_new_orders), R.drawable.drawable_orderlist), getString(R.string.label_new_orders), OrdersFragment.class);
        dlDrawer.addNavItem(new DrawerItem(getString(R.string.label_my_proposals), R.drawable.drawable_my_proposals), getString(R.string.label_my_proposals), MyProposalsFragment.class);
        dlDrawer.addNavItem(new DrawerItem(getString(R.string.label_my_orders), R.drawable.drawable_my_orders), getString(R.string.label_my_orders), MyOrdersFragment.class);
        dlDrawer.addNavItem(new DrawerItem(getString(R.string.label_create_filter), R.drawable.drawable_create_filter), getString(R.string.label_create_filter), CreateFilterFragment.class);
        if (preferences.getBoolean(Constants.SHARED_SETTINGS_OVERVIEW, false))
            dlDrawer.addNavItem(new DrawerItem(getString(R.string.label_overview_by_country), R.drawable.drawable_overview_by_country),getString(R.string.label_overview_by_country), OverviewByCountryFragment.class);
        dlDrawer.addNavItem(new DrawerItem(getString(R.string.label_map_orders), R.drawable.drawable_map_orders),getString(R.string.label_map_orders), MapOrdersFragment.class);
        dlDrawer.addNavItem(new DrawerItem(getString(R.string.label_settings), R.drawable.drawable_settings),getString(R.string.label_settings), SettingsFragment.class);
        dlDrawer.addNavItem(new DrawerItem(getString(R.string.label_log_out), R.drawable.drawable_log_out), "", LogOutFragment.class);
        // Select default
        if (savedInstanceState == null) {
            dlDrawer.selectDrawerItem(0);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                dlDrawer,
                mToolbar,
                R.string.drawer_open,
                R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
                syncState();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                syncState();
            }
        };

        dlDrawer.setDrawerListener(mDrawerToggle);
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());

    }

    public void syncDrawerState()
    {
        if ( preferences.getBoolean(Constants.SHARED_SETTINGS_OVERVIEW, false) && !dlDrawer.isOverviewByCountryContains())
        {
            dlDrawer.addNavItemToPosition(new DrawerItem(getString(R.string.label_overview_by_country), R.drawable.drawable_overview_by_country), 4, getString(R.string.label_overview_by_country), OverviewByCountryFragment.class);
            dlDrawer.syncDrawerTitleState();
        }
        else if (!preferences.getBoolean(Constants.SHARED_SETTINGS_OVERVIEW, false) && dlDrawer.isOverviewByCountryContains())
            dlDrawer.removeOverviewByCountry();
        mDrawerToggle.syncState();
        dlDrawer.syncDrawerTitleState();
    }

    @Override
    protected void onResume() {

        android.support.v4.app.Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.flContent);
        if ((mFragment instanceof FilteredOrdersFragment)) {
            setSupportActionBar(mToolbar);
        }
        setToolBar();

        if ( preferences.getBoolean(Constants.SHARED_SETTINGS_OVERVIEW, false) && !dlDrawer.isOverviewByCountryContains())
        {
            dlDrawer.addNavItemToPosition(new DrawerItem(getString(R.string.label_overview_by_country), R.drawable.drawable_overview_by_country), 4, getString(R.string.label_overview_by_country), OverviewByCountryFragment.class);
            dlDrawer.syncDrawerTitleState();
        }
        else if (!preferences.getBoolean(Constants.SHARED_SETTINGS_OVERVIEW, false) && dlDrawer.isOverviewByCountryContains())
            dlDrawer.removeOverviewByCountry();


        super.onResume();
    }

    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        return false;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {


    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        android.support.v4.app.Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.flContent);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUESTCODE_FILTER) {

                if (mFragment instanceof OrdersFragment) {
                    if (data != null && data.getStringExtra(Constants.BUNDLE_FILTER_STRING) != null) {
                        String receivedFilterString = data.getStringExtra(Constants.BUNDLE_FILTER_STRING);
                        try
                        {
                            final android.support.v4.app.FragmentManager ft = getSupportFragmentManager();
                            ft.beginTransaction().replace(R.id.flContent, FilteredOrdersFragment.newInstance(receivedFilterString)).commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    dlDrawer.selectDrawerItem(0);
                    String receivedFilterString = data.getStringExtra(Constants.BUNDLE_FILTER_STRING);
                    final android.support.v4.app.FragmentManager ft = getSupportFragmentManager();
                    ft.beginTransaction().replace(R.id.flContent, FilteredOrdersFragment.newInstance(receivedFilterString)).commit();
                }
            } else if (requestCode == Constants.REQUESTCODE_REJECT_PROPOSAL) {
                if (mFragment instanceof MyProposalsFragment) {
                    ((MyProposalsFragment) mFragment).reloadProposals();
                }
            }
            else if (requestCode == Constants.REQUESTCODE_MAP) {
                dlDrawer.selectDrawerItem(5);
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        android.support.v4.app.Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.flContent);
        if (!(mFragment instanceof SavedFiltersFragment)) {
            this.finish();
            this.overridePendingTransition(0, R.anim.top_slide_in_down);
        }
    }

    public void setToolBar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setSupportActionBar(mToolbar);
    }


    private android.support.v4.app.FragmentManager.OnBackStackChangedListener getListener()
    {

        return new android.support.v4.app.FragmentManager.OnBackStackChangedListener()
        {
            public void onBackStackChanged()
            {
                android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
                if (manager != null)
                {
                    android.support.v4.app.Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.flContent);
                    if (mFragment instanceof SavedFiltersDeleteFragment)
                    {
                        ((SavedFiltersDeleteFragment)mFragment).updateUI();
                    }

                }
            }
        };
    }

    @Override
    public void doBack() {

    }

    public void setFragment(int i) {
        dlDrawer.selectDrawerItem(i);
    }
}
