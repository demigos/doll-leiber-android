package mobapply.freightexchange.library;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.ConfigurationItem;
import mobapply.freightexchange.model.ErrorMessage;
import mobapply.freightexchange.model.FilterItem;
import mobapply.freightexchange.model.Order;

/**
 * Created by Ivan Dunskiy on 4/8/15.
   Copyright (c) 2015 Rarus. All rights reserved.
 */
public class Utils {

    private static final String TAG = "Utils";


    public static String getFormattedDate(Context context, long mValue)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat(context.getString(R.string.format_adapter_date));
        Date date = new Date(mValue);
        return dateFormat.format(date);
    }


    public static String getFormattedTime(Context context, long mValue){
        Date date = new Date(mValue);
        DateFormat formatter = new SimpleDateFormat(context.getString(R.string.format_adapter_time));
        return formatter.format(date);
    }

    private static void storeFilterItems(Context context, List<FilterItem> filters) {
        // used for store arrayList in json format
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(Constants.SHARED_PREFS_NAME,Context.MODE_PRIVATE);
        editor = settings.edit();
        Gson gson = new Gson();
        String jsonFilters = gson.toJson(filters);
        editor.putString(Constants.SHARED_FILTER_LIST, jsonFilters);
        editor.commit();
    }
    public static ArrayList<FilterItem> loadFiltersList(Context context) {
        // used for retrieving arraylist from json formatted string
        SharedPreferences settings;
        List<FilterItem> filters;
        settings = context.getSharedPreferences(Constants.SHARED_PREFS_NAME,Context.MODE_PRIVATE);
        if (settings.contains(Constants.SHARED_FILTER_LIST)) {
            String jsonFavorites = settings.getString(Constants.SHARED_FILTER_LIST, null);
            Gson gson = new Gson();
            FilterItem[] filterItems = gson.fromJson(jsonFavorites,FilterItem[].class);
            filters = Arrays.asList(filterItems);
            filters = new ArrayList<>(filters);
        } else
            return null;
        return (ArrayList<FilterItem>) filters;
    }
    public static void addFilterItem(Context context, FilterItem filterItem) {
        List<FilterItem> filtersList = loadFiltersList(context);
        if (filtersList == null)
            filtersList = new ArrayList<>();
        filtersList.add(filterItem);
        storeFilterItems(context, filtersList);
    }
    public static void removeFilterItem(Context context, FilterItem filterItem) {
        List<FilterItem> filtersList = loadFiltersList(context);
        if (filtersList != null) {
            filtersList.remove(filterItem);
            storeFilterItems(context, filtersList);
        }

    }

    public static void checkGoogleServices(Activity activity, int status)
    {
        try {
            if (status != ConnectionResult.SUCCESS) {
                GooglePlayServicesUtil.getErrorDialog(status, activity, Constants.RQS_GooglePlayServices).show();
            }
        } catch (Exception e) {
            Log.e(TAG , e.getLocalizedMessage());
        }
    }


    /*
     * Receive ArrayList of orders and return HashMap with uuid as a key with the same collection size
     */
    public static Map<String, Order> getOrdersHashMap(List<Order> arrayList) {
        OrderManager.getInstance().getOrders().clear();
        for (Order order : arrayList){
            OrderManager.getInstance().getOrders().put(order.getUuid(), order);
        }
        return OrderManager.getInstance().getOrders();

    }


    public static void saveCurrentConfiguration(Context mContext, JSONObject jsonObject ) {
        SharedPreferences sharedPreferences = mContext.getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(Constants.SHARED_CURRENT_CONFIG, jsonObject.toString());
        prefsEditor.apply();
    }

    public static ConfigurationItem getCurrentConfiguration(Context mContext)
    {
        SharedPreferences sharedPreferences = mContext.getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(Constants.SHARED_CURRENT_CONFIG, null);
        return gson.fromJson(json, ConfigurationItem.class);
    }

    public static String getUrlWithParameters(String url, JSONObject object) {
        URL ur = null ;
        try {

            if (object != null) {
                Iterator<String> iter = object.keys();
                Map<String, Object> params = new LinkedHashMap<>();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        Object value = object.get(key);
                        params.put(key, value);
                    } catch (JSONException e) {
                        // Something went wrong!
                    }
                }

                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (postData.length() != 0)
                        postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                ur = new URL(url + postData.toString());
            } else
                ur = new URL(url);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ur.toString();
    }

    /*
     * Method for Volley requests sending and handling
     */
    public static void sendNewJsonVolleyRequest(final Context mContext, int method ,String url, final JSONObject params, final VolleyCallback volleyCallback){


        final ProgressDialog mProgressDialog = ProgressDialog.show(mContext, mContext.getString(R.string.dialog_progress_title),
                mContext.getString(R.string.dialog_progress_message), true);
        RequestQueue queue = Volley.newRequestQueue(mContext);
        final SharedPreferences preferences = mContext.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        String uuid =   preferences.getString(Constants.SHARED_CURRENT_USERNAME, null);
        String access_token =   preferences.getString(Constants.SHARED_CURRENT_ACCESS_TOKEN, null);
        Log.i(TAG + " uuid data", "uuid " + uuid + " access_token " + access_token);
        CustomVolleyRequest request = new CustomVolleyRequest(method, url,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        // This is status code: response.statusCode
                        // This is string response: NetworkResponseRequest.parseToString(response)
                        if (response.statusCode==204){
                            volleyCallback.onSuccess(Constants.ACTION_REJECT_SUCCESS);
                        }
                        if (CustomVolleyRequest.parseToString(response)!=null){
                            volleyCallback.onSuccess(CustomVolleyRequest.parseToString(response));
                            mProgressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();

                if(error.networkResponse != null && error.networkResponse.data != null){
                    String errorMessage = new String(error.networkResponse.data);
                    ErrorMessage errorMessageObj = null;
                    try {
                         errorMessageObj = new Gson().fromJson(String.valueOf(new JSONObject(errorMessage)),ErrorMessage.class);
                         volleyCallback.onErrorResponse(errorMessageObj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    volleyCallback.onErrorResponse( mContext.getString(R.string.error_network_timeout));
                } else if (error instanceof AuthFailureError) {
                    //TODO
                } else if (error instanceof ServerError) {
                    //TODO
                } else if (error instanceof NetworkError) {
                    //TODO
                } else if (error instanceof ParseError) {
                    //TODO
                }
            }
        }

        ) {


            @Override
            protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
                Map<String, String> responseHeaders = response.headers;
                    for (Map.Entry<String, String> thisEntry : responseHeaders.entrySet()) {
                        Object key = thisEntry.getKey();
                        Object value = thisEntry.getValue();
                        Log.i(TAG + " headers values", " head key " + key + " value " + value);
                        if (key.toString().equals("X-Result-Count")) {
                            volleyCallback.onSuccess(value.toString());
                            Log.i(TAG, " head key " + key + " value" + value);
                        }
                    }
                return Response.success(response, HttpHeaderParser.parseCacheHeaders(response));
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> hashMap = new HashMap<>();
                if (params != null && !params.toString().isEmpty()) {
                    Iterator<?> keys = params.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        try {
                            hashMap.put(key, params.getString(key));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
                return hashMap;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String uuid = preferences.getString(Constants.SHARED_CURRENT_USERNAME, null);
                String access_token = preferences.getString(Constants.SHARED_CURRENT_ACCESS_TOKEN, null);
                params.put("X-Consumer-Key", uuid);
                params.put("X-Consumer-Secret", access_token);
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(request);
    }

    public static String getRequestUrl(Context context, WebRequestTypes webRequestType){
        String url = null;
        boolean isDefaultUrl = true;
        SharedPreferences mPreferences = context.getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        if (mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") != null &&
                !mPreferences.getString(Constants.SHARED_CURRENT_HOST, "").isEmpty() )
        {
            isDefaultUrl = false;
        }

        switch (webRequestType){
            case LOGIN:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "users/accessTokens";
                }
                else
                    url = AuthenticationRequester.URL_LOGIN ;
                break;
            case ALL_ORDERS:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "orders/";
                }
                else
                    url = AuthenticationRequester.URL_ALL_ORDERS ;
                break;
            case USERS:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "users/";
                }
                else
                    url = AuthenticationRequester.URL_USERS ;
                break;
            case FILTER:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "orders?";
                }
                else
                    url = AuthenticationRequester.URL_ORDER_FILTER ;
                break;
            case PROPOSALS:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "proposals/";
                }
                else
                    url = AuthenticationRequester.URL_PROPOSALS ;
               break;
            case ALL_CITIES:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "addresses/cities";
                }
                else
                    url = AuthenticationRequester.URL_GET_CITIES ;
                break;
            case ALL_COUNTRIES:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "addresses/countries";
                }
                else
                    url = AuthenticationRequester.URL_GET_COUNTRIES;
                break;
            case ALL_ZIPCODES:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "addresses/zipCodes";
                }
                else
                    url = AuthenticationRequester.URL_GET_ZIPCODES ;
                break;
            case OVERVIEW_BY_COUNTRY:
                if (!isDefaultUrl){
                    url = mPreferences.getString(Constants.SHARED_CURRENT_HOST, "") + "orders?orderStatuses=NEW&grouped=true";
                }
                else
                    url = AuthenticationRequester.URL_OVERVIEW_BY_COUNTRY ;
                break;
        }
        return url;
    }
}
