package mobapply.freightexchange.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Constants;

/**
 * Created by Ivan Dunskiy on 4/28/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 * DialogFragment for logging out operation
 */
public class OrderCancelFragment extends DialogFragment {

    private SharedPreferences preferences;
    private EditText edit_cancel_order;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View layout = inflater.inflate(R.layout.fragment_cancel_order, null);
        edit_cancel_order = (EditText)layout.findViewById(R.id.edit_cancel_order);
        preferences= getActivity().getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(layout)
                .setIcon(R.drawable.ic_launcher)
                .setTitle(getString(R.string.text_cancel_order_title))
                .create();
        Button btn_cancel_with_gps = (Button) layout.findViewById(R.id.btn_cancel_with_gps);
        Button btn_cancel_send = (Button) layout.findViewById(R.id.btn_cancel_send);
        Button btn_log_out_no = (Button) layout.findViewById(R.id.btn_cancel_quit);
        btn_log_out_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderCancelFragment.this.dismiss();
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, getActivity().getIntent());

            }
        });
        btn_cancel_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( edit_cancel_order.getText() !=null &&
                        !edit_cancel_order.getText().toString().isEmpty()) {
                    Intent a = new Intent();
                    a.putExtra(Constants.BUNDLE_CANCELLATION_MESSAGE,edit_cancel_order.getText().toString() );
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, a);
                    OrderCancelFragment.this.dismiss();
                }
                else
                    Toast.makeText(getActivity(), getString(R.string.toast_empty_cancel_message), Toast.LENGTH_SHORT).show();
            }
        });
        btn_cancel_with_gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( edit_cancel_order.getText() !=null &&
                        !edit_cancel_order.getText().toString().isEmpty()) {
                    Intent a = new Intent();
                    a.putExtra(Constants.BUNDLE_CANCELLATION_MESSAGE, edit_cancel_order.getText().toString() );
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, a);
                    OrderCancelFragment.this.dismiss();
                }
                else
                    Toast.makeText(getActivity(), getString(R.string.toast_empty_cancel_message), Toast.LENGTH_SHORT).show();
            }
        });
        return alertDialog;

    }


}
