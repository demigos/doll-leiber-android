package mobapply.freightexchange.providers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import org.json.JSONException;
import org.json.JSONObject;

import mobapply.freightexchange.library.Constants;
import mobapply.freightexchange.library.JSONParser;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.library.WebRequestTypes;
import mobapply.freightexchange.library.interfaces.VolleyCallback;
import mobapply.freightexchange.library.webutils.AuthenticationRequester;
import mobapply.freightexchange.model.MyProposal;

/**
 * Created by Ivan Dunskiy on 6/4/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 */
public class SpecificProposalProvider {
    private static final String TAG = "SpecificProposalProvider";
    private static Context mContext;
    private String mProposalUUID ;
    private MyProposal mSpecificProposal;

    public SpecificProposalProvider(Context mContext, String mProposalUUID) {
        this.mProposalUUID = mProposalUUID;
        this.mContext = mContext;

    }
    /*
    * Class for loading and processing orders
    */
    public void loadProposal(final SpecificProposalCallback callback) {
        String url = Utils.getRequestUrl(mContext, WebRequestTypes.PROPOSALS)   + mProposalUUID;
        Utils.sendNewJsonVolleyRequest(mContext, Request.Method.GET , url, null, new VolleyCallback() {
            @Override
            public void onSuccess(String  stringObject) {
                try {
                    ((Activity)(mContext)).runOnUiThread(new OrderParsingHelper(new JSONObject(stringObject), callback));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void rejectProposal(final Context context, final SpecificProposalCallback callback, String uuid ) {
        String url = Utils.getRequestUrl(mContext, WebRequestTypes.PROPOSALS) + uuid;
        Utils.sendNewJsonVolleyRequest(context, Request.Method.DELETE , url, null, new VolleyCallback() {
            @Override
            public void onSuccess(String  stringObject) {
                //                    callback.onProposalLoaded(JSONParser.parseSpecificProposal(new JSONObject(stringObject)));
                if (stringObject.equalsIgnoreCase(Constants.ACTION_REJECT_SUCCESS))
                    callback.onProposalRejected();
            }

            @Override
            public void onErrorResponse(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface SpecificProposalCallback {
        void onProposalLoaded(MyProposal mOrder);
        void onProposalRejected();
    }
    /*
     * Class for data parsing and displaying
     */
    class OrderParsingHelper implements Runnable {
        // Defines the code to run for this task
        JSONObject receivedJsonArray;
        SpecificProposalCallback ordersCallback;

        OrderParsingHelper(JSONObject receivedJsonArray, SpecificProposalCallback ordersCallback) {
            this.receivedJsonArray = receivedJsonArray;
            this.ordersCallback = ordersCallback;
        }

        public void run() {
            mSpecificProposal = JSONParser.parseSpecificProposal(receivedJsonArray);
            ordersCallback.onProposalLoaded(mSpecificProposal);
        }
    }
}
