package mobapply.freightexchange.library.arrayadapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mobapply.freightexchange.R;
import mobapply.freightexchange.library.Utils;
import mobapply.freightexchange.model.FilterItem;

/**
 * Created by Ivan Dunskiy on 4/2/15.
 *
 *  Adapter for filter items displaying with choosing for delete ability ( check boxes )
 */
public class FilterToDeleteAdapter extends BaseAdapter {
    private List<FilterItem> mItems = new ArrayList<>();
    private final Activity context;

    public FilterToDeleteAdapter(Activity context) {
        this.context = context;
    }

    public void addItems(List<FilterItem> yourObjectList) {
        if(mItems==null){
            mItems = new ArrayList<>();
        }
        mItems.addAll(yourObjectList);
        notifyDataSetChanged();
    }

    List<FilterItem> getSavedFiltersItems() {
        return mItems;
    }

    public void removeFiltersItems() {
        for(Iterator<FilterItem> it = getSavedFiltersItems().iterator(); it.hasNext();) {
            FilterItem filter = it.next();
            if (filter.isHasToBeDeleted()) {
                it.remove();
                Utils.removeFilterItem(context, filter);
            }
        }
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
            view = context.getLayoutInflater().inflate(R.layout.toolbar_spinner_item_dropdown, parent, false);
            view.setTag("DROPDOWN");
        }

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
            view = context.getLayoutInflater().inflate(R.layout.
                    saved_delete_filter_item, parent, false);
            view.setTag("NON_DROPDOWN");
        }
        FilterItem currentFilterItem = mItems.get(position);
        TextView textView = (TextView) view.findViewById(R.id.save_filter_name);
        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.chb_delete_filter);
        checkBox.setChecked(currentFilterItem.isHasToBeDeleted());
        textView.setText(getTitle(position));
        return view;
    }

    private String getTitle(int position) {
        return position >= 0 && position < mItems.size() ? mItems.get(position).getName() : "";
    }

}