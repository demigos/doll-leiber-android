package mobapply.freightexchange.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by Ivan Dunskiy on 5/12/15.
 * Copyright (c) 2015 Rarus. All rights reserved.
 *
 *
 */
public class ErrorDialogFragment
        extends DialogFragment {

    // Global field to contain the error dialog
    private Dialog mDialog;

    /**
     * Default constructor. Sets the dialog field to null
     */
    @SuppressLint("ValidFragment")
    public ErrorDialogFragment() {
        super();
        mDialog = null;
    }

    /**
     * Set the dialog to display
     *
     * @param dialog
     *            An error dialog
     */
    public void setDialog(Dialog dialog) {
        mDialog = dialog;
    }

    /*
     * This method must return a Dialog to the DialogFragment.
     */
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return mDialog;
    }
}